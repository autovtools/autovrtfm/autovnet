# autovnet

[TOC]

## Description

`autovnet` provides simple, performant, intuitive, internet-scale IP network simulation, empowering Cyber Range administrators and virtual Red Teamers to provide unprecedented realism in adversary emulation for "Red vs Blue" cyber exercises and competitions.

Red Teamers simply import the game configuration from a central _autovnet server_, and each Red Teamer can immediately, concurrently emulate arbitrarily many (e.g. _millions_) of unique actors with various levels of sophistication, each with separate Command and Control (C2) infrastructure and IP addresses.

This is [Red Team Force Multiplication (RTFM)](https://autovtools.gitlab.io/autovrtfm/autovnet-docs/rtfm/) - the ultimate goal for Red Team Cyber Wargame infrastructure - finally made possible at scale with `autovnet`.

![setup](docs/img/term/client/setup.svg)

## Documentation

The full documentation for `autovnet` is hosted at [https://autovtools.gitlab.io/autovrtfm/autovnet-docs](https://autovtools.gitlab.io/autovrtfm/autovnet-docs).

If you already have `autovnet` installed, you can view the docs locally in your browser with

```bash
$ autovnet docs serve
```
