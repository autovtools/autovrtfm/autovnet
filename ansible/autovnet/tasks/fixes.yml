---

# Depending on your network config, IPv6 might not even be possible
# https://askubuntu.com/questions/32298/prefer-a-ipv4-dns-lookups-before-aaaaipv6-lookups
#precedence  ::1/128       50
#precedence  ::/0          40
#precedence  2002::/16     30
#precedence ::/96          20
#precedence ::ffff:0:0/96  100
- name: Prefer IPv4 over IPv6
  lineinfile:
    path: /etc/gai.conf
    line: "{{ item }}"
    create: True
    backup: True
  loop:
    - "precedence  ::1/128       50"
    - "precedence  ::/0          40"
    - "precedence  2002::/16     30"
    - "precedence ::/96          20"
    - "precedence ::ffff:0:0/96  100"
  when: AUTOVNET_FIX_GAI == "1"

# https://github.com/moloch--/sliver-py/tree/master#kali-linux--fix-openssl-errors
- name: (Slow, be patient) Fix sliver SSL handshake errors
  become: False
  environment:
    GRPC_PYTHON_BUILD_SYSTEM_OPENSSL: True
  # .run makes this happen inside autovnet's venv
  # TODO: bump version to keep in sync with pyprompt.toml / poetry.lock
  command: ".run python3 -m pip install --use-pep517 --force-reinstall --no-binary :all: 'grpcio>=1.59.0'"

# FIXME: this is somehow not getting applied on boot in Kali
# https://stackoverflow.com/questions/75675261/alternatives-to-tiocsti-now-that-is-disable-on-default-for-kernels-6-2
- name: Fix TIOCSTI on new kernels, needed by autovnet type it
  lineinfile:
    # NOTE: /etc/sysctl.conf is no longer respected in Kali
    #path: /etc/sysctl.conf
    path: /etc/sysctl.d/10-legacy-tiocsti.conf
    line: "dev.tty.legacy_tiocsti=1"
    # replace any existing setting
    regexp: "^dev.tty.legacy_tiocsti.*"
    create: True
    backup: True
  when: AUTOVNET_FIX_TIOCSTI == "1"

- name: Find pipx
  become: False
  command:
    cmd: which pipx
  register: which_pipx
  changed_when: False

######
# This block can be removed when / if https://github.com/calebstewart/pwncat/pull/288 is merged,
# and the 'no module distutils' bug get resolved
# (distutils was moved to setuptools in python 3.12, but there is magic, and upgrading the version of 'packaging' fixes everything)

- name: Check if pwncat-cs is fixed yet (it's ok if this fails)
  become: False
  ansible.builtin.command: pwncat-cs --help
  register: pwncat_result
  ignore_errors: True

- name: Uninstall broken pwncat-cs
  become: False
  community.general.pipx:
    name: pwncat-cs
    executable: "{{ which_pipx.stdout }}"
    state: uninstall
  when: pwncat_result is failed

# pipx install git+https://github.com/SafaSafari/pwncat.git@e0f6c0625b0609d7d159fc654e5ebc26c7afb818
- name: Install fork of pwncat-cs that almost fixes Python 3.12 compatibilty
  become: False
  community.general.pipx:
    name: git+https://github.com/SafaSafari/pwncat.git@e0f6c0625b0609d7d159fc654e5ebc26c7afb818
    executable: "{{ which_pipx.stdout }}"
  when: pwncat_result is failed

# pipx inject --force pwncat-cs packaging --pip-args='--upgrade'
- name: Upgrade packaging version used by pwncat-cs
  become: False
  community.general.pipx:
    name: pwncat-cs
    inject_packages: packaging
    pip_args: "--upgrade"
    force: True
    executable: "{{ which_pipx.stdout }}"
    state: inject
  when: pwncat_result is failed
######
