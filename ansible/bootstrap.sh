#!/bin/bash


# Level 1 bootstrapper, where execution starts
# Goal: get ansible installed, punt execution to ansible

set -euo pipefail

if [ "${UID}" = "0" ]; then
    cat << EOF
Do not run as root.
Run as a sudoer
EOF
    exit 1
fi


APT() {
    sudo DEBIAN_FRONTEND=noninteractive apt $@
}

AUTOVNET_INSTALL_BRANCH=${AUTOVNET_INSTALL_BRANCH:-main}
BOOTSTRAP_YML_URL="${BOOTSTRAP_YML_URL:-https://gitlab.com/autovtools/autovrtfm/autovnet/-/raw/${AUTOVNET_INSTALL_BRANCH}/ansible/bootstrap.yml}"
BOOTSTRAP_YML="${BOOTSTRAP_YML:-/tmp/bootstrap.yml}"
ASK_BECOME_PASS="${ASK_BECOME_PASS:- --ask-become-pass}"

# Must be kept in sync with bootstrap.yml
AUTOVNET_INSTALL_DIR="${AUTOVNET_INSTALL_DIR:-"${HOME}/autovnet"}"
AUTOVNET_YML="${AUTOVNET_YML:-${AUTOVNET_INSTALL_DIR}/ansible/autovnet.yml}"

FIRST_INSTALL="0"
if [ ! -d "${AUTOVNET_INSTALL_DIR}" ]; then
    # Assume this is the first time we've ever run this script, and we need to reboot after
    FIRST_INSTALL="1"
    echo "FIRST_INSTALL detected"
fi
APT update
APT install -y \
    curl \
    python3 \
    python3-apt \
    pipx \

PIPX_BIN_DIR=${PIPX_BIN_DIR:-"${HOME}/.local/bin"}
# PIPX should detect user's shell
pipx ensurepath
# Manually update the path, because the user's shell may not be /bin/bash (but this script is)
export PATH="${PATH}:${PIPX_BIN_DIR}"

# Install ansible and all the related cli tools
# --system-site-packages unforunately required because of python3-apt
pipx install --include-deps --system-site-packages ansible

# Install any ansible-galaxy dependencies needed by the bootstrap playbook
# (the autovnet role can declare / install more, when it executes)
ansible-galaxy collection install community.general 

# Download the bootstrap playbook
DOWNLOADED_BOOTSTRAP_YML="0"
if [ ! -f "${BOOTSTRAP_YML}" ]; then
    curl -sL "${BOOTSTRAP_YML_URL}" > "${BOOTSTRAP_YML}"
    DOWNLOADED_BOOTSTRAP_YML="1"
fi
ANSIBLE_ARGS="${ANSIBLE_ARGS:-}"

# Un-cache any sudo creds
sudo -K
# Try passwordless sudo
if sudo -n true &> /dev/null ; then
    # Passwordless sudo already enabled
    ansible-playbook ${ANSIBLE_ARGS} "${BOOTSTRAP_YML}"
else
    # Passwordless sudo not enabled
    echo "[autovnet installer] Enter the sudo password when prompted for the BECOME password:"
    ansible-playbook${ASK_BECOME_PASS} ${ANSIBLE_ARGS} "${BOOTSTRAP_YML}"
fi

if [ "${DOWNLOADED_BOOTSTRAP_YML}" = "1" ]; then
    # Delete the temp file so a re-execution will re-download latest
    rm "${BOOTSTRAP_YML}"
fi

# Run the bootstrap playbook
ansible-playbook ${ANSIBLE_ARGS} "${AUTOVNET_YML}"

if [ "${FIRST_INSTALL}" = "1" ]; then
    read -t 30 -p "Reboot required. Rebooting in 30s (ENTER to reboot now, CTRL+C to abort)" || true
    sudo reboot now
fi
