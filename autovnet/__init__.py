from .version import __version__

import typer

context_settings = {"help_option_names": ["-h", "--help"], "max_content_width": 800}
app = typer.Typer(context_settings=context_settings)
