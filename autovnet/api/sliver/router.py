import os
import shutil
import tarfile
import shlex
import shutil
import tempfile
from pathlib import Path

import typer

from fastapi import APIRouter, Request, HTTPException
from fastapi.responses import FileResponse
from starlette.background import BackgroundTask

from autovnet.models import Mount

from autovnet.utils import (
    compose_up,
    gen_codename,
    Cryptor,
)

router = APIRouter()


@router.get("/status")
async def status(
    request: Request,
):
    """Check if server is up"""
    pass


# WARNING: race condition on player name
@router.get("/{player}/config")
async def config(
    player: str,
    # token: str,
    request: Request,
) -> FileResponse:
    """Request a multiplayer config for player"""
    # TODO
    # if not request.app.validate_token(token):
    #    raise HTTPException(status_code=403, detail="[-] Invalid token")

    if not request.app.validate_player(player):
        raise HTTPException(status_code=400, detail="[-] Invalid player")

    # This should always be redundant,
    #   but in case authentication is ever broken, use trusted values
    token = request.app.token

    # WARNING: player variable is scary user input
    # Try to check for path escape / defeat LFI
    encrypted_bundle = Path(os.path.normpath(request.app.player_bundles_dir / f"{player}.etgz")).resolve()
    if request.app.player_bundles_dir not in encrypted_bundle.parents:
        raise HTTPException(status_code=400, detail="[-] Invalid player")

    # Any default sliver binaries that should be distributed in the bundle
    generated = request.app.server_dir / "generated"
    # Any scripts that should be distributed in the bundle
    scripts = request.app.server_dir / "scripts"

    # The svc mount for the actual sliver multiplayer API
    mount_yaml = request.app.server_dir / "mount.yaml"

    # Cert was already issued, resend it
    if encrypted_bundle.is_file():
        # WARNING: this enables impersonation, be nice
        typer.echo(f"[+] Found config for {player}")
        return FileResponse(encrypted_bundle)

    typer.echo(f"[+] Generate config for {player} ...")

    compose_file = "docker-compose.config.yml"
    out_dir = Path("/out")
    config_name = "sliver.json"

    # Create a tempdir used to generate the client cert
    with tempfile.TemporaryDirectory() as payload_dir:
        payload_dir = Path(payload_dir)
        bundle_dir = payload_dir / request.app.server_codename
        bundle_dir.mkdir(exist_ok=True, parents=True)
        sliver_config = bundle_dir / config_name

        tmp_bundle = bundle_dir / (str(sliver_config.name) + "tar.gz")

        project = f"sliver_{request.app.server_codename}_{gen_codename()}"
        docker_path = request.app.avn.docker_path(f"plugins/sliver")

        # /opt/sliver-bin/sliver-server operator --lhost 127.0.0.1 --lport 31337 --name grouchy_chocolate --save /tmp/sliver.json
        env = {
            "SLIVER_DATA": str(request.app.server_dir),
            "SLIVER_USER": player,
            "MULTIPLAYER_HOST": str(request.app.multiplayer_host),
            "MULTIPLAYER_PORT": str(request.app.multiplayer_port),
            "MOUNT_DIR": str(payload_dir),
            "CONTAINER_MOUNT_DIR": str(out_dir),
            "OUT_DIR": str(out_dir / request.app.server_codename),
            "UID": str(os.geteuid()),
            "GID": str(os.getegid()),
        }

        compose_up(docker_path, env=env, project=project, compose_file=compose_file, quiet=True)

        if not sliver_config.is_file():
            raise FileNotFoundError(f"[-] Client config {sliver_config} not generated!")

        for p in [mount_yaml, generated, scripts, request.app.client_dir]:
            if p.is_dir():
                shutil.copytree(p, bundle_dir / p.name, dirs_exist_ok=True)
            else:
                shutil.copy(p, bundle_dir)
        # Replace the "default" config used by the host with the generated config for the client
        # (players are all "trusted", but let everyone have a unique codename)
        configs_dir = bundle_dir / request.app.client_dir.name / "configs"
        default_config = configs_dir / "default.json"
        default_config.unlink()

        shutil.move(sliver_config, configs_dir / sliver_config.name)

        with tarfile.open(tmp_bundle, mode="w:gz") as tar:
            tar.add(str(bundle_dir), arcname=bundle_dir.name)

        cryptor = Cryptor(password=token)
        cryptor.encrypt_file(tmp_bundle, out_path=encrypted_bundle)
        tmp_bundle.unlink()

    typer.echo(f"[+] Serve config {encrypted_bundle}")
    cleanup = None
    if not request.app.save_configs:
        cleanup = BackgroundTask(encrypted_bundle.unlink)
    return FileResponse(encrypted_bundle, background=cleanup)
