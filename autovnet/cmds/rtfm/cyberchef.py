import os
import gzip
import shutil
import shlex
import subprocess
from pathlib import Path
from typing import Optional, List

import typer

from ...autovnet import AutoVnet, avn
from autovnet.models import Mount
from autovnet.utils import (
    compose_up_ctx,
    complete_codename,
    read_env_file,
    rand_port,
    wait_url_open,
    open_url,
    ensure_cert,
    pg_dumper,
)

app = typer.Typer()

MOUNT_TYPE = "cyberchef"


@app.command()
def export(
    networks: Optional[List[str]] = typer.Option(
        None, "-n", "--network", help="Simulated network to draw a random IP from."
    ),
    dest_ip: Optional[str] = typer.Option(
        "127.0.0.1",
        "-d",
        "--dest-ip",
        help="[ADVANCED] Bind address for the server (anything but 127.X.X.X may allow anyone to access!).",
    ),
    dest_port: Optional[int] = typer.Option(
        None, "-p", "--dest-port", help="Local bind port for the server. (default: random)."
    ),
    no_browser: Optional[bool] = typer.Option(
        False, "-N", "--no-browser", help="If set, do not open the default browser."
    ),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(MOUNT_TYPE), "-D", "--payload-dir", help="Directory to save command metadata."
    ),
    codename: Optional[str] = typer.Argument(
        None,
        autocompletion=complete_codename,
        help="Previous codename to load",
    ),
):
    """Run a self-hosted instance of cyberchef

    CyberChef is a "Cyber Swiss Army Knife" and great for CTF-like encoding / decoding tasks.
    """

    if dest_port is None:
        dest_port = rand_port()

    ctx = None
    new_svc = False

    if codename is None:
        # New svc
        ctx = lambda: avn.svc(
            networks=networks, dest_ip=dest_ip, dest_port=dest_port, payload_dir=payload_dir, echo=False
        )
        new_svc = True
    else:
        # Loaded svc
        ctx = lambda: avn.svc_from_codename(codename, payload_dir=payload_dir, echo=False)
        # Peek at listener config to grab the values we need
        ln = avn.listener_from_codename(codename, payload_dir=payload_dir)
        model = ln.lns[0]
        dest_ip = model.next_dest_ip
        dest_port = model.next_dest_port

    # Prepare compose
    docker_path = avn.docker_path(f"plugins/{MOUNT_TYPE}/")

    proto = "http"
    cyberchef_bind = f"{dest_ip}:{dest_port}"
    url = f"{proto}://{cyberchef_bind}"
    env = {
        "CYBERCHEF_BIND": cyberchef_bind,
    }

    with ctx() as (proc, codename, remote_addr):

        project = f"{MOUNT_TYPE}_{codename}"
        # dest_port included because invite link will contain it
        mount_cmd = f"autovnet rtfm {MOUNT_TYPE} mount -C {codename} {remote_addr}"
        export_cmd = f"autovnet rtfm {MOUNT_TYPE} export {codename}"

        typer.echo(f"[+] {export_cmd}")
        typer.echo(f"[+] connect : xdg-open {url}")
        typer.echo(f"[+] mount   : {mount_cmd}")

        with compose_up_ctx(docker_path, env=env, project=project, quiet=True) as (
            compose_thread,
            compose_stop,
        ):
            typer.echo(f"[*] Waiting for {MOUNT_TYPE} ...")
            ok = wait_url_open(url, proc=proc, thread=compose_thread)
            if ok:
                typer.echo(f"[+] {MOUNT_TYPE} is ready!")
            elif ok is False:
                typer.echo(f"[!] {MOUNT_TYPE} may still be initializing")

            if not no_browser:
                open_url(url)

            compose_thread.join()


@app.command()
def mount(
    dest_ip: Optional[str] = typer.Option(
        "127.0.0.1",
        "-d",
        "--dest-ip",
        help="[ADVANCED] Destination IP you will use to access the service. Anything but 127.X.X.X may be insecure!",
    ),
    dest_port: Optional[int] = typer.Option(
        None,
        "-p",
        "--dest-port",
        help="[ADVANCED] Destination port you will use to access the service (default: random).",
    ),
    no_browser: Optional[bool] = typer.Option(
        False, "-N", "--no-browser", help="If set, do not open the default browser."
    ),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(MOUNT_TYPE), "-D", "--payload-dir", help="Directory to save command metadata."
    ),
    codename: Optional[str] = typer.Option(None, "-C", "--set-codename", help="Set the codename for the mount."),
    opt: str = typer.Argument(
        ...,
        autocompletion=complete_codename,
        help="IP:PORT or codename to mount.",
    ),
):
    """Mount and connect to another player's cyberchef service"""

    # Nothing fancy here, a standard mount
    proto = "http"

    if no_browser:
        proto = None

    with avn.svc_mount(
        codename=codename,
        opt=opt,
        dest_ip=dest_ip,
        dest_port=dest_port,
        payload_dir=payload_dir,
        xdg_open=proto,
        mount_type=MOUNT_TYPE,
    ) as proc:
        proc.wait()
