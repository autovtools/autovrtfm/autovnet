import os
import re
import time
import shlex
import subprocess
from pathlib import Path
from typing import Optional, List

import typer

from ...autovnet import AutoVnet, avn
from autovnet.models import Mount
from autovnet.utils import (
    compose_up,
    compose_down,
    read_env_file,
    complete_codename,
    complete_opt_codename,
    gen_codename,
    rand_port,
    split_host_port,
    timestamp,
)

app = typer.Typer()


@app.command()
def export(
    networks: Optional[List[str]] = typer.Option(
        None, "-n", "--network", help="Simulated network to draw a random IP from."
    ),
    count: Optional[int] = typer.Option(1, "-c", "--count", help="The number of remote IP addresses to allocate."),
    remote_port: Optional[int] = typer.Option(None, "-p", "--port", help="Remote port to use for sshfs."),
    env_file: Optional[str] = typer.Option(
        AutoVnet.config_path("rtfm.env"), "-e", "--env-file", help="Environment file to pass to docker-compose"
    ),
    codename: Optional[bool] = typer.Option(
        False, "-C", "--codename", help="Use codename completion instead of file completion."
    ),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path("fs"), "-D", "--payload-dir", help="Directory to safe share output."
    ),
    opt: Optional[str] = typer.Argument(
        None,
        autocompletion=complete_opt_codename,
        help="Directory, or codename to export. If not give, a new directory will be created by codename.",
    ),
):
    """Export a local directory for other players to mount as a secure network share

    Uses sshfs, secured by the autovnet key.
    Red Teamers that mount this fs can see all exported files, edit, and delete them,
        but no one else can without access to the autovnet config secrets.
    Data in transit is encrypted via ssh.
    """
    dest_ip = "127.1.33.7"

    docker_path = avn.docker_path("plugins/fs")
    env = read_env_file(env_file)

    if opt is not None:
        # Best effort, use ./ or abs path to prevent matching codename regex
        codename_pattern = re.compile("^[a-z_]+$")
        is_path = Path(opt).is_absolute() or len(Path(opt).parts) != 1 or not codename_pattern.match(opt)

        # Must expand to absolute path now, or docker-compose will share the wrong dir (:
        if is_path:
            opt = Path(opt).absolute()
            if not opt.exists():
                raise FileNotFoundError(f"[-] {opt} not found!")

    ln = None
    # If codename
    if opt is not None and not is_path and (Path(payload_dir) / opt).is_dir():
        # codename
        ln = avn.listener_from_codename(opt, payload_dir=payload_dir, echo=False)

    else:
        if remote_port is None:
            remote_port = rand_port()
        ln = avn.listener(networks, [remote_port] * count, dest_ip, echo=False)
        # double-save to generate the codename :/
        ln.save(payload_dir)

        if opt is None:
            # Create new export
            opt = Path(payload_dir) / ln.codename / "export"
            opt.mkdir(parents=True, exist_ok=True)

        for model in ln.lns:
            model.fs_export = str(opt)
        ln.save(payload_dir, exist_ok=True)

    # Bad assumption
    dest_ip = ln.lns[0].dest_ip
    dest_port = ln.lns[0].dest_port
    fs_export = ln.lns[0].fs_export

    tmp_env = {
        "SHARE_DIR": fs_export,
        "UID": str(os.geteuid()),
        "GID": str(os.getegid()),
        "SSH_BIND": f"{dest_ip}:{dest_port}",
    }
    # Allow hardcore users to do what they want by crafting an env file
    tmp_env.update(env)
    env = tmp_env

    project = f"fs_export_{ln.codename}"
    with ln.listener():
        typer.echo(f"[+] autovnet rtfm fs export -C {ln.codename}")
        typer.echo(f"[+] export: {fs_export}")
        for model in ln.lns:
            typer.echo(f"[+] mount: autovnet rtfm fs mount -C {ln.codename} {model.remote_ip}:{model.remote_port}")
        compose_up(docker_path, detach=False, env=env, project=project, quiet=True)


@app.command()
def mount(
    codename: Optional[str] = typer.Option(None, "-C", "--set-codename", help="Set the codename for the mount."),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path("fs"), "-D", "--payload-dir", help="Directory to save metadata. Subdirectories will be created."
    ),
    mirror: Optional[bool] = typer.Option(
        False, "-m", "--mirror", help="Mirror the remote directory locally as a backup."
    ),
    mirror_path: Optional[str] = typer.Option(
        None,
        "-M",
        "--mirror-path",
        help="Alternate path to store mirror. Do NOT make local changes in the mirror directory.",
    ),
    mirror_interval: Optional[int] = typer.Option(
        60, "-i", "--mirror-interval", help="Seconds between mirror updates."
    ),
    mirror_checksum: Optional[bool] = typer.Option(
        False,
        "-k",
        "--mirror-checksum",
        help="Compute checksums to determine which files have changed (default uses modified times and size).",
    ),
    mirror_quiet: Optional[bool] = typer.Option(
        False,
        "-q",
        "--mirror-quiet",
        help="Do not print confirmations after a successful mirror cycle",
    ),
    opt: str = typer.Argument(
        ...,
        autocompletion=complete_codename,
        help="IP:PORT or codename to mount.",
    ),
    mountpoint: Optional[str] = typer.Argument(
        None,
        help="If given, custom location to mount the fs.",
    ),
):
    """Securely mount a network share exported by another player

    Generally, the creator of the share will provide you with the command you need to run.
    """

    codename_pattern = re.compile("^[a-z_]+$")
    fs = None
    # The :PORT should always be given, so this should work well
    if codename_pattern.match(opt):
        # codename
        codename = opt
        fs = Mount.load(payload_dir, codename)
    else:
        # IP:PORT
        host, port = split_host_port(opt)

        fs = Mount(
            host=host,
            port=port,
            mountpoint=mountpoint,
        )
        try:
            spec, codename = fs.save(payload_dir, codename=codename, cmd="fs")
        except FileExistsError as e:
            typer.echo(f"{e} | Loading existing config...")
            fs = Mount.load(payload_dir, codename)

    # Mountpoint priority:
    # 1. Explict cli arg
    # 2. From yaml
    # 3. Default
    if mountpoint is None:
        mountpoint = fs.mountpoint
    if mountpoint is None:
        mountpoint = Path(payload_dir) / codename / "mnt"

    Path(mountpoint).mkdir(parents=True, exist_ok=True)

    typer.echo(f"[+] autovnet rtfm fs mount {codename}")
    typer.echo(f"[+] mountpoint: {mountpoint}")

    sshfs_cmd = [
        "sshfs",
        "-f",
        "-o",
        "StrictHostKeyChecking=no",
        "-o",
        "UserKnownHostsFile=/dev/null",
        "-o",
        "IdentitiesOnly=yes",
        "-o",
        "ServerAliveInterval=30",
        "-o",
        "ServerAliveCountMax=5",
        "-o",
        "PasswordAuthentication=no",
        "-o",
        "auto_cache",
        "-o",
        "reconnect",
        "-o",
        "cache_timeout=5",
        "-o",
        "Compression=yes",
        "-o",
        f"IdentityFile={avn.config.rtfm_creds.key}",
        "-p",
        str(fs.port),
        f"x@{fs.host}:/fs",
        str(mountpoint),
    ]

    if mirror_path is None:
        mirror_path = str(Path(payload_dir) / codename / "mirror")

    rsync_src = str(mountpoint)
    if not rsync_src.endswith("/"):
        rsync_src += "/"

    rsync_cmd = [
        "rsync",
        "-azq",
        rsync_src,
        mirror_path,
        "--delete",
    ]
    if mirror_checksum:
        rsync_cmd.append("--checksum")
    if mirror:
        Path(mirror_path).mkdir(parents=True, exist_ok=True)
        typer.echo(f"[+] mirror every {mirror_interval}s to {mirror_path}")

    sshfs_proc = None
    rsync_proc = None
    try:
        sshfs_proc = subprocess.Popen(sshfs_cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        try:
            res = sshfs_proc.wait(timeout=5)
            raise Exception(f"[-] sshfs exited with code {res}: {shlex.join(sshfs_cmd)}")
        except subprocess.TimeoutExpired:
            while mirror and sshfs_proc.poll() is None:
                rsync_proc = subprocess.Popen(rsync_cmd)
                ret = rsync_proc.wait()
                if ret != 0:
                    typer.echo(f"[!] [{timestamp()}] Failed mirror: rsync returned {ret}")
                elif not mirror_quiet:
                    typer.echo(f"[{timestamp()}] Successful mirror to {mirror_path}")
                time.sleep(mirror_interval)

        sshfs_proc.wait()
    finally:
        for proc in [rsync_proc, sshfs_proc]:
            if proc is not None and proc.poll() is None:
                proc.terminate()
                proc.wait()
