import os
import subprocess
import gzip
import shlex
import shutil
from typing import Optional, List
from pathlib import Path

import typer

from ...autovnet import AutoVnet, avn
from autovnet.models import Mount
from autovnet.utils import (
    compose_up_ctx,
    complete_codename,
    rand_port,
    wait_url_open,
    open_url,
    ip_gen,
    gen_codename,
    gen_password,
    split_host_port,
)


DEFAULT_HTTP_PORT = 1980
DEFAULT_RTMP_PORT = 1935
MOUNT_TYPE = "owncast"

app = typer.Typer()


@app.command()
def export(
    networks: Optional[List[str]] = typer.Option(
        None, "-n", "--network", help="Simulated network to draw a random IP from."
    ),
    http_dest_ip: Optional[str] = typer.Option(
        "127.0.0.1",
        "-d",
        "--http-dest-ip",
        help="[ADVANCED] Bind address for the server UI (for watching streams) (anything but 127.X.X.X may allow anyone to access!).",
    ),
    rtmp_dest_ip: Optional[str] = typer.Option(
        "127.0.0.1",
        "-d",
        "--rtmp-dest-ip",
        help="[ADVANCED] Bind address for the server RTMP (for publishing a stream to owncast) (anything but 127.X.X.X may allow anyone to access!).",
    ),
    http_dest_port: Optional[int] = typer.Option(
        DEFAULT_HTTP_PORT, "-p", "--http-dest-port", help="Local bind port for the server."
    ),
    rtmp_dest_port: Optional[int] = typer.Option(
        DEFAULT_RTMP_PORT, "-P", "--rtmp-dest-port", help="Local bind port for the server."
    ),
    no_browser: Optional[bool] = typer.Option(
        False, "-N", "--no-browser", help="If set, do not open the default browser."
    ),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(MOUNT_TYPE), "-D", "--payload-dir", help="Directory to save command metadata."
    ),
    codename: Optional[str] = typer.Argument(
        None,
        autocompletion=complete_codename,
        help="Previous codename to load",
    ),
):
    """Run a self-hosted instance of owncast"""

    proto = "http"
    url = f"{proto}://{http_dest_ip}:{http_dest_port}"

    # Eww, gross, but none of the rest of the internals expect 2 ports
    # We also need 2 stunnel processes anyway, so this isn't _that_ unreasonable.
    # For now, any svc that needs multiple ports must have a finite number of with blocks
    http_ctx = None
    rtmp_ctx = None

    if codename is None:
        # New svc
        # We need both remote IPs and codenames to match
        remote_ip = next(ip_gen(avn.networks(networks)))
        codename = gen_codename()
        # New svc
        http_ctx = lambda: avn.svc(
            networks=[remote_ip],
            dest_ip=http_dest_ip,
            dest_port=http_dest_port,
            payload_dir=payload_dir,
            echo=False,
            codename=codename,
            suffix="http",
        )
        rtmp_ctx = lambda: avn.svc(
            networks=[remote_ip],
            dest_ip=rtmp_dest_ip,
            dest_port=rtmp_dest_port,
            payload_dir=payload_dir,
            echo=False,
            codename=codename,
            suffix="rtmp",
        )

    else:
        # Loaded svc
        http_ctx = lambda: avn.svc_from_codename(codename, payload_dir=payload_dir, echo=False, suffix="http")
        rtmp_ctx = lambda: avn.svc_from_codename(codename, payload_dir=payload_dir, echo=False, suffix="rtmp")

        # Peek at listener config to grab the values we need
        ln = avn.listener_from_codename(codename, payload_dir=payload_dir, suffix="http")
        model = ln.lns[0]
        http_dest_ip = model.next_dest_ip
        http_dest_port = model.next_dest_port

        ln = avn.listener_from_codename(codename, payload_dir=payload_dir, suffix="rtmp")
        model = ln.lns[0]
        rtmp_dest_ip = model.next_dest_ip
        rtmp_dest_port = model.next_dest_port

    docker_path = avn.docker_path(f"plugins/{MOUNT_TYPE}/")

    data_dir = Path(payload_dir) / codename / "data"
    for _dir in [data_dir]:
        _dir.mkdir(parents=True, exist_ok=True)

    stream_key_file = data_dir / "stream.key"
    stream_key = None
    if stream_key_file.is_file():
        stream_key = stream_key_file.read_text()
    else:
        stream_key = gen_password(secure=True)
        stream_key_file.write_text(stream_key)

    with http_ctx() as (http_proc, _, http_remote_addr):
        with rtmp_ctx() as (rtmp_proc, _, rtmp_remote_addr):
            tmp_env = {
                "OWNCAST_HTTP_BIND": f"{http_dest_ip}:{http_dest_port}",
                "OWNCAST_RTMP_BIND": f"{rtmp_dest_ip}:{rtmp_dest_port}",
                "OWNCAST_DATA": str(data_dir),
            }
            env = tmp_env
            env["UID"] = str(os.geteuid())
            env["GID"] = str(os.getegid())

            project = f"{MOUNT_TYPE}_{codename}"
            remote_ip, http_remote_port = split_host_port(http_remote_addr)
            _, rtmp_remote_port = split_host_port(rtmp_remote_addr)

            mount_cmd = (
                f"autovnet rtfm {MOUNT_TYPE} mount -C {codename} {remote_ip} {http_remote_port} {rtmp_remote_port}"
            )

            if http_dest_port != DEFAULT_HTTP_PORT:
                mount_cmd += f" -p {http_dest_port}"

            if rtmp_dest_port != DEFAULT_RTMP_PORT:
                mount_cmd += f" -P {rtmp_dest_port}"

            export_cmd = f"autovnet rtfm {MOUNT_TYPE} export {codename}"

            rtmp_url = f"rtmp://127.0.0.1:{rtmp_dest_port}/live"

            typer.echo(f"[+] {export_cmd}")
            typer.echo(f"[+] ========== viewers ==========")
            typer.echo(f"[+] connect : xdg-open {url}")
            typer.echo(f"[+] mount   : {mount_cmd}")

            typer.echo(f"[+] ========== streamers ==========")
            typer.echo(f"[+] configure  : xdg-open {url}/admin")
            typer.echo(f"[+] user       : admin")
            typer.echo(f"[+] key / pass : {stream_key}")
            typer.echo(f"[+] stream     : {rtmp_url}")

            with compose_up_ctx(docker_path, env=env, project=project, quiet=True) as (
                compose_thread,
                compose_stop,
            ):
                typer.echo(f"[*] Waiting for {MOUNT_TYPE} ...")
                ok = wait_url_open(url, proc=http_proc, thread=compose_thread)
                if ok:
                    typer.echo(f"[+] {MOUNT_TYPE} is ready!")
                elif ok is False:
                    typer.echo(f"[!] {MOUNT_TYPE} may still be initializing")

                if not no_browser:
                    open_url(url)

                compose_thread.join()


@app.command()
def mount(
    dest_ip: Optional[str] = typer.Option(
        "127.0.0.1",
        "-d",
        "--dest-ip",
        help="[ADVANCED] Destination IP you will use to access the service. Anything but 127.X.X.X may be insecure!",
    ),
    http_dest_port: Optional[int] = typer.Option(
        DEFAULT_HTTP_PORT,
        "-p",
        "--http-dest-port",
        help="[ADVANCED] Destination port to mount http.",
    ),
    rtmp_dest_port: Optional[int] = typer.Option(
        DEFAULT_RTMP_PORT,
        "-P",
        "--rtmp-dest-port",
        help="[ADVANCED] Destination port to mount rtmp.",
    ),
    no_browser: Optional[bool] = typer.Option(
        False, "-N", "--no-browser", help="If set, do not open the default browser."
    ),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path(MOUNT_TYPE), "-D", "--payload-dir", help="Directory to save command metadata."
    ),
    codename: str = typer.Option(None, "-C", "--set-codename", help="Codename for the mount."),
    remote_ip: Optional[str] = typer.Argument(
        None,
        autocompletion=complete_codename,
        help="Remote IP for service.",
    ),
    http_remote_port: Optional[int] = typer.Argument(
        None,
        autocompletion=complete_codename,
        help="Remote PORT for http.",
    ),
    rtmp_remote_port: Optional[int] = typer.Argument(
        None,
        autocompletion=complete_codename,
        help="Remote PORT for rtmp.",
    ),
):
    """Mount and connect to another player's owncast server"""

    http_remote = None
    rtmp_remote = None

    if no_browser:
        proto = None

    if remote_ip is not None:
        if any(x is None for x in [http_remote_port, rtmp_remote_port]):
            raise ValueError(f"[-] remote_ip requires HTTP and RTMP ports!")
        http_remote = f"{remote_ip}:{http_remote_port}"
        rtmp_remote = f"{remote_ip}:{rtmp_remote_port}"

    # Massage our weird double-port shenanigans into 2 separate calls to our naive helper
    # Toggle codename "off" and opt "on" if this is a new svc (and vice-versa)
    http_codename = None
    http_opt = http_remote
    if http_remote is None:
        # Existing svc
        http_opt = codename
    else:
        # New svc, use --set-codename
        http_codename = codename

    rtmp_codename = None
    rtmp_opt = rtmp_remote
    if rtmp_remote is None:
        # Existing svc
        rtmp_opt = codename
    else:
        # New svc, use --set-codename
        rtmp_codename = codename

    proto = None
    if not no_browser:
        proto = "http"

    cmd_str = f"autovnet rtfm {MOUNT_TYPE} mount -C {codename}"

    typer.echo(f"[+] {cmd_str}")

    with avn.svc_mount(
        codename=rtmp_codename,
        opt=rtmp_opt,
        dest_ip=dest_ip,
        dest_port=rtmp_dest_port,
        payload_dir=payload_dir,
        mount_type=MOUNT_TYPE,
        suffix="rtmp",
        wait_tcp=True,
        echo=False,
        echo_ready=False,
        echo_mount=False,
    ) as rtmp_proc:
        with avn.svc_mount(
            codename=http_codename,
            opt=http_opt,
            dest_ip=dest_ip,
            dest_port=http_dest_port,
            payload_dir=payload_dir,
            xdg_open=proto,
            mount_type=MOUNT_TYPE,
            suffix="http",
            echo=False,
            echo_mount=False,
        ) as http_proc:
            http_proc.wait()
            rtmp_proc.wait()
