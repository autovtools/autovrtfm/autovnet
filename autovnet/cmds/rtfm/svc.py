import re

from pathlib import Path
from typing import Optional, List
import typer

from ...autovnet import AutoVnet, avn
from autovnet.models import Mount
from autovnet.utils import (
    compose_up,
    compose_down,
    read_env_file,
    complete_codename,
    rand_port,
    as_port,
    split_host_port,
    wait_container_exit,
)

app = typer.Typer()


@app.command()
def export(
    networks: Optional[List[str]] = typer.Option(
        None, "-n", "--network", help="Simulated network to draw a random IP from."
    ),
    infra_category: Optional[str] = typer.Option(
        "svc", "-I", "--infra-category", help="[ADVANCED] Category of infrastructure."
    ),
    count: Optional[int] = typer.Option(1, "-c", "--count", help="The number of remote IP addresses to allocate."),
    dest_ip: Optional[str] = typer.Option("127.0.0.1", "-d", "--dest-ip", help="Destination IP for tunnelled traffic."),
    dest_port: Optional[int] = typer.Option(
        None, "-p", "--dest-port", help="Destination port for tunnelled traffic (default: random)."
    ),
    env_file: Optional[str] = typer.Option(
        avn.docker_path("plugins/stunnel/stunnel.env"),
        "-e",
        "--env-file",
        help="[ADVANCED] Environment file to pass to docker-compose",
    ),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path("svc"), "-D", "--payload-dir", help="Directory to save command metadata."
    ),
    set_codename: Optional[str] = typer.Option(
        None,
        "-C",
        "--set-codename",
        autocompletion=complete_codename,
        help="[ADVANCED] Explictly set codename to this value.",
    ),
    set_suffix: Optional[str] = typer.Option(
        "",
        "--set-suffix",
        help="[ADVANCED] Set internal codename suffix.",
    ),
    no_deconflict: Optional[bool] = typer.Option(
        False, "-N", "--no-deconflict", help="[ADVANCED] Do not deconflict public IP assignment."
    ),
    opt: Optional[str] = typer.Argument(
        None,
        autocompletion=complete_codename,
        help="Codename or remote port to use.",
    ),
):
    """Securely export (expose) a local service to other players

    Only other autovnet players with the shared TLS cert can connect.

    The SSH tunnel redirects traffic from autovnet to a local stunnel instance (random local port), which provides mTLS.
    Then, the local stunnel instance then redirects the "raw" TCP traffic to the provided dest_ip / dest_port.
    """
    stunnel_dest_ip = "127.1.33.7"
    stunnel_dest_port = rand_port()
    if dest_port is None:
        dest_port = rand_port()

    env = read_env_file(env_file)
    ln = None
    remote_port = None
    if opt is None:
        remote_port = rand_port()
    else:
        try:
            remote_port = as_port(opt)
            # port
        except ValueError as e:
            pass
    if remote_port is None:
        # codename
        ln = avn.listener_from_codename(opt, payload_dir=payload_dir, echo=False, suffix=set_suffix)
        for model in ln.lns:
            if model.next_dest_ip is None:
                model.next_dest_ip = dest_ip
            if model.next_dest_port is None:
                model.next_dest_port = dest_port
        # Probably a bad idea that will cause weird future bugs (:
        ln.save(payload_dir, exist_ok=True, suffix=set_suffix)
    else:
        remote_ports = [remote_port] * count
        networks = avn.networks(networks, category=infra_category)
        ln = avn.listener(
            networks,
            remote_ports,
            stunnel_dest_ip,
            dest_port=stunnel_dest_port,
            echo=False,
            codename=set_codename,
            deconflict=(not no_deconflict),
        )
        for model in ln.lns:
            model.next_dest_ip = dest_ip
            model.next_dest_port = dest_port
        ln.save(payload_dir, suffix=set_suffix)

    # Bad assumption
    stunnel_dest_ip = ln.lns[0].dest_ip
    stunnel_dest_port = ln.lns[0].dest_port
    dest_ip = ln.lns[0].next_dest_ip
    dest_port = ln.lns[0].next_dest_port

    # STUNNEL_ACCEPT is internal - user does not control it
    # STUNNEL_CONNECT sends the unwrapped traffic to the user-specified destination
    tmp_env = {
        "AUTOVRTFM_CERT": str(avn.config_path("rtfm.cert")),
        "AUTOVRTFM_KEY": str(avn.config_path("rtfm.key")),
        "STUNNEL_ACCEPT": f"{stunnel_dest_ip}:{stunnel_dest_port}",
        "STUNNEL_CONNECT": f"{dest_ip}:{dest_port}",
        "STUNNEL_CLIENT": "no",
    }

    # Allow hardcore users to do what they want by crafting an env file
    tmp_env.update(env)
    env = tmp_env

    project = f"svc_export_{ln.codename}"
    set_suffix_arg = ""
    if set_suffix:
        project += f"_{set_suffix}"
        set_suffix_arg = f" --set-suffix {set_suffix}"

    docker_path = avn.docker_path("plugins/stunnel/")
    try:
        with ln.listener():
            cmd_str = f"autovnet rtfm svc export{set_suffix_arg} {ln.codename}"
            typer.echo(f"[+] {cmd_str}")
            typer.echo(f"[+] exporting: {dest_ip}:{dest_port}")
            for model in ln.lns:
                typer.echo(
                    f"[+] mount: autovnet rtfm svc mount -C {ln.codename}{set_suffix_arg} {model.remote_ip}:{model.remote_port}"
                )
                compose_up(docker_path, env=env, project=project, quiet=True)
    finally:
        if not wait_container_exit(project):
            raise Exception(f"[-] Containers for {project} did not exit!")


@app.command()
def mount(
    dest_ip: Optional[str] = typer.Option(
        "127.0.0.1", "-d", "--dest-ip", help="Destination IP you will use to access the service."
    ),
    dest_port: Optional[int] = typer.Option(
        None, "-p", "--dest-port", help="Destination port you will use to access the service (default: random)."
    ),
    env_file: Optional[str] = typer.Option(
        avn.docker_path("plugins/stunnel/stunnel.env"),
        "-e",
        "--env-file",
        help="[ADVANCED] Environment file to pass to docker-compose",
    ),
    payload_dir: Optional[str] = typer.Option(
        avn.data_path("svc"), "-D", "--payload-dir", help="Directory to save command metadata."
    ),
    codename: Optional[str] = typer.Option(None, "-C", "--set-codename", help="Set the codename for the mount."),
    mount_type: Optional[str] = typer.Option(None, "-m", "--mount-type", help="[ADVANCED] Mount type."),
    set_suffix: Optional[str] = typer.Option(
        "",
        "--set-suffix",
        help="[ADVANCED] Set internal codename suffix.",
    ),
    opt: str = typer.Argument(
        ...,
        autocompletion=complete_codename,
        help="IP:PORT or codename to mount.",
    ),
):
    """Securely mount another player's shared service

    Mounting a service means binding locally to a (localhost) port.
    Packets you send to the local port will be
    1. wrapped in TLS, using the rtfm.key
    2. sent through the autovnet server
    3. reach the other player's 'svc export' instance
    4. be authenticated (by rtfm.key)
    5. TLS unwrapped
    6. forwared to the "local" service that the other player exported

    So, you use the standard client for the service (e.g. a web browser) pointing at your "local" end of the tunnel,
    and the "local" service of the other player will get your requests.

    On the wire, it's encrypted with TLS, and more importantly, _only_ other autovnet players that have the rtfm.key
    can ride the tunnel to reach the other player's service.

    So, this is how you should securely expose any "autovnet player only" services, like tools for collaboration
    """

    if dest_port is None:
        dest_port = rand_port()
    codename_pattern = re.compile("^[a-z_]+$")
    mnt = None
    # The :PORT should always be given, so this should work well
    if codename_pattern.match(opt):
        # codename
        codename = opt
        mnt = Mount.load(payload_dir, codename, suffix=set_suffix)
    else:
        # IP:PORT
        host, port = split_host_port(opt)

        mnt = Mount(host=host, port=port, svc_host=dest_ip, svc_port=dest_port)
        try:
            if mount_type is None:
                mount_type = "svc"
            spec, _codename = mnt.save(payload_dir, codename=codename, cmd=mount_type, suffix=set_suffix)
            codename = _codename
        except FileExistsError as e:
            if codename is None:
                raise
            typer.echo(f"{e} | Loading existing config...")
            mnt = Mount.load(payload_dir, codename, suffix=set_suffix)

    env = read_env_file(env_file)

    # STUNNEL_ACCEPT is the local end of the tunnel that this player will connect to
    # STUNNEL_CONNECT is the autovnet IP from the other player's export
    tmp_env = {
        "AUTOVRTFM_CERT": str(avn.config_path("rtfm.cert")),
        "AUTOVRTFM_KEY": str(avn.config_path("rtfm.key")),
        "STUNNEL_ACCEPT": f"{mnt.svc_host}:{mnt.svc_port}",
        "STUNNEL_CONNECT": f"{mnt.host}:{mnt.port}",
        "STUNNEL_CLIENT": "yes",
    }

    # Allow hardcore users to do what they want by crafting an env file
    tmp_env.update(env)
    env = tmp_env

    project = f"svc_mount_{codename}"
    if set_suffix:
        project += f"_{set_suffix}"
    docker_path = avn.docker_path("plugins/stunnel/")
    typer.echo(f"[+] autovnet rtfm svc mount {codename}")
    typer.echo(f"[+] mounted locally: {mnt.svc_host}:{mnt.svc_port}")
    try:
        compose_up(docker_path, env=env, project=project, quiet=True)
    finally:
        if not wait_container_exit(project):
            raise Exception(f"[-] Containers for {project} did not exit!")
