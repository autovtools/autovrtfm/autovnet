#!/bin/bash

set -eu

wait_db()
{
    while ! 2>/dev/null echo -n > "/dev/tcp/${POSTGRES_HOST}/${POSTGRES_PORT}"; do
        echo "Waiting for ${POSTGRES_HOST}..."
        sleep 1
    done
}

start_authority()
{
    PDNS_CONFIG="/etc/powerdns/pdns.conf"
    PDNS_CONFIG_TEMPLATE="${PDNS_CONFIG}.template"

    envsubst < "${PDNS_CONFIG_TEMPLATE}" > "${PDNS_CONFIG}"
    ls -alh "${PDNS_CONFIG}"
    cat "${PDNS_CONFIG}"
    exec pdns_server $@
}

start_recursor()
{
    PDNS_CONFIG="/etc/powerdns/recursor.conf"
    PDNS_CONFIG_TEMPLATE="${PDNS_CONFIG}.template"

    export AVN_AUTHORITY="$(dig "${AVN_AUTHORITY}" +short):${AVN_AUTHORITY_PORT}"
    envsubst < "${PDNS_CONFIG_TEMPLATE}" > "${PDNS_CONFIG}"
    ls -alh "${PDNS_CONFIG}"
    cat "${PDNS_CONFIG}"
    exec pdns_recursor $@
}

wait_db

if [ "${PDNS_MODE}" = "authority" ]; then
    start_authority
elif [ "${PDNS_MODE}" = "recursor" ]; then
    start_recursor
else
    echo "Mode ${PDNS_MODE} invalid"
    exit 1
fi
