# Level 2

## Connecting

```{: .host .host-title}
ssh -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -p 2220 bandit1@bandit.labs.overthewire.org
```

```{: .check .check-title .no-copy}
...
bandit1@bandit.labs.overthewire.org's password:
```

Enter the password:
```{: .target .target-title}
NH2SXQwcBdpmTEzi3bvBHMM9H66vVXjL
```

You should successfully log in.
```{: .check .check-title .no-copy}
...
bandit1@bandit:~$
```


## Winning

```{: .target .target-title}
cat < -
```

```{: .check .check-title .no-copy}
rRGizSaX8Mk1RTb1CNQoXTcYZWU6lgzi
```
