# Examples

This is regular text.

## This is a header

**bold** _italics_ `inline code`

### A

Add more `#` to create smaller headers

### B

* This is a bulleted list
    * With sub-bullets
        * foo
        * bar

* More understand edge final grow certain difficult.
* Suffer more smile whose upon soldier.
* Indeed above will them idea entire them.
* Radio general vote cut rock group candidate.


#### Foo

This is a quote:

> Cause shake anything artist threat actually within.
> Energy short policy morning race thousand. Song term spring million grow notice break.
> Model south author outside choice position until. Suddenly husband rich through. Possible hard result certainly they.
> Admit police story economy. Whole loss very hour arrive southern.

#### Bar

bar

### C

C is for `code`!

```
print("This is a code block")
```

```python
print("This is a python code block")
```

```bash
echo "This is a bash code block"
```

## Custom Formatting

`pasta` includes some extra formatting tricks that are not supported by normal `markdown`

### Custom Code Blocks

`pasta` includes special code blocks that makes it possible to color-coordinate your instructions.

```{: .host .host-title}
echo "This is code you would run on your system"
``` 

```{: .target .target-title}
echo "This is code you would run on a target / victim system"
``` 

```{: .msf .msf-title}
echo "This is code you would run in metasploit"
``` 

```{: .sliver .sliver-title}
echo "This is code you would run in sliver"
``` 

```{: .merlin .merlin-title}
echo "This is code you would run in merlin"
``` 

```{: .discord .discord-title}
This is a message you could paste in discord
``` 

```{: .check .check-title .no-copy}
This is example output.
The copy button is disabled.
``` 

### Custom Notes

!!! note
    This is a note

    You need to tab like this

!!! warning
    This is a warning

    DANGER DANGER

??? hint "spoiler / hint: click to expand!"
    Hint: the answer is 42


### Variable Substitution

Your pasta can be customized automatically when it is rendered (with the pasta `serve` subcommand)

This works via [jinja](https://jinja.palletsprojects.com/en/3.1.x/)-style variable syntax.

Here are some examples:

#### From Environment

Any variable in your environment can be used in your `pasta`.

* Your username is `{{USER}}`
* Your shell is `{{SHELL}}`

#### From `pasta.env`

If a directory contains a file named `pasta.env`,
any variables it defines can be accessed from any file in the directory,
and from any file in any subdirectory


These variables were from a `pasta.env`:

* `{{PASTA_ENV_MSG}}`
* This is your `uname -a`: `{{UNAME_VAR}}`
