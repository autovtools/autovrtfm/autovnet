FROM debian:testing-slim
ARG UID
ARG GID
ARG FOCALBOARD_VERSION

RUN apt update && DEBIAN_FRONTEND=noninteractive apt install --no-install-recommends -y \
    gettext-base \
    curl \
    wget \
    ca-certificates \
    && \
    rm -rf /var/lib/apt/lists/*

RUN mkdir -p /opt/focalboard && \
    chown "${UID}:${GID}" /opt/focalboard && \
    cd /opt/ && \
    wget https://github.com/mattermost/focalboard/releases/download/${FOCALBOARD_VERSION}/focalboard-server-linux-amd64.tar.gz && \
    tar xf focalboard-*.tar.gz && \
    rm -f focalboard-*.tar.gz && \
    chown -R "${UID}:${GID}" /opt/focalboard

COPY docker-entrypoint.sh /docker-entrypoint.sh
COPY config.json /etc/focalboard/config.json.template

RUN chmod 0500 /docker-entrypoint.sh && \
    chown "${UID}:${GID}" /docker-entrypoint.sh && \
    touch /etc/focalboard/config.json && \
    chown "${UID}:${GID}" /etc/focalboard/config.json && \
    chmod 0600 /etc/focalboard/config.json
    
WORKDIR /opt/focalboard/
USER nobody
ENTRYPOINT ["/docker-entrypoint.sh"]
