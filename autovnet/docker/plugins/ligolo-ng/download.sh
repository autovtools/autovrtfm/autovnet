#!/bin/bash

# Uses release naming convention from >= v0.4.4
set -euo pipefail
LIGOLO_NG_VERSION="${1:-latest}"
LIGOLO_AGENT_BUILDS=${LIGOLO_AGENT_BUILDS:-linux_amd64 windows_amd64 darwin_amd64}
LIGOLO_PROXY_BUILDS=${LIGOLO_PROXY_BUILDS:-linux_amd64}
LIGOLO_NG_URL="${LIGOLO_NG_URL:-https://github.com/nicocha30/ligolo-ng}"
RELEASE_URL="${RELEASE_URL:-}"

if [ -z "${RELEASE_URL:-}" ]; then
    RELEASE="releases/download/${LIGOLO_NG_VERSION}"
    if [ "${LIGOLO_NG_VERSION}" = "latest" ]; then
        RELEASE="releases/latest/download"
    fi
    RELEASE_URL="${LIGOLO_NG_URL}/${RELEASE}"
fi

# resolve redirects
RELEASE_URL="$(curl -Ls -o /dev/null -w %{url_effective} "${RELEASE_URL}")"
# parse vX.X.X from end of url to X.X.X
RELEASE_VERSION=$(echo "${RELEASE_URL}" | awk -F/ '{print $NF}' | awk -Fv '{print $2}')

# windows agents are released as `.zip` and have .exe, everything else is .tar.gz and no extension
for build in ${LIGOLO_PROXY_BUILDS}; do
    ext=".tar.gz"
    exe=""
    if [[ "${build}" =~ "windows_" ]]; then
        ext=".zip"
        exe=".exe"
    fi
    echo "${build} ${ext}"
    bin_name="ligolo-ng_proxy_${RELEASE_VERSION}_${build}${ext}"
    wget "${RELEASE_URL}/${bin_name}"
    if [ "${ext}" = ".zip" ]; then
        unzip -n "${bin_name}"
        mv proxy.exe proxy
    else
        tar --skip-old-files -xf "${bin_name}"
    fi
    mv proxy "proxy.${build}${exe}"
    rm "${bin_name}"
done

for build in ${LIGOLO_AGENT_BUILDS}; do
    ext=".tar.gz"
    exe=""
    if [[ "${build}" =~ "windows_" ]]; then
        ext=".zip"
        exe=".exe"
    fi
    echo "${build} ${ext}"
    bin_name="ligolo-ng_agent_${RELEASE_VERSION}_${build}${ext}"
    wget "${RELEASE_URL}/${bin_name}"
    if [ "${ext}" = ".zip" ]; then
        unzip -n "${bin_name}"
        mv agent.exe agent
    else
        tar --skip-old-files -xf "${bin_name}"
    fi
    mv agent "agent.${build}${exe}"
    rm "${bin_name}"
done
