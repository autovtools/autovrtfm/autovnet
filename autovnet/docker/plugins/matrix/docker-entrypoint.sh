#!/bin/bash
set -eu

MATRIX_CONFIG="${MATRIX_CONFIG:-/matrix/config/dendrite.yaml}"
MATRIX_CONFIG_TEMPLATE="${MATRIX_CONFIG_TEMPLATE:-/matrix/config/dendrite.yaml.template}"

MATRIX_KEY="${MATRIX_KEY:-/matrix/certs/matrix_key.pem}"
MATRIX_TLS_KEY="${MATRIX_TLS_KEY:-/matrix/certs/server.key}"
MATRIX_TLS_CERT="${MATRIX_TLS_CERT:-/matrix/certs/server.crt}"

POSTGRES_HOST="${POSTGRES_HOST:-db}"
POSTGRES_PORT="${POSTGRES_PORT:-5432}"

if [ ! -f "${MATRIX_KEY}" ]; then
    generate-keys --private-key "${MATRIX_KEY}"
fi

if [ ! -f "${MATRIX_TLS_KEY}" ] || [ ! -f "${MATRIX_TLS_CERT}" ]; then
    generate-keys --tls-cert "${MATRIX_TLS_CERT}" --tls-key "${MATRIX_TLS_KEY}"
fi

if [ ! -f "${MATRIX_CONFIG}" ]; then
    envsubst < "${MATRIX_CONFIG_TEMPLATE}" > "${MATRIX_CONFIG}"
    rm -f "${MATRIX_CONFIG_TEMPLATE}"
fi

while ! 2>/dev/null echo -n > "/dev/tcp/${POSTGRES_HOST}/${POSTGRES_PORT}"; do
    echo "Waiting for ${POSTGRES_HOST}..."
    sleep 1
done

# If doing an --init-sql, we need to wait for the import to complete before matrix starts using the database
if [ "${INIT_SQL_SLEEP}" != "0" ]; then
    echo "Waiting ${INIT_SQL_SLEEP}s for import..."
    sleep "${INIT_SQL_SLEEP}"
fi

exec /usr/bin/dendrite-monolith-server --tls-cert "${MATRIX_TLS_CERT}" --tls-key "${MATRIX_TLS_KEY}" --config "${MATRIX_CONFIG}" --really-enable-open-registration
