#!/bin/bash

set -euo pipefail
MERLIN_VERSION="${1}"
ZIP_PASS="merlin"
URL="https://github.com/Ne0nd0g/merlin/"
# Download merlin server + all agents into current directory

RELEASE="releases/download/${MERLIN_VERSION}/"
if [ "${MERLIN_VERSION}" = "latest" ]; then
    RELEASE="releases/latest/download/"
fi
# WIP
# the Server (linux) and all the agents are now bundled together in one .7z
#for bin in Server-Linux-x64 ; do
for bin in Server-Linux-x64 Agent-Linux-x64 Agent-Windows-x64 Agent-Darwin-x64; do
    wget "${URL}${RELEASE}merlin${bin}.7z"
    7z x "-p${ZIP_PASS}" "merlin${bin}.7z"
    # all the agent binaries
    #mv ./data/bin/* .
    rm "merlin${bin}.7z"
done
