#!/bin/sh

# Simple helper script sourced after initdb is called.
# If user only wanted to run initdb, then exit before the database is started
set -e
if [ ! -z "${INITDB_ONLY}" ]; then
    echo "INITDB_ONLY: exiting"
    exit 1
fi
