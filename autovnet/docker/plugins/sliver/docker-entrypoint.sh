#!/bin/bash

set -euo pipefail

wait_tcp() {
    while ! 2>/dev/null echo -n > "/dev/tcp/${1}/${2}"; do
        echo "Waiting for ${1}:${2}..."
        sleep 1
    done
    echo "[+] ${1}:${2} ready"
}

SLIVER_MODE=${SLIVER_MODE:-server}
SLIVER_USER=${SLIVER_USER:-default}
MULTIPLAYER_HOST="${MULTIPLAYER_HOST:-127.0.0.1}"
MULTIPLAYER_PORT="${MULTIPLAYER_PORT:-31337}"

BIN_DIR="/opt/sliver-bin"
SLIVER="/opt/sliver"
LNK_DIR="${SLIVER}/.sliver"

for d in .sliver .sliver-client; do
    # The image contains the default contents of src
    src="${HOME}/${d}"
    dst="${SLIVER}/${d}"
    if [ ! -d "${dst}" ]; then
        # If this is the first boot, move the defaults to the shared dir
        mv "${src}" "${dst}"
    else
        # If this is a subsequent boot, the image's defaults need to be deleted
        rm -rf "${src}"
    fi
    # Link the active dir (shared) to the path sliver expects it
    ln -s "${dst}" "${src}"
done

if [ "${SLIVER_MODE}" = "config" ]; then
    exec "${BIN_DIR}/sliver-server" operator \
        --name "${SLIVER_USER}" \
        --lhost "${MULTIPLAYER_HOST}" \
        --lport "${MULTIPLAYER_PORT}" \
        --save "${OUT_DIR}/sliver.json"
    exit 1
fi

if [ ! -d "${LNK_DIR}/go" ]; then
    # These resources are massive (~ 0.75 GB), so we try to keep them within the image
    ln -s "${HOME}/.sliver-go" "${LNK_DIR}/go"
fi

if [ "${SLIVER_MODE}" = "server" ]; then
    # sliver's implementation of postgres support (at leastas of v1.5.22) is super broken
    #SLIVER_DB_CONFIG_TEMPLATE="/etc/sliver/database.json.template"
    #SLIVER_DB_CONFIG="${LNK_DIR}/configs/database.json"

    #if [ ! -f "${SLIVER_DB_CONFIG}.done" ]; then
    #    envsubst < "${SLIVER_DB_CONFIG_TEMPLATE}" > "${SLIVER_DB_CONFIG}"
    #    touch "${SLIVER_DB_CONFIG}.done"
    #fi

    CLIENT_CONFIGS="${SLIVER}/.sliver-client/configs"
    mkdir -p "${CLIENT_CONFIGS}"
    if [ ! -f "${CLIENT_CONFIGS}/default.json" ]; then
        "${BIN_DIR}/sliver-server" operator \
            --name "${SLIVER_USER}" \
            --lhost "${MULTIPLAYER_HOST}" \
            --lport "${MULTIPLAYER_PORT}" \
            --save "${CLIENT_CONFIGS}/default.json"
    fi

    #wait_tcp "${POSTGRES_HOST}" "${POSTGRES_PORT}"
    exec "${BIN_DIR}/sliver-server" daemon --lhost "${MULTIPLAYER_HOST}" --lport "${MULTIPLAYER_PORT}"

elif [ "${SLIVER_MODE}" = "client" ]; then
    wait_tcp "${MULTIPLAYER_HOST}" "${MULTIPLAYER_PORT}"
    exec "${BIN_DIR}/sliver-client"
fi
