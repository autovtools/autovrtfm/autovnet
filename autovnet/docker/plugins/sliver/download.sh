#!/bin/bash
set -euo pipefail

# Modified from
# https://sliver.sh/install

#
# Download and Unpack Sliver Server
#
SERVER_INSTALL=/usr/local/bin/sliver-server
BIN_PATH="/opt/sliver-bin"
SLIVER_VERSION="${SLIVER_VERSION:-latest}"
SLIVER_SERVER='sliver-server_linux'
SLIVER_CLIENT='sliver-client_linux'
ARTIFACTS="sliver-client_linux sliver-server_linux"
BASE_URL="https://github.com/BishopFox/sliver/releases"

RELEASES_URL="${BASE_URL}/latest/download"
if [ "${SLIVER_VERSION}" != "latest" ]; then
    RELEASES_URL="${BASE_URL}/download/${SLIVER_VERSION}"
fi

for artifact in $ARTIFACTS; do
    url="${RELEASES_URL}/${artifact}"
    echo "Downloading ${url}"
    curl -s -L "${url}" -o "${BIN_PATH}/${artifact}"
    chmod 755 "${BIN_PATH}/${artifact}"
    mv "${BIN_PATH}/${artifact}" "${BIN_PATH}/${artifact%_linux}"
done

