#!/bin/sh
set -eu
DISPLAY=${DISPLAY:-:0}
VNC_OPTS="-auth ${XAUTHORITY} -display ${DISPLAY} -shared -alwaysshared -forever -loop -threads -xrandr exit -viewonly -nopw -noxdamage -nocmds -noremote -norc -nossl -nosel -nobell -ping 60 -nodpms -safer"
id
set +u
if [ ! -z "${VNC_PASSWORD}" ]; then
    PASSWD_DIR="${HOME}/.vnc"
    PASSWD_FILE="${PASSWD_DIR}/passwd"
    mkdir -p "${PASSWD_DIR}"
    x11vnc -storepasswd "${VNC_PASSWORD}" "${PASSWD_FILE}" 
    VNC_OPTS="${VNC_OPTS} -rfbauth ${PASSWD_FILE}"
fi
if [ ! -z "${VNC_SCALE}" ]; then
    VNC_OPTS="${VNC_OPTS} -scale ${VNC_SCALE}"
fi
exec x11vnc ${VNC_OPTS}
