#!/bin/bash
set -eu

for key in ssh_host_rsa_key ssh_host_ecdsa_key ssh_host_ed25519_key; do
    if [ ! -f "${HOME}/.ssh/${key}" ]; then
        ssh-keygen -q -C '' -P '' -f "${HOME}/.ssh/${key}"
    fi
done

cp /authorized_keys ~/.ssh/authorized_keys
chmod 0400 ~/.ssh/authorized_keys

sed "s/AUTOVRTFM_SSH_PORT/${AUTOVRTFM_SSH_PORT}/g" /etc/ssh/sshd_config > sshd_config
exec /usr/sbin/sshd -D -e -f sshd_config
