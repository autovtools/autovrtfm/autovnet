# Proxy Demo

[TOC]

## Proxy (Connect)

### Client (Red)

Then, we can use `autovnet` as a proxy to hide our true (Red) IP address.

1. We send a few direct requests to the target to show our 'real' IP.
2. We use `proxychains` to proxy through autovnet, using a random IP.
3. We show how `autovnet` can generate valid `socks5` urls that can be passed directly to proxy-aware applications.
4. The final command logs in to the target via SSH, and demonstrates that the SSH login came 'from' the random proxy IP.

![server](../img/term/client/proxy.svg)

### Target (Blue)

Here, we see the Blue Team's view of these connections.

1. We see the 'direct' connections, revealing the true IP.
2. All requests that were proxied show the correct, random IPs in the log.

![web_server.svg](../img/term/target/web_server.svg)


## Proxy (Scan)

`autovnet` can be used to proxy anything that `proxychains4` can proxy, which includes scanning tools like `nmap`!

In this demo, we scan Python's `http.server`, running on the `target`

### Client (Red)

1. We connect (`curl`) and scan (`nmap`) the `target` directly, revealing our true IP.

2. We use `autovnet` to run our `nmap` scan through a random, proxy IP.

3. Finally, we use `-c 10000` to proxy the same nmap scan through **10,000** random, proxy IPs!!
* This uses `proxychains4`'s `random_chain` feature. Each connection chooses a random IP from a pool of 10000.
* The takeaway: *you* (the Red Teamer) have granual control of how your connections and scans appear to Blue Team

![nmap](../img/term/client/nmap.svg)


### Target (Blue)

Here, we see the webserver logs of Blue Team.
1. We see the direct connection and scan from Red, all from the same IP.
2. We see a scan from a proxied IP address
*  All connections come from the same IP, because the scan was run with the (default) `-c 1` - `1` proxy IP
3. Finally, we see the `-c 10000` scans, where each connection comes from a randomized IP address, as if being scanned by a botnet.

![nmap_target](../img/term/target/nmap_target.svg)

