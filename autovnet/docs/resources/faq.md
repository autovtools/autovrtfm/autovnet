[TOC]

# FAQs

## Can I run `autovnet` in a container?

Containers are _awesome_!
However, `autovnet` _uses_ containers (e.g. calls `docker-compose`).
Also, if you tried to run `autovnet` in a different network namespace than your host, managing traffic redirection would get gross fast.

It's theoretically possible, but simplicity and ease of use trumps this use case (for now)

## What about `podman` / `podman-compose`?

Not supported, sorry.


## What about AppArmor / SELinux / RHEL-based distros?

Feel free to try, but `docker` doesn't play nice with them.
`autovnet` tries really hard to be as easy to set up and use as possible, and `docker` itself is easiest to get up and running on Debian-based systems.

## Can I run `autovnet` server or client on Windows / BSD?

No.
You can use `autovnet` in a network with Windows hosts, but the expectation is that Red runs Kali (or similar).
Also, the `autovnet server` requires the Linux kernel to simulate IPs, so Linux is a hard requirement.

## I am a Cyber Range Administrator and am concerned about the security of changing my firewall configuration

Don't worry: there is nothing to be scared of.
* The `autovnet server` does *not* (and should not) be reachable from the public internet
* The `autovnet server` can (and generally should) be assigned a *private* IP block that is *not in use* in your network
    - No legitimate / existing traffic will ever touch the `autovnet` server
    - Only traffic *explictly* destined for the simulated IP block(s) will go to the `autovnet server`
        * Red Team controls exactly how and when to opt-in to using `autovnet` to obfuscate their traffic
* The firewall rules to allow Blue <-> `autovnet server` and Red <-> `autovnet server` are no more scary than the existing firewall rule to allow Blue <-> Red
    - `autovnet` is basically like grafting an alien IP block into the Red IP space


Did you already plan on deploying a 'jump box' for Red to use?
* `autovnet` is an application to run on the 'jump box'!
* It just supercharges the jump box to simulate millions of IPs, and makes it easy for Red Team to use the jump box

## Is there an `autovnet` uninstaller?

No. Servers are temporary, source is forever.

The install process may make changes to your system (installing / upgrading packages, etc.) in a way that is difficult to fully remove.
You can "uninstall" the `autovnet server` component with:
```bash
$ autovnet rtfm server uninstall
```
but the `autovnet` program itself will still be available, should you change your mind.

## Can I use my _random old Kali VM_ / _my favorite super custom Kali VM_?

Maybe, but VMs are temporary, source is forever.

`autovnet` will only try to maintain comaptibility with the latest Kali release (open an issue if something breaks), so old versions may not work.

Also, if your super special custom changes aren't applied to a base install using Infrastructure as Code (IaC) for configuration managment (e.g. ansible), tracked in version control (e.g. `git`)
_your super special custom configuration doesn't actually exist_, and you are running on borrowed time :)


## Can I use `autovnet` offline (on an offline / air-gapped network)?

Currently, not really.

Technically, if did the installation and setup online, and forced everyone to run all commands that use `docker` images (e.g. `autovnet rtfm share`) while online, you could probably disconnect from the internet.

Eventually, support for 100% offline install and usage (assuming you have a package mirror set up, or all required packages pre-installed) is planned.

## What if 2 players try to use the same IP? / Birthday Problem? / IP conflicts?

See [here](troubleshooting.md#the-birthday-problem-ip-conflicts).
