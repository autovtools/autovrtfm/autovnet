# References

[TOC]

## Helpful Resources

* [https://blog.cloudflare.com/how-we-built-spectrum/](https://blog.cloudflare.com/how-we-built-spectrum/)
    - This was the blog post that described the `AnyIP` trick: a little-known Linux Kernel feature that allows grafting arbitrary network blocks to a server, and the reason why all of `127.0.0.0/8` is `localhost`, not just `127.0.0.0`
    - Once the IP addresses are "attached" to the `autovnet` server, then regular applications (like `sshd` and `dante`) can bind to any of those IPs, making `autovnet` possible (and ridiculously scalable)
* [https://bluescreenofjeff.com/2017-12-05-designing-effective-covert-red-team-attack-infrastructure/](https://bluescreenofjeff.com/2017-12-05-designing-effective-covert-red-team-attack-infrastructure/)
* [https://blog.cobaltstrike.com/2014/09/09/infrastructure-for-ongoing-red-team-operations/](https://blog.cobaltstrike.com/2014/09/09/infrastructure-for-ongoing-red-team-operations/)

## Similar-ish Projects

Are there other projects out there that do similar things? Open an issue so they can be added here.

### RITRedteam

RITRedteam has a few tools that are similar in concept to `autovnet`'s core features: `proxy`-ing and `listen`-ing:

* [https://github.com/RITRedteam/Sangheili](https://github.com/RITRedteam/Sangheili)
* [https://github.com/RITRedteam/TheArk](https://github.com/RITRedteam/TheArk)

They do pre-date `autovnet` (though the authors of `autovnet` did not discover them prior to developing `autovnet`).

No offense is intended, but under the hood, the primitives used by `autovnet` are clearly superior and infinitely more scalable.

* [A virtual interface per ip doesn't scale](https://github.com/RITRedteam/Sangheili/blob/b0a7bf234398e85cff85062c91bde1aacfe25edc/src/networking.py#L51)
* [Yikes, manually `arp`'ing for virtual IPs?](https://github.com/RITRedteam/Sangheili/blob/b0a7bf234398e85cff85062c91bde1aacfe25edc/src/arp.py)
* [TheArk uses the same techniques](https://github.com/RITRedteam/TheArk/blob/18b1a18aa8129eb21957698f7caa8424be7befed/theark/hosts.py#L151)

Still, [RITSEC Redteam](https://github.com/RITRedteam) deserves a shout-out for independently idenitifying and attempting to solve some of the same problems solved by `autovnet`.

### Alex's CDC Tools

These tools are not open source, and the only known description of them is [this 2017 blog post](https://alexlevinson.wordpress.com/2017/05/09/know-your-opponent-my-ccdc-toolbox/).

See especially `BORG`: 1 VM per IP - made completely irrelevant by `autovnet`'s magic `proxy` feature.
And `GRID`: 
> GRID is a piece of physical hardware which acts as tens of thousands of infected hosts on the network. These “hosts” can proxy call backs and serve malicious files.
> ... GRID is a piece of physical hardware which acts as tens of thousands of infected hosts on the network. These “hosts” can proxy call backs and serve malicious files.

Lol.
_\*`autovnet rtfm listen` would like to know your location.\*_

This toolkit likely holds the honor of the first to attempt to solve the Red Team IP problem, but frankly, the approach here (at least as of `2017`, the date of the blog post) is laughably complex compared to `autovnet`, and not anywhere near as scalable.
Just because you _can_ write custom code for a problem, doesn't mean you _should_ ;)

Take the easy win, and let Linux do the heavy lifting for you (like `autovnet` does). 

Also, `autovnet` is open source!

Competition rules should disallow "cheese strats", and competition scoring should disincentivize them.

The easiest way to do this is to use your `autovnet` infrastructure to launch both Red Team attacks and scoring checks that are essentially indistinguishable from resonable TTPs.

* For hardcore competitions, go as far as scripting out full, `proxied remote authentication -> code execution -> challenge / response` chains _as_ scoring / uptime checks
* If a Red Teamer with valid credentials cannot pretend to be a "remote sysadmin working from home" and use them to do non-distruptive "sysadmin" / "developer"-like tasks, consider adding a "manual" score check that penalizes teams that have hardened their systems past the point of usability
    - E.g. if you discover Blue Team is killing all new TTYs by script, take a minute to try to log in to all teams as a "legit" user and reward the teams that did not cheese the scoring system.
