# In the Wild

[TOC]

Have you used `autovnet` for something cool and want a shoutout? Open an issue!

## [Currently Unnamed Competition]
* `autovnet` has been battle-tested in multiple large-scale Cyber competitions with hundreds of concurrent Red Teamers and Blue Teamers
  - Multiple months of uptime, zero downtime

## Clouds
* `autovnet` has been deployed to Azure, AWS, and vSphere environments without issue
  - Because it relies only on layer-3 routing, it works in any environment where you have sufficient control of routes
  - It relies on no cloud-specific features

## [BishopFox](https://bishopfox.com/)
* `autovnet` was featured by [BishopFox](https://bishopfox.com/) in their [pen testing tools of 2021 roundup](https://bishopfox.com/blog/pen-testing-tools-2021)
