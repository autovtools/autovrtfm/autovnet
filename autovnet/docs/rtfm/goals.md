# RTFM: Goals

[TOC]

## Acid Tests

A proper solution to the 'Red Team IP problem' must satisfy:
* If the Blue Team created a pcap, it should look *good* / *realistic*
    - E.g. Port scan from a botnet of 10k+ IPs
    - E.g. A different IP (from a pool of literally millions) for every beaconing RAT, all https / :443
    - E.g. (If desired by the Red Teamer), the ability to coorelate network events by IP address to attribute specific actors based on the IP / set of IPs they use

* Red Team must have *effortless*, *self-service*, 'provisioning' of *as many IP addresses as they want*, *all at once*, *without disconnecting from the network*
    - E.g. Red Team generates a set of IPs (A, B, C) which belong to mock actor X. Blue Team sees a successful login from IP A, followed by a file download from IP B, followed by a beacon every minute to IP C. Blue Team does IR, attributes A, B, C as IOCs of 'new APT X', and recommends blocking A, B, C as part of the remediation. Red Team confirms that A, B, C are associated with a single actor and used for the reported malicious activity, and approves remediation. Since Red Team can effortlessly generate usable IPs from an arbitrarily large pool (e.g. literally up to the entire IPv4 space), they can 'roll' IPs instantly to emulate the next threat / actor.

* It must be simple enough / reliable enough to use at scale
    - Nothing should be custom that need not be custom
        * E.g. A custom web application for registering port forwards for Red Team is not something that should exist - always, always look for giants so you can stand on their shoulders
    - Any routing solution that is linear in the number of IPs simulated (e.g. 1 iptables rule or virtual interface for every virtual IP) is unacceptable

* It must be simple enough to deploy in an existing competition topology to get buy-in from the Cyber Range administrators
    - E.g. Any solution that requires certain software running on the firewall, a particular VPN setup, configuration changes to Blue Team machines, etc. is unacceptable


## Comparision To Real World

Surely real Penetration Testers / APTs have similar IP problems, right?
No one wants to hack from their residental IP address, and penetration testers are probably hacking from their internal, private intrastructure.

The real world solution is to *trade money for IP addresses*, and / or use decentralized networks to send / proxy traffic for you - e.g. rent a botnet to do your scans, use Tor, etc.

Some great resources on this topic:

* [https://github.com/byt3bl33d3r/Red-Baron](https://github.com/byt3bl33d3r/Red-Baron)
* [https://bluescreenofjeff.com/2017-12-05-designing-effective-covert-red-team-attack-infrastructure/](https://bluescreenofjeff.com/2017-12-05-designing-effective-covert-red-team-attack-infrastructure/)
* [https://blog.cobaltstrike.com/2014/09/09/infrastructure-for-ongoing-red-team-operations/](https://blog.cobaltstrike.com/2014/09/09/infrastructure-for-ongoing-red-team-operations/)

The general idea is that every IP the Blue Team ever sees should be ephemeral and easy to 'roll' / replace. You should never connect directly from your 'residental' IP address, and you should never call back directly to your C2 server. Instead, connect from and beacon to proxies / redirectors that lend your traffic a temporary IP.


So if a Penetration Tester with persistent C2 infrastructure wanted to scan a customer network, log in to a public SSH server, download a payload from a local web server, which will beacon back to their local C2 server, they might do something like:


1. Purchase a VPN subscription or provision a VPS in the cloud
2. Connect to the VPN or proxy through the VPS to do the scan (IP A)

3. Repeat 1-2 since IP A may have tripped IDS and be 'burned'
4. Proxy through IP B to log in to the customer server

5. Repeat 1-2 to get a new IP (IP C) to serve the payload file
6. Register a domain name for IP C
7. Set up redirection such that traffic to IP C:443 will make it to the C2 file server

8. Repeat 1-2 again to get IP D, which will receive the beacon traffic
9. Register a domain name for IP D
10. Set up redirection such that traffic to IP D:443 will make it to the C2 server

-- Line separating boring from fun / important for realism --

11. Configure a payload that beacons to IP D, put it on the file server
12. From customer machine, download the payload from IP C and run it
13. Customer --beacon--> IP D --redirection--> Persistent C2

Tools like Red-Baron can automate some of this setup, but fundamentally rely on *trading money for IP addresses*.
(See also [https://bluescreenofjeff.com/tags#red%20team%20infrastructure](https://bluescreenofjeff.com/tags#red%20team%20infrastructure))

Actually provisioning cloud infrastructure for real penetration testing is a great skill to have... but the 'realism' of entering credit card information into AWS provides no value for Red Teamers doing adversary emulation within a contained Cyber Range network.

