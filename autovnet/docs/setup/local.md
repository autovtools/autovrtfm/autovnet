[TOC]

# Local Testing

If you just want to test locally, you can run the `autovnet server` and `autovnet client` on your Kali machine.
(Note: you *do not* need to and *should not* do this if your Cyber Range administrator is running an `autovnet server` for you)

# Install

See [install.md](install.md)

# Configure

```bash
# Generate the config files
# (Simulate all of 10.192.0.0/10)
$ autovnet config init -n 10.192.0.0/10
```

Install the _server_ component locally on your Kali machine as a `systemd` service
```bash
# Internet connection required, might take a bit to start
$ autovnet rtfm server install

# Confirm it is running:
$ systemctl status autovnet
```

Example output of a succesful install (these "errors" are fine and expected):
```bash
autovnet[2132]: autovnet.sockd_1  | New password: Retype new password: passwd: password updated successfully
autovnet[2132]: autovnet.sockd_1  | sockd[1]: warning: checkconfig(): rotation for external addresses is set to same-same, but the number of external addresses is only one, so this does not make sense
autovnet[2132]: autovnet.sockd_1  | sockd[1]: info: Dante/server[1/1] v1.4.3 running
autovnet[2132]: autovnet.sshd_1   | Server listening on 0.0.0.0 port 2222.
autovnet[2132]: autovnet.sshd_1   | Server listening on :: port 2222.
autovnet[2132]: autovnet.sshd_1   | Couldn't create pid file "/run/sshd.pid": Permission denied
```

Follow the logs:
```bash
$ journalctl -u autovnet.service -f
```

**Watch the logs until you see the example output above**

It may take several minutes after install to reach this state and be ready to use.

Now your local machine is simulating all of `10.192.0.0/10` - over _4 million_ unique IP addresses!

_External_ systems won't be able to reach you on those IPs (that requires a proper IP route configuration, see the [server install guide](configure.md) for how to do a production deployment),
but now you can try out `autovnet` locally to get a feel for how it works.

# Configure External Systems

Only do this if you need to do local testing and can't do a proper [server installation](configure.md) with a firewall and a single status route.

You can force an external system on the same network to respect your simulated network by adding a direct route to your Kali machine:

```bash
# On external (Linux) machine:
# (Change these)
NET= "10.192.0.0/10"
INTERFACE="ens192"
REAL_KALI_IP="X.X.X.X"

sudo ip route add "${NET}" dev "${INTERFACE}" via "${REAL_KALI_IP}"
```

# Test It!

`autovnet` includes a test command that will verify that your configuration is valid and you can use the _autovnet server_ to redirect packets on your behalf.
```bash
autovnet net test
 _______ _     _ _______  _____  _    _ __   _ _______ _______
 |_____| |     |    |    |     |  \  /  | \  | |______    |
 |     | |_____|    |    |_____|   \/   |  \_| |______    |

Hacking the planet...  [####################################]  100%
[+] OK!
```

Any errors here indicate a misconfiguration.

# Cleaning up

When you are done testing locally, you should clean up the `autovnet` server component,
because it will conflict with a "real" autovnet server install.

## Uninstall the Service

This command will uninstall the `systemd` service, and stop the network simulation
```bash
$ autovnet rtfm server uninstall
```

You can delete your local `~/.autovnet` config directory, or use the `-f` (force) flag when you import the "real" `autovnet` server's config later

```bash
# Note the "."
$ rm -rf ~/.autovnet

# OR
# run 'autovnet config export' on your real autovnet server

$ autovnet config import -f GENERATED_URL
```
