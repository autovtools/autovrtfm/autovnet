# cyberchef

[TOC]

# Intro

[CyberChef](https://gchq.github.io/CyberChef/) is a simple, intuitive web app for carrying out all manner of "cyber" operations within a web browser.

It's a Cyber Swiss Army Knife, and great for CTF challenges that involve encoding / decoding.

It's a static site that processes all the data in your browser, much like `drawio`, so this intergation is super simple.

# `cyberchef export`

Start a new `cyberchef` instance:
```bash
$ autovnet rtfm cyberchef export
[+] autovnet rtfm cyberchef export electric_shoulder
[+] connect : xdg-open http://127.0.0.1:40774
[+] mount   : autovnet rtfm cyberchef mount -C electric_shoulder 10.246.11.135:64182
[*] Waiting for cyberchef ...
[+] cyberchef is ready!
```

# `cyberchef mount`

Connect to another player's `cyberchef` instance:
```bash
$ autovnet rtfm cyberchef mount -C electric_shoulder 10.246.11.135:64182
[+] autovnet rtfm cyberchef mount electric_shoulder
[*] Waiting for cyberchef ...
[+] connect: xdg-open http://127.0.0.1:35318
[+] cyberchef is ready!
```
