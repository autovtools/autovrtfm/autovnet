# focalboard

[TOC]

## `focalboard` (Project Mangement) Sharing

[focalboard](https://www.focalboard.com/) is an open source, self-hosted alternative to Trello, Notion, and Asana.

It makes nice [Kanban Boards](https://en.wikipedia.org/wiki/Kanban_board) and related Project Management things to help you keep your team organized on on track to success,
especially for longer (e.g. multi-day) games.

# `focalboard export`

Player 1 can host a `focalboard` server using the standard `export` semantics:

```bash
$ autovnet rtfm focalboard export
[+] autovnet rtfm focalboard export joyous_subject
[+] connect : xdg-open http://127.0.0.1:62277
[+] mount   : autovnet rtfm focalboard mount -C joyous_subject 10.196.210.42:34784 -p 62277
[*] Waiting for focalboard ...
[+] focalboard is ready!
```

**Important: the first user created is the `focalboard` admin**
* Create your admin user before sharing the connection details with other players!

## User Registration

Creating a user in the Web UI is simple (your browser opens for you when you `export` or `mount`):

1. Register
  - Email: `your_handle@x.localhost`
  - Username: `your_handle`
    * technically, any email address will work as long as it is in the `HOST.DOMAIN` form (it will never be emailed)
  - Password: Just use a browser-generated password and save it - never re-use a real password in a game environment

2. Click the Focalboard Logo (top left)

3. Invite Users
  - Other users must `focalboard mount` your service, then open this registration link in order to create an account

# `focalboard mount`

Now Players 2-N can mount the `focalboard` service (and follow the registration instructions above):

**The player who ran export must generate an invite link and give other players _both_ the `mount` command and the invite link.**

```bash
$ autovnet rtfm focalboard mount -C joyous_subject 10.196.210.42:34784 -p 62277
[+] autovnet rtfm focalboard mount joyous_subject
[*] Waiting for focalboard ...
[+] connect: xdg-open http://127.0.0.1:62277
[+] focalboard is ready!
[+] Get invite link from host > Register (my_handle@x.localhost)
```

Then, players can open the invite link to create their account.
All boards are visibile by all connected players.

# Backups

`focalboard` backups use the same `postgres` backup mechanism used elsewhere.

You can manually export / import board archives via the UI,
but - by default - `autovnet` will take periodic database snapshots for you.

The backups are saved in `/opt/rtfm/focalboard/CODE_NAME/backups/` and can be restored by using the `--init-sql` argument of `autovnet rtfm focalboard export`
