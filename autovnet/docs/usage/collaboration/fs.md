# Shared Directories

[TOC]


## Intro

`autovnet` makes it simple to _securely_ share directories with other `autovnet` (e.g. Red-only) players.

Any player can export a secure network share, and any other player can mount it as if it where a locally attached disk.

# Example: Default Paths

By default, `autovnet` will use reasonable, generated paths for your exports and mounts, based on your codename.


## `fs export`

```bash
# Player 1: export (host) a file system
$ autovnet rtfm fs export
[+] autovnet rtfm fs export -C closed_photo
[+] export: /opt/rtfm/fs/closed_photo/export
[+] mount: autovnet rtfm fs mount -C closed_photo 10.250.44.5:53179
```
* To re-do this command later, Player 1 would run `autovnet rtfm fs export -C closed_photo`
* The directory being exported is `/opt/rtfm/fs/closed_photo/export` (or `~/.mnt/closed_photo/export` for slightly less typing)
* Other players can `mount` this directory with `autovnet rtfm fs mount -C closed_photo 10.250.44.5:53179`

## `fs mount`

```bash
# Player 2: mount the file system
autovnet rtfm fs mount -C closed_photo 10.250.44.5:53179
[+] autovnet rtfm fs mount closed_photo
[+] mountpoint: /opt/rtfm/fs/closed_photo/mnt
```
* The `-C` sets the codename to = `closed_photo` - now the host who exported and all players who mount can refer to this entity with the same codename
* To later re-mount this export, Player 2 would run `autovnet rtfm fs mount closed_photo`
* Player 2's `/opt/rtfm/fs/closed_photo/mnt` (or `~/.mnt/closed_photo/mnt`) _is_ Player 1's `/opt/rtfm/fs/closed_photo/export`
    - Any files created by other players will be seen by all players who mounted `closed_photo`
    - Any changes are available in almost real-time (just a few seconds for new files to become visible)
* The actual files are stored on Player 1's system, and if they stop the `export`, all other players are disconnected until they restart it.

# Example: Custom Paths

You can also use custom paths when exporting or mounting to implement your own favorite organziation strategy, or to type less for commonly used paths.

## `autovnet rtfm fs export`

```bash
# Player 1: export a shared "loot" directory for collaboration
$ autovnet rtfm fs export ~/loot
[+] autovnet rtfm fs export -C nasty_union
[+] export: ~/loot
[+] mount: autovnet rtfm fs mount -C nasty_union 10.232.138.222:46045
```

## `fs mount`

We can append a custom mountpoint to the generated `mount` command:
```bash
# Player 2-N: mount the shared loot directory to ~/loot
$ autovnet rtfm fs mount -C nasty_union 10.232.138.222:46045 ~/loot
[+] autovnet rtfm fs mount nasty_union
[+] mountpoint: ~/loot
```

Now, `~/loot` for all players is actually hosted by Player 1,
    and all players see any files, scan results, etc contributed by any player in basically real-time!

# Network Performance

`autovnet` is magic in a lot of ways, but can't yet teleport bytes between players without using the network.

That is, keep in mind _where_ files are being stored:
* Files are stored on the system that ran the `fs export`

Under the hood, other players are mounting that remote directory via [sshfs](https://github.com/libfuse/sshfs).

So when Player 2 (the mount-er) reads a file from the mount, the bytes flow from Player 1 (the export-er)'s system,
    through the `autovnet` server, and then to Player 1.

If the player system are remote (e.g. VPN'ing into the game network, rather than having Kali VMs hosted _in_ the game network),
    this means that Player 2 can only read files as fast as Player 1 can "upload" them (their internet upload speed provided by their ISP).

If Player 2 copies a file to the mount, they are essentially uploading the file from Player 2's system to Player 1's system, and would be subject
    to the upload speed of Player 2.

Once Player 2's file is copied, than Player 3 could access it at the upload speed of Player **1**, who is actually hosting all files.

It can be confusing to figure out whose upload speed will be used. But here is a simple trick to figure it out:
* Who has the actual bytes on disk?
    - Regardless of the setup, if those bytes make it to another player, then the player with the bytes on disk used their upload speed.

So, as a game admin with remote players, what should you do to make things as fast as possible for your players?
* If possible, host a `autovnet` _client_ on the same infrastructure hosting the `autovnet` _server_
    - Run any critical exports that all players will mount from the central client
    - Because the bytes are in the cloud / central infrastructure, your cloud / enterprise upload speed will be used, which is probably much faster than residential
* Otherwise, use the ISP upload speed of players to determine who should host large or important exports


# Local Mirroring

But what if your players want a local copy of all data so they can browse offline, or take home all the notes, files, etc. shared as part of the game?

`autovnet` makes this easy with opt-in local mirroring.

We can use the `-m` / `--mirror` flag when we mount (or remount, as shown below), and we will maintain an local, offline copy of the remote export.
```bash
$ autovnet rtfm fs mount nasty_union -m
[+] autovnet rtfm fs mount nasty_union
[+] mountpoint: ~/loot
[+] mirror every 60s to /opt/rtfm/fs/nasty_union/mirror
```
There are more options to customize mirroring, see [help.md](../help.md) for the full list.

Things to keep in mind when mirroring:
* Everyone who mirrors is essentionally causing the export-er to upload the entire contents of the directory to each of their machines at least once (the changes after that are incremental)
    - You can very quickly saturate the export-er's upload link with large files being concurrently mirrored
* Do NOT make local changes to your mirror directory - they will be lost
    - The mirror is a snapshot of the remote export; if your local changes / files are not in the remote export, the next mirror cycle will delete them
* You will get the _entire_ remote export, so if someone is sharing a directory full of hundreds of ISOs, it will take you a very long time to finish mirroring
