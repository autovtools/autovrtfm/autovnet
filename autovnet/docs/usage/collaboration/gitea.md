# gitea

[TOC]

## Intro

`gitea` is a lightweight, self-hosted [git](https://git-scm.com/) server.

It's basically a lean-and-mean `gitlab`, supporting everything that most basic users need while being far more simple and resource efficient.

Welcome to the wonderful world of version control!

**All code should be stored in a version control system**.

You don't _have_ to use `autovnet`'s `gitea` instance if you already have something suitable, especially if you want to have a persistent server between games.

But the rule of thumb to live by is:

> If it isn't in `git`, it doesn't exist (:


This `gitea` integration is the recommended way to pre-seed a game environment with code, e.g. if you do not allow your players to directly download tools from the internet, a game admin can mirror approved public repositories to an internal `gitea export`.

# `gitea export`

Player 1 can host a `gitea` server using the standard `export` semantics:

```bash
$ autovnet rtfm gitea export
[+] autovnet rtfm gitea export lazy_big
[+] connect : xdg-open http://127.0.0.1:3080
[+] mount   : autovnet rtfm gitea mount -C lazy_big 10.214.27.174 40725 37627
[*] Waiting for gitea ...
[+] gitea is ready!
```

**Important: the first user created is the `gitea` admin**
* Create your admin user before sharing the connection details with other players!

## User Registration

Creating a user in the Web UI is simple (your browser opens for you when you `export` or `mount`):

1. Register
  - Username: `your_handle`
  - Email: `your_handle@x.localhost`
    * technically, any email address will work as long as it is in the HOST.DOMAIN form (it will never be emailed)
  - Password: Just use a browser-generated password and save it - never re-use a real password in a game environment

2. Add an ssh key to your account
  - Profile Icon (top right)
  - Settings
  - SSH / GPG Keys (tab)
  - Add Key

3. If you are new to ssh keys and don't know where to get yours:
  * Try `cat ~/.ssh/id_rsa.pub`.
  * If it worked (file existed), paste that in the `Content` box for `gitea`
  * If it gave an error, run `ssh-keygen` and keep pressing `<Enter>` to accept all defaults.
  * Repeat the above command to get and add your key


## Organizations / Teams / Groups / Roles

`gitea` uses different names that `gitlab` does.

* `gitlab group` = `gitea organization` (lets you group related repos together)
* `gitlab role` = `gittea team` (lets you set permissions)

# `gitea mount`

Now Players 2-N can mount the `gitea` service (and follow the registration instructions above):

```bash
$ autovnet rtfm gitea mount -C lazy_big 10.214.27.174 40725 37627
[+] autovnet rtfm gitea mount -C lazy_big
[*] Waiting for gitea ...
[+] connect: xdg-open http://127.0.0.1:3080
[+] gitea is ready!
```

Tell players to follow the registration instructions to create their account

# Backups

The backup strategy for `gitea` is very similar to `hedgedoc`.

By default, there are automatic `postgres` backups.
You can semi-manually backup the application data directory (`/opt/rtfm/gitea/CODE_NAME/data/`) with `fs export` and `fs mount --mirror`

If you only care about preserving your previous source code and not about preserving the `gitea` instance (users, teams, etc.),
then you can find the bare git repos (which contain the entire repo history and `git` metadata) in `/opt/rtfm/gitea/CODE_NAME/data/git/repositories`
