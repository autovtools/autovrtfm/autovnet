# matrix

[TOC]

## Intro

_This is a preview feature that may not be ready for prime time. Test it at scale before commiting to using it for your game._

[matrix](https://matrix.org/) is a decentralized communication platform / a set of APIs for such a platform.

[dendrite](https://github.com/matrix-org/dendrite) is an implementation of the [matrix specification](https://spec.matrix.org/latest/).
* It is currently early in development, lacks some important features, and may not be stable yet
[element](https://element.io/) is a frontend for `matrix`.

By their powers combined, they form a self-hostable (but not 1-1) alternative to `slack`, `discord`, `rocketchat`, or `mattermost`.

It is the first chat solution integrated into `autovnet` due to its relatively mature [SDK](https://matrix.org/sdks/) / collection of [bots](https://matrix.org/bots/) / [bridges](https://matrix.org/bridges/).

Some tips / warnings:
* Opt out of the usage stats, unless you really want to (it's a prompt from [element](https://element.io/), not your game admin)
* spaces seem... broken
* notification settings are not currently implemented by `dendrite`
* some API calls are not currently implemented by `dendrite`, so there are some UI errors (e.g. `failed to load list of X`)
* video calls / voice calls are probably going to be rough
* encrypted messages / channels are probably going to be rough

# `matrix export`

You know the drill - Player 1 exports, and other players will mount.

```bash
$ autovnet rtfm matrix export
[+] autovnet rtfm matrix export spicy_animal
[+] connect : .element
[+] mount   : autovnet rtfm matrix mount -C spicy_animal 10.232.90.240:36594
[*] Waiting for matrix ...
[+] matrix is ready!
```

`element` will open, pre-configured to connect to your homeserver.

Register your handle, and create spaces (kinda like a `discord` server) / rooms (like a `discord` channel).

# `matrix mount`

Other players mount the instance, register their own handle, and join the conversation:

```bash
$ autovnet rtfm matrix mount -C spicy_animal 10.232.90.240:36594
[+] autovnet rtfm matrix mount spicy_animal
[*] Waiting for matrix ...
[+] matrix is ready!
[+] connect: .element
```

# Backups

`matrix` backups use the same `postgres` backup mechanism used elsewhere.

You can manually export / import chat history via the UI,
but - by default - `autovnet` will take periodic database snapshots for you.

The backups are saved in `/opt/rtfm/matrix/CODE_NAME/backups/` and can be restored by using the `--init-sql` argument of `autovnet rtfm focalboard export`

Other important application data is saved in `/opt/rtfm/matrix/CODE_NAME/matrix`, which you would need to back up manually (or via `fs export`), if you want a complete backup.

