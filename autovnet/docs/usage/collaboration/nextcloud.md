# nextcloud

[TOC]

## Intro

[nextcloud](https://nextcloud.com/) is a popular self-hosted alternative to Google Drive.

It's a large, complex, an click-intensive PHP application, and should not be your first choice for `autovnet` collaboration.
It is included due to its popularity and possibly useful marketplace apps.

* If you want to share files, [use `fs`](#shared-directories)
* If you want to have collaborative notes / documents, [use `hedgedoc`](#hedgedoc)

# `nexcloud`-isms

`nextcloud` can a bit confusing for first-time users.

Here are some tips
* Each `nextcloud` user has their own directory structure, and is expected to "share" items with other users explicitly.
* There is no open registration; either share the admin creds, manually create accounts (eww) or figure out how to script it.

# `nextcloud export`

```bash
$ autovnet rtfm nextcloud export
[+] autovnet rtfm nextcloud export uptight_session
[+] connect : xdg-open http://127.0.0.1:47209
[+] mount   : autovnet rtfm nextcloud mount -C uptight_session 10.222.1.210:58685
[*] Waiting for nextcloud ...
[+] nextcloud is ready!
[+] Complete setup in browser
```

# `nextcloud mount`

```bash
$ autovnet rtfm nextcloud mount -C uptight_session 10.222.1.210:58685
[+] autovnet rtfm nextcloud mount uptight_session
[*] Waiting for nextcloud ...
[+] connect: xdg-open http://127.0.0.1:37177
[+] nextcloud is ready!
```

# Backups

`autovnet` tries to back up your `nextcloud` data, but getting [a nextcloud restore to work](https://docs.nextcloud.com/server/latest/admin_manual/maintenance/restore.html) is left as an exercise for the reader. (YMMV.)

You can find the raw files at `/opt/rtfm/nextcloud/CODE_NAME/nextcloud/data/USER/files`
