# owncast

[TOC]

## Intro

[owncast](https://owncast.online/) is a self-hosted live video and web chat server for use with existing popular broadcasting software.

It's basically a single-streamer alternative to [Twitch](https://www.twitch.tv/), and can accept streams from [OBS](https://obsproject.com/) and similar.

The `autovnet` install scripts install `.obs` for you, a simple command for launching `OBS`.
It's relatively intuitive to use `OBS`, but you will need to experiment with the settings to see what works best for you.

Thankfully, the [`OBS` documentation](https://obsproject.com/help) is great, and there is a large user base.

### Use Case 

*Note: if you just want to screenshare with another player, [`vnc`](#screen-sharing) will probably be much higher quality and lower latency*

`owncast` makes the most sense in the following situation(s):
* You need to broadcast your video (and/or audio) to many players at once
* Your upload speed (from your ISP) is very limited
* You can tolerate a high stream delay (e.g. `30s+`) (the delay between when you take an action and when it appears on the stream)
* You want to allow external spectators (or Blue Team, etc.) that are not `autovnet` players to view the stream

### Limited Upload Speed

If you have remote players, run the `owncast export` on the system with the highest upload speed from the ISP

Usually, this will mean running a new, central `autovnet` "client" system (e.g. VM) on the same infrastructure as the central `autovnet` server
    * **Never** run `owncast` (or any other shared, central services) on the same VM as the `autovnet` server - it's a terrible idea, and could take down your network when the load gets too high
    * This central `autovnet` client is approximately equidistant from all remote players, and because of how `autovet` works, your bandwidth to remote players is already capped at the upload speed of your `autovnet` server

If you are unable to provision an additional dedicated, central VM, a player with high upload speed can host the owncast server instead
    * The "streamer" can max out their (probably low) upload speed streaming to the `owncast` server, and the `owncast` server will then **re-**stream the feed to all viewers
    * Unlike the `vnc export` command, where each additional viewer causes the _streamer_ to upload more data, here, each additional viewer causes the _`owncast` export-er_ to serve more data

**TL;DR: owncast: `streamer -- X Mbps --> owncast == N * X Mbps ==> N viewers**
**v.s.       vnc: `streamer == N * X Mbps ==> N viewers`**

### Streaming Tips

These are some general tips that may be helpful.
You absolutely must plan ahead and test in advance to find the settings that are right for your environment and hardware.

* `http://127.0.0.1:1980/admin` is the admin interface, where you can [configure `owncast`](https://owncast.online/docs/configuration/)
    - Your config options are persistent (saved as  part of you `code_name` data)
    - There is no easy way to prepopulate config options into `owncast`, due to the decision to remove `config.yaml` (: [ref](https://github.com/owncast/owncast/issues/1234#issuecomment-884104466)

# `owncast export`

```bash
$ autovnet rtfm owncast export 
[+] autovnet rtfm owncast export glamorous_join
[+] ========== viewers ==========
[+] connect : xdg-open http://127.0.0.1:1980
[+] mount   : autovnet rtfm owncast mount -C glamorous_join 10.226.139.116 63148 51507
[+] ========== streamers ==========
[+] configure  : xdg-open http://127.0.0.1:1980/admin
[+] user       : admin
[+] key / pass : yOTfGzotx-SlGHJsIA6cyYaiv9EClqnbB0_arHoRWbs
[+] stream     : rtmp://127.0.0.1:1935/live
[*] Waiting for owncast ...
[+] owncast is ready!
```

You can start [OBS](https://obsproject.com/) with `.obs`.

Only share the stream key if you want the remote `mount`'er to stream to your server.

# `owncast mount`

```bash
$ autovnet rtfm owncast mount -C glamorous_join 10.226.139.116 63148 51507
[+] autovnet rtfm owncast mount -C glamorous_join
[*] Waiting for owncast ...
[+] connect: xdg-open http://127.0.0.1:3080
[+] owncast is ready!
```

# VoD / Scrubbing / Recording

`owncast` does not save the video, and only displays the live feed.

If you want to record the stream, you should do this in your broadcasting software (e.g [OBS](https://obsproject.com/)) and upload / host your videos separately.
