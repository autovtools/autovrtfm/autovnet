# Screen Sharing

[TOC]

## Intro

There is no greater troubleshooting technology than the ability for an experienced player to view the screen of a less experienced player to quickly diagnose and resolve issues.

Also, if an experienced player (or teacher) broadcasts their screen, other players (or students) can watch and learn in real time.

`autovnet` makes screen sharing easy with secure, peer-to-peer VNC, leveraging the core `autovnet rtfm svc` design to encrypt all data in transit and ensure that only other authenticated players are capable of connecting to the stream.

The VNC server used is hardened and empathically view-only.
It does not and cannot accept any control input from connecting clients - it essentially a high-res video stream.

See [help.md](../help.md) for additional details on the options available.

**Note: the `vnc` host must be logged in to a graphical session or clients will fail to connect**



# `vnc export`

Here, Player 1 shares their screen for other players.
Player 1 has a `4k` (`3840x2160`) screen, but recognizes that most other players for the current event have `1080p` (`1920x1080`) displays.
So, Player 1 scales their `vnc` output resolution to `1600x900`, which most players will be able to comfortably view without scrolling.

(Player 1 still has full `4k` output locally.)
```bash
$ autovnet rtfm vnc export -s 1600x900
[+] autovnet rtfm vnc export spurious_marriage
[+] mount: autovnet rtfm vnc mount -C spurious_marriage 10.201.233.72:35439
```

# `vnc mount`
Then, Players 2-N can all watch Player 1 work in real-time!

```bash
autovnet rtfm vnc mount -C spurious_marriage 10.201.233.72:35439
[+] autovnet rtfm vnc mount spurious_marriage
```

A vnc viewer window (`1600x900`) opens for Players 2-N.

When Player 1 stops the `vnc export` command with `CTRL+C`, the stream is ended and all clients are disconnected.

See the [help](../help.md) menu for additional options, including how to generate a random VNC password, if you want to further restrict access.
Again, by default, **only** other `autovnet` players can connect, since only they have the `rtfm.key` used by `stunnel` under the hood.

# Multiple Resolutions

If your players have a variety of resolutions or complain about your screenshare being too large or too small,
you can simply start addtional `vnc export`'s with different `--scale` settings, and let players choose the resolutions that is right for them.

# Voice / Audio?

Real-time voice / audio streaming is not a priority, because

1. `autovnet` deployments are almost all VMs, which often have limited audio input / output
2. [Discord](https://discord.com/) exists, and provides excellent, essentially unrestricted voice chat features.
    * It also has video streaming support, but the resolutions are limited, and video compression is heavy
    * For `autovnet` clients on the same virtual network, the quality of the VNC streams are exceptional

(For presentation-style audio, you might be interested in [owncast](owncast.md))

# Screen Resizing

If the host / presenter of a VNC stream is using a VM, be careful to avoid any guest screen resize events!

If you change your screen resolution during a screen (e.g. by _fullscreen_-ing your VM or resizing the window, causing VMware tools or VirtualBox guest additions to resize the guest output resolution),
bad things will happen and you will have to restart your vnc export.

To make it super obvious that you messed up, all vnc clients will be kicked if you resize, and if they reconnect, they will likey see a frozen image of your old screen.

Thankfully, you can just re-execute the printed `autovnet rtfm vnc export CODE_NAME` command, and players can just re-execute the printed `autovnet rtfm vnc mount CODE_NAME` command to reconnect.

(`x11vnc`'s `-xrandr` behavior is related, but `xtightvncviewer` can't handle the resize, and the `-xrandr exit` option doesn't properly restart `x11vnc` for us :/)


# Wayland / Xorg

`x11vnc` does not work with `Wayland`, so `vnc export` will not work if you are running `Wayland`.

Ubuntu and some other distros ship with `Wayland` enabled by default.

See [this guide](https://fostips.com/switch-back-xorg-ubuntu-21-04/) for switching back to `Xorg`.

