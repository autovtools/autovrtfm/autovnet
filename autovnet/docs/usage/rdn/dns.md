# dns

[TOC]

## Intro

Most normal, real-world internet traffic (web browsing, etc.) uses [`dns`](https://en.wikipedia.org/wiki/Domain_Name_System) to resolve a human-friendly name like `google.com` to a numeric [IP address](https://en.wikipedia.org/wiki/IP_address) that your computer understands.

In the real world, people pay money to "register" a `dns` record.
After paying one of the right companies, someone on the other side of the world can access your website via the human-friendly name you associated with our IP.

Malicious actors often also register `dns` names as part of their C2 infrastructure.

* This can help make malicious traffic look more "normal"
* By changing the `dns` record later, the actor can change which C2 server(s) that malware will connect to

For similar reasons, and to better emulate real-world actors, we want to support creating `dns` records as part of our [RTFM](../../rtfm/README.md) infrastructure.

If you game admin [followed the `dns` configuration guide](../../rtfm/autovnet.md#dns),
then your `autovnet` server supports `dns` and will allow `autovnet` players to register `dns` names (for free, obviously) that will work within your game network.

## `register`

You can use the `autovnet rtfm dns register` command to create `dns` records.

All records you create will end in a custom [TLD](https://en.wikipedia.org/wiki/Top-level_domain),
`.avn` (for `autovnet`) by default.

### No Args

```bash
$ autovnet rtfm dns register
[+] Connecting...
[+] dns ready!
longing.salary.avn:10.229.134.116
```

With no arguments, `register` chooses a random `dns` name for you, and a random IP.

The `dns` record is valid until you `CTRL+C` out of the register command.

We can confirm the registration worked by performing a `dns` query with `dig`
```bash
# Any system can now resolve the registered name
$ dig longing.salary.avn +short
10.229.134.116
```

### Chosen IP

If you already have an IP - perhaps your `map_ip` from a [`tun`](../rdn/tun.md) - you can give it to `register`,
which will generate a random `dns` record for it.

```bash
$ autovnet rtfm dns register 10.231.52.107
[+] Connecting...
[+] dns ready!
disgusted.score.avn:10.231.52.107
```

### Chosen DNS Name

If you have a special name that you want to use - perhaps a human-friendly name for a game service - you can pass that to `register`, too.

If you just pass a name, then `register` will choose a random IP for you.

```bash
$ autovnet rtfm dns register scoreboard
[+] Connecting...
[+] dns ready!
scoreboard.avn:10.216.60.142
```

Notice that `.avn` was added to your registration for you.
This is an intentional design choice; all game queries will end in the game TLD,
and game registrations won't break real `dns` needed for internet access, etc.

You can use the tld explictly if you prefer:
```bash
autovnet rtfm dns register foo.avn   
[+] Connecting...
[+] dns ready!
foo.avn:10.242.229.186
```
### Chosen Record

Know both the name you want and the IP?
Separate them with a colon like `name:ip`!

```bash
$ autovnet rtfm dns register foo.avn:10.239.77.243
[+] Connecting...
[+] dns ready!
foo.avn:10.239.77.243
```

### Detach Register, Unregister

But what if you want to register a `dns` name long-term and don't want to sacrifice a terminal to appease `autovnet`?

Use the `-d` flag to create the registration, without waiting for a `CTRL+C` to unregister.
```bash
$ autovnet rtfm dns register -d
[+] Connecting...
[+] dns ready!
swift.man.avn:10.250.116.17

$
```

Test the registration.
```bash
$ dig swift.man.avn +short      
10.250.116.170
```

Manually `unregister` when you are done, using `NAME` or `NAME:IP` syntax

```bash
$ autovnet rtfm dns unregister swift.man.avn
[+] Connecting...
[+] dns ready!
```

Now the registration is gone.
```bash
$ dig swift.man.avn +short      

$
```

### Save / Load From File

Want to do something fancy with your `dns` registerations?

Want to register or unregister domains in batches?

You can save or load registrations from a simple text file!

Here, we create `10` registrations and save them to a file we can use later.

```bash
$ autovnet rtfm dns register -c 10 -d -o /tmp/domains.list
[+] Connecting...
[+] dns ready!
ordinary.scene.avn:10.246.79.98
absorbed.airport.avn:10.208.15.185
thin.force.avn:10.200.118.131
superb.newspaper.avn:10.236.181.236
tranquil.difference.avn:10.206.67.106
loving.revolution.avn:10.231.243.114
incandescent.cream.avn:10.255.164.126
voracious.price.avn:10.198.71.155
determined.laugh.avn:10.194.16.55
slimy.blind.avn:10.216.146.132
[+] Created /tmp/domains.list
```

Now that we have text file, we can do all kinds of neat tricks.

Print a random domain name from the file:
```bash
$ cat /tmp/domains.list | cut -d: -f1 | sort -R | head -n 1
superb.newspaper.avn

$ cat /tmp/domains.list | cut -d: -f1 | sort -R | head -n 1
voracious.price.avn
```

You can also easily test all your registrations like:
```bash
$ cat /tmp/domains.list| cut -d: -f1 | xargs -ti -n 1 dig {} +short
dig ordinary.scene.avn +short
10.246.79.98
dig absorbed.airport.avn +short
10.208.15.185
dig thin.force.avn +short
10.200.118.131
dig superb.newspaper.avn +short
10.236.181.236
dig tranquil.difference.avn +short
10.206.67.106
dig loving.revolution.avn +short
10.231.243.114
dig incandescent.cream.avn +short
10.255.164.126
dig voracious.price.avn +short
10.198.71.155
dig determined.laugh.avn +short
10.194.16.55
dig slimy.blind.avn +short
10.216.146.132
```

Or you simply count the number of successful resolutions:
```bash
$ cat /tmp/domains.list| cut -d: -f1 | xargs -i -n 1 dig {} +short | wc -l
10
```

You can also unregister in bulk:
```bash
$ autovnet rtfm dns unregister -i /tmp/domains.list
...
```

And confirm they are gone:
```bash
$ cat /tmp/domains.list| cut -d: -f1 | xargs -i -n 1 dig {} +short | wc -l
0
```

And finally, you can re-register the batch all at once
```bash
$ autovnet rtfm dns register -d -i /tmp/domains.list
...

$ cat /tmp/domains.list| cut -d: -f1 | xargs -i -n 1 dig {} +short | wc -l
10
```

See the [help menu](../help.md#autovnet-rtfm-dns-register) for more ideas and try it out!

### But I can't connect to the random IP?

The `register` command creates `dns` _records_.

It does not reserve the IP addresses shown or cause the IP shown to point to your machine.

If you want to actually use the random IP that `register` might print out, you can use the [`listen`'s `-n` option](../rdn/listen.md).


Create a service to share:
```bash
$ python -m http.server
Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ...
```

Use -n to request the IP rolled by register:
```bash
$ autovnet rtfm listen -n 10.229.134.116 80 -p 8000
[+] autovnet rtfm listen wakeful_market
[+] wakeful_market | 10.229.134.116:80 => 127.0.0.1:8000
```

Now, you can access your webserver using your registered `dns` name
```bash
$ curl longing.salary.avn
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
</ul>
<hr>
</body>
</html>

```

## `listen` integration

To potentionally save you a step, you can register `dns` names at the same time you create a manual `listen` command.

Assuming you have an http server like:
```bash
$ python -m http.server 
Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ...
```

You can expose it with `listen` and register a random `dns` name:

```bash
$ autovnet rtfm listen 80 -p 8000 -R
[+] autovnet rtfm listen nippy_many
[+] nippy_many | 10.199.44.94:80 => 127.0.0.1:8000
afraid.client.avn:10.199.44.94
```

Now you can access your web server using your registered `dns` name:
```bash
$ curl afraid.client.avn
...
```

You can also register a particular `dns` name:
```bash
$ autovnet rtfm listen 80 -p 8000 -r foo.avn
[+] autovnet rtfm listen abhorrent_field
[+] abhorrent_field | 10.219.240.234:80 => 127.0.0.1:8000
foo.avn:10.219.240.234
```

And if you later restart the `listen` by codename, the `dns` name is re-registered:

```bash
$ autovnet rtfm listen abhorrent_field
[+] autovnet rtfm listen abhorrent_field
[+] abhorrent_field | 10.219.240.234:80 => 127.0.0.1:8000
foo.avn:10.219.240.234
```

## `tun` integration

You can also register `dns` names when you run `tun` commands.

Assuming you have an http server like:
```bash
$ python -m http.server 
Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ...
```

You can bring up a `tun` that will register a random `dns` name on each roll:

```bash
$ autovnet rtfm tun up -R 
...
[+] map_ip: 10.225.206.231
elite.internet.avn:10.225.206.231
```

Note that we can only access the `tun` from a different system - because `tun`s are weird.
Also, notice that the port is `:8000`, because `tun` maps our IP `1-1`.

```bash
$ curl elite.internet.avn:8000
...
```

When you roll a new IP, you also roll your random `dns` name.

```bash
...
[>] Roll new IP?
 [y/N]: y
[+] map_ip: 10.247.120.227
wretched.spell.avn:10.247.120.227
```

The `-A` option is similar: a random `dns` name is generated, but the registration will update to point to your new `tun` IP when you roll.

It can be combined with `-R`, if desired.
```bash
$ autovnet rtfm tun up -A   
...
[+] map_ip: 10.213.225.184
spotty.bonus.avn:10.213.225.184
[>] Roll new IP?
 [y/N]: y
[+] map_ip: 10.203.205.154
spotty.bonus.avn:10.203.205.154
```

Finally, `-a` is like `-A`, giving you a `dns` registration that updates when you roll.
But `-a` allows you to specify the name to register.

It can also be combined with `-R`.

```bash
$ autovnet rtfm tun up -a foo.avn
[+] map_ip: 10.210.207.243
foo.avn:10.210.207.243
[>] Roll new IP?
 [y/N]: y
[+] map_ip: 10.225.104.2
foo.avn:10.225.104.2
```
