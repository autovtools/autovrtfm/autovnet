# util

`autovnet` includes some utilities that do not fit well into the [`pwn`](../pwn/README.md) or [`collaboration`](../collaboration/README.md) categories.

They are lumped into the `util` category.
