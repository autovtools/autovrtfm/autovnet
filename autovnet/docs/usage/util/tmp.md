# tmp

[TOC]

## Intro

Have you ever needed a clean scratch directory to start an `http.server`, extract an archives, do a clean `git clone`, etc. ?

`autovnet tmp` makes this easy.

## `autovnet tmp shell`

`autovnet tmp shell` will create a clean scratch directory (by default in `/opt/rtfm/tmp`) and spawn a new shell in that cwd.

After installing `autovnet`, the `.tmp` shortcut is available to save a few keystrokes.

```bash
$ pwd
/foo

$ .tmp
[+] /opt/rtfm/tmp/acidic_court

$ pwd
/opt/rtfm/tmp/acidic_court

$ exit

$ pwd
/foo
```

By default, the created directory is deleted when you `exit` your new shell

## `autovnet tmp dir`

`autovnet tmp dir` creates and prints a tmp directory, which may be helpful in scripts.

```bash
$ autovnet tmp dir
/opt/rtfm/tmp/sharp_shoulder
```

For example, you can do something like:
```bash
$ pushd $(autovnet tmp dir) && python3 -m http.server && popd
/opt/rtfm/tmp/faded_selection ~/
Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ...
```

But note that the `tmp` directory will not be deleted util you call `autovnet tmp clean`.


## `autovnet tmp clean`

`autovnet tmp clean` simply cleans up the `tmp` directory (default `/opt/rtfm/tmp`).

If you store big things in your `tmp` dirs, clean them regularly to free up your space.
```bash
$ autovnet tmp clean
Recursively delete /opt/rtfm/tmp ? [y/N]: y
```
