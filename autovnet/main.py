import os
import signal
import traceback
from pathlib import Path
from typing import Optional, List

import typer
import yaml
import pydantic

from .autovnet import AutoVnet, avn

from autovnet import app
from autovnet.models import *
import autovnet.cmds

NO_CONFIG_REQUIRED = ["config", "docs"]

app.add_typer(autovnet.cmds.config.app, name="config", help="Manage autovnet config")

app.add_typer(autovnet.cmds.net.app, name="net", help="Manage IP network simulation")

app.add_typer(autovnet.cmds.tmp.app, name="tmp", help="Create tmp dirs")

app.add_typer(autovnet.cmds.docs.app, name="docs", help="Manage autovnet offline docs")

app.add_typer(autovnet.cmds.rtfm.app, name="rtfm", help="Unleash the power of RTFM\n(Red Team Force Multiplication)")

app.add_typer(autovnet.cmds.apt.app, name="apt", help="Manage APT profiles")

app.add_typer(autovnet.cmds.pasta.app, name="pasta", help="Render (copy)pasta for easy copy-paste")

app.add_typer(autovnet.cmds.type.app, name="type", help="Execute command, writing a payload to STDIN.")


@app.callback()
def main(
    ctx: typer.Context,
    verbose: Optional[bool] = typer.Option(False, "-v", "--verbose", help="Increase log verbosity."),
    config: Optional[str] = typer.Option(AutoVnet.config_path("config.yaml"), "--config", help="Path to config file."),
):
    try:
        avn.init(config, verbose=verbose)
    except FileNotFoundError as e:
        if ctx.invoked_subcommand not in NO_CONFIG_REQUIRED:
            raise


def sigterm_handler(sig, frame):
    # Convert SIGTERM signal to nice exception that we can catch and handle gracefully
    raise KeyboardInterrupt


def entrypoint():
    signal.signal(signal.SIGTERM, sigterm_handler)
    try:
        app()
    except Exception as e:
        if os.environ.get("AUTOVNET_STACK_TRACE", "0") != "0":
            typer.echo("=" * 80)
            traceback.print_exc()
            typer.echo("=" * 80)
        typer.echo(f"{e}")
        os._exit(1)
