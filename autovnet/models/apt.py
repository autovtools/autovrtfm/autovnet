from enum import Enum


class APTSize(str, Enum):
    one = "one"
    min = "min"
    tiny = "tiny"
    small = "small"
    default = "default"
    medium = "medium"
    large = "large"
    huge = "huge"
