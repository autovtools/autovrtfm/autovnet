from typing import Optional, List, Dict

from pydantic import BaseModel  # pylint: disable=no-name-in-module


class Creds(BaseModel):
    user: Optional[str] = None
    password: Optional[str] = None
    key: Optional[str] = None
    pubkey: Optional[str] = None
    host: Optional[str] = None
    ports: Optional[Dict[str, int]] = {}
    reuse: Optional[bool] = True

    def ssh_dest(self):
        return f"{self.user}@{self.host}"
