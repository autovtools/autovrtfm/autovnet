from typing import Optional, List

from pydantic import BaseModel  # pylint: disable=no-name-in-module

from autovnet.utils import get_ip


class Dns(BaseModel):
    host: Optional[str] = get_ip()
    port: Optional[int] = 5353
    enable: Optional[bool] = True
    tld: Optional[str] = "avn"
    server_names: Optional[List[str]] = ["autovnet", "core", "avn", "ns"]
