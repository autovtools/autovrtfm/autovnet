import os
from pathlib import Path
from typing import List

import yaml
import typer


def _get_param(ctx: typer.Context, param_name: str):
    # Silly work around because ctx.params.get and ctx.lookup_default don't
    # actually return the default value of a param
    if ctx.params.get(param_name):
        # Explictily set by user
        return ctx.params.get(param_name)

    for p in ctx.command.params:
        if p.name == param_name:
            return p.get_default(ctx)
    return None


def _complete_codename(ctx: typer.Context, incomplete: str, payload_dir: str):
    payload_dir = Path(payload_dir)
    if not payload_dir.is_dir():
        return []

    for path in payload_dir.glob(f"{incomplete}*"):
        readme_path = path / "README"
        readme = ""
        if readme_path.is_file():
            readme = readme_path.read_text()
        yield (path.name, readme)
    return []


def complete_files(ctx: typer.Context, incomplete: str):
    incomplete = Path(incomplete)
    paths = []
    if incomplete.exists():
        paths = list(Path(incomplete).glob("*"))
    else:
        paths = list(Path(incomplete.parent).glob(f"{incomplete.name}*"))

    # hack: returning a single option will insert a space
    # so, never return a dir as the only option
    if len(paths) == 1 and paths[0].is_dir():
        yield f"{paths[0]}/"
        yield f"{paths[0]}"
    else:
        for p in paths:
            yield str(p)


def complete_codename(ctx: typer.Context, incomplete: str):
    """Do codename completion

    Assumes basedir contains only directories named with the code names
    """
    payload_dir = _get_param(ctx, "payload_dir") or "/opt/rtfm/payloads"
    return _complete_codename(ctx, incomplete, payload_dir)


def complete_msf_codename(ctx: typer.Context, incomplete: str):
    payload_dir = _get_param(ctx, "msf_dir") or _get_param(ctx, "payload_dir") or "/opt/rtfm/msf"

    return _complete_codename(ctx, incomplete, payload_dir)


def complete_opt_codename(ctx: typer.Context, incomplete: str):
    payload_dir = _get_param(ctx, "payload_dir") or "/opt/rtfm/payloads"
    if ctx.params.get("codename"):
        # Codename completion
        return complete_codename(ctx, incomplete)
    else:
        # File completion
        return complete_files(ctx, incomplete)


def _complete_apt_infra(ctx: typer.Context, incomplete: str, key: str):
    payload_dir = _get_param(ctx, "payload_dir") or "/opt/rtfm/apt/profiles"
    payload_dir = Path(payload_dir)
    apt = _get_param(ctx, "profile") or os.getenv("AVN_APT")

    if apt is None or not payload_dir.is_dir():
        return []

    path = payload_dir / apt / f"{apt}.yaml"
    if not path.is_file():
        return []
    infra = yaml.safe_load(path.read_text())

    if key not in infra:
        return []

    for ip, data in infra[key].items():
        if ip.startswith(incomplete):
            yield (ip, data)


def complete_apt_dns(ctx: typer.Context, incomplete: str):
    return _complete_apt_infra(ctx, incomplete, "dns")


def complete_apt_geo(ctx: typer.Context, incomplete: str):
    return _complete_apt_infra(ctx, incomplete, "geo")
