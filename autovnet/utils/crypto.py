import os
import struct
import secrets
import tempfile
import shutil
import subprocess
import hashlib
from pathlib import Path

from codenamize import codenamize
from getpass import getpass

from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
from Crypto.Protocol.KDF import scrypt

PASSWORD_WORDS = 3
SALT_LEN = 32
IV_LEN = 16
KEY_LEN = 32
GMAC_LEN = 16

# https://pycryptodome.readthedocs.io/en/latest/src/protocol/kdf.html?highlight=pbkdf#scrypt-func
# More secure == more slow; too slow is annoying
SCRYPT_COST = 1 << 20
SCRYPT_BLOCK_SIZE = 8
SCRYPT_PARALLEL = 1

# SALT | IV | DATA | GMAC
ENC_HEADER = f"{SALT_LEN}s{IV_LEN}s"
ENC_FMT = "{}s".join([f"{ENC_HEADER}", f"{GMAC_LEN}s"])

HASH_LEN = len(hashlib.sha256().hexdigest())


def hash_file(path):
    chunk_len = 1 << 16
    sha256 = hashlib.sha256()
    with open(path, "rb") as f:
        while True:
            data = f.read(chunk_len)
            if not data:
                break
            sha256.update(data)
    return sha256.hexdigest()


def derive_key(password, salt):
    return scrypt(password, salt, KEY_LEN, SCRYPT_COST, SCRYPT_BLOCK_SIZE, SCRYPT_PARALLEL)


def encrypt_data(plaintext, password):
    salt = get_random_bytes(SALT_LEN)
    iv = get_random_bytes(IV_LEN)
    key = derive_key(salt, password)

    cipher = AES.new(key, AES.MODE_GCM, nonce=iv)
    header = struct.pack(ENC_HEADER, salt, iv)
    cipher.update(header)
    encrypted, gmac = cipher.encrypt_and_digest(plaintext)

    return header + encrypted + gmac


def unpack_data(encrypted):
    header_len = struct.calcsize(ENC_HEADER)
    data_len = len(encrypted) - header_len - GMAC_LEN

    return struct.unpack(ENC_FMT.format(data_len), encrypted)


def decrypt_data(encrypted, password):
    salt, iv, encrypted, gmac = unpack_data(encrypted)
    key = derive_key(salt, password)

    cipher = AES.new(key, AES.MODE_GCM, nonce=iv)
    cipher.update(salt + iv)
    return cipher.decrypt_and_verify(encrypted, gmac)


# Helpers for files
def encrypt_file(in_path, password, out_path=None):
    if out_path is None:
        out_path = in_path
    plaintext = Path(in_path).read_bytes()
    encrypted = encrypt_data(plaintext, password)
    Path(out_path).write_bytes(encrypted)


def decrypt_file(in_path, password, out_path=None):
    if out_path is None:
        out_path = in_path
    encrypted = Path(in_path).read_bytes()
    plaintext = decrypt_data(encrypted, password)
    Path(out_path).write_bytes(plaintext)


def gen_password(secure=True):
    """Lower-level password generation"""
    ret = secrets.token_urlsafe(KEY_LEN)
    if secure:
        return ret
    return codenamize(ret, PASSWORD_WORDS - 1, capitalize=True).replace(" ", "")


def get_password(password=None, prompt=False, secure=False, generate=True):
    """Higher-level password generation"""
    if password is not None:
        return password
    if prompt:
        return getpass("Password: ")
    if not generate:
        return None
    # Generate
    return gen_password(secure=secure)


def gen_codename(seed="", tokens=2, join="_"):
    if os.getenv("AVN_CN"):
        # Allow magic environment variable to force codename allocation to use a precomputed value
        # (This can be helpful in scripts)
        return os.getenv("AVN_CN")
    if not seed:
        seed = secrets.token_bytes(256)
    name = codenamize(seed, tokens - 1, join=join)
    if " " not in join:
        name = name.replace(" ", "")
    return name


def gen_adjective():
    return gen_codename(tokens=2, join=" ").split()[0]


def gen_cert(cert_path, key_path):
    """Create a simple self-signed cert with openssl"""

    Path(cert_path).parent.mkdir(parents=True, exist_ok=True)
    Path(key_path).parent.mkdir(parents=True, exist_ok=True)
    cert_cmd = [
        "openssl",
        "req",
        "-x509",
        "-newkey",
        "rsa:4096",
        "-nodes",
        "-keyout",
        str(key_path),
        "-out",
        str(cert_path),
        "-sha256",
        "-days",
        "3650",
        "-subj",
        "/C=XX/ST=X/L=X/O=autovnet/OU=rtfm/CN=rtfm.localhost",
    ]

    subprocess.run(cert_cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, check=True)

    if not cert_path.is_file():
        raise FileNotFoundError(f"[-] Cert {cert_path} was not created!")
    if not key_path.is_file():
        raise FileNotFoundError(f"[-] Key {key_path} was not created!")


def ensure_cert(cert_path, key_path):
    """Call gen_cert(), if the cert does not already exist"""

    if not Path(cert_path).is_file() or not Path(key_path).is_file():
        gen_cert(cert_path, key_path)


def gen_dhparam(dhparam_path):
    Path(dhparam_path).parent.mkdir(parents=True, exist_ok=True)

    # Sacrifice some security for speed - dhparam is painfully slow and CPU-expensive to generate
    # https://security.stackexchange.com/questions/95178/diffie-hellman-parameters-still-calculating-after-24-hours
    cert_cmd = [
        "openssl",
        "dhparam",
        "-dsaparam",
        "-out",
        str(dhparam_path),
        "2048",
    ]
    subprocess.run(cert_cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, check=True)

    if not Path(dhparam_path).is_file():
        raise FileNotFoundError(f"[-] dhparam {dhparam_path} was not created!")


#######################################
# Cryptor Class
#######################################


class Cryptor:
    """A helper for encrypting and decrypting files"""

    def __init__(self, password=None, secure=True, generate=False, chunk_len=(1 << 16)):
        self.secure = secure
        self.password = password
        if generate and self.password is None:
            self.password = gen_password(self.secure)

        self.cipher = None
        self.salt = None
        self.iv = None
        self.gmac = None
        self.key = None
        self.header = None

        self.in_file = None
        self.out_file = None
        self.remaining = None
        self.chunk_len = chunk_len

    def init_encrypt(self):
        self.salt = get_random_bytes(SALT_LEN)
        self.iv = get_random_bytes(IV_LEN)
        self.key = derive_key(self.salt, self.password)
        self.gmac = None

        self.cipher = AES.new(self.key, AES.MODE_GCM, nonce=self.iv)
        self.header = struct.pack(ENC_HEADER, self.salt, self.iv)

    def encrypt_file(self, in_path, out_path=None, password=None):
        if password is not None:
            self.password = password
        if self.password is None:
            self.password = gen_password(self.secure)

        if out_path is None:
            out_path = in_path
        in_path = Path(in_path)
        if not in_path.is_file():
            raise FileNotFoundError(f"[-] File not found: {in_path}")
        self.init_encrypt()
        with tempfile.NamedTemporaryFile(delete=False) as tmp_file:
            try:
                with in_path.open("rb") as self.in_file:
                    tmp_file.write(self.header)
                    while True:
                        chunk = self.in_file.read(self.chunk_len)
                        if not chunk:
                            break
                        encrypted = self.cipher.encrypt(chunk)
                        tmp_file.write(encrypted)
                    self.gmac = self.cipher.digest()
                    tmp_file.write(self.gmac)
                tmp_file.close()
                shutil.move(tmp_file.name, out_path)
            finally:
                Path(tmp_file.name).unlink(missing_ok=True)

    def decrypt_file(self, in_path, out_path=None, password=None):
        if password is not None:
            self.password = password
        if self.password is None:
            self.password = getpass("Decryption Password: ")

        if out_path is None:
            out_path = in_path
        in_path = Path(in_path)
        if not in_path.is_file():
            raise FileNotFoundError(f"[-] File not found: {in_path}")

        header_len = struct.calcsize(ENC_HEADER)
        file_len = in_path.stat().st_size
        file_len -= GMAC_LEN
        file_len -= header_len
        if file_len <= 0:
            raise ValueError(f"[-] File too short: {in_path}")
        with tempfile.NamedTemporaryFile(delete=False) as tmp_file:
            try:
                with in_path.open("rb") as self.in_file:
                    self.header = self.in_file.read(header_len)
                    if len(self.header) != header_len:
                        raise Exception(f"[-] Failed to read header")
                    # Get key
                    self.salt, self.iv = struct.unpack(ENC_HEADER, self.header)
                    self.key = derive_key(self.salt, self.password)
                    self.cipher = AES.new(self.key, AES.MODE_GCM, nonce=self.iv)

                    self.remaining = file_len
                    while self.remaining > 0:
                        chunk = self.in_file.read(min(self.chunk_len, self.remaining))
                        tmp_file.write(self.cipher.decrypt(chunk))
                        self.remaining -= len(chunk)
                    self.gmac = self.in_file.read(GMAC_LEN)
                    if len(self.gmac) != GMAC_LEN:
                        raise Exception(f"[-] Failed to read GMAC")
                    self.cipher.verify(self.gmac)
                tmp_file.close()
                shutil.move(tmp_file.name, out_path)
            finally:
                Path(tmp_file.name).unlink(missing_ok=True)

    def crypt(self, data):
        pass

    def verify(self, gmac):
        return self.cipher.verify(gmac)
