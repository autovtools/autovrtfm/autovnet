import select
import fcntl
import os
import time
import shlex
import subprocess
import sys
import termios
from pathlib import Path
from contextlib import contextmanager

from .docker import _thread_ctx


GHOST_TYPE_CHAR_DELAY = 0
GHOST_TYPE_LINE_DELAY = 0.05


def tar_extract(archive, out_dir):
    """A safe tar extractall to stop nerds from hacking each other

    WARNING: Could be wrong

    https://stackoverflow.com/questions/10060069/safely-extract-zip-or-tar-using-python
    https://medium.com/ochrona/python-path-traversal-prevention-the-tarbomb-5be58f06dd70
    https://github.com/cwgem/python-safe-tar-extract/blob/master/extract.py
    """
    out_dir = Path(out_dir).resolve(strict=True)

    for member in archive.getmembers():
        target_file = Path(os.path.normpath(out_dir / member.name)).resolve()
        target_link = None
        if member.issym() or member.islnk():
            target_link = Path(os.path.normpath(out_dir / member.linkname)).resolve()

        if out_dir not in target_file.parents or (target_link is not None and out_dir not in target_link.parents):
            raise ValueError(f"[-] Malicious .tar detected! OOB write to {target_file} | {target_link} was blocked!")

        archive.extract(member, path=out_dir)


def ioctl_yeet(tty, text):
    """yeet text at tty with ioctls"""
    # Is this a good idea?
    termios.tcdrain(tty)

    # Disable tty echo
    # https://devdocs.io/python~3.9/library/termios#termios.tcsetattr
    old = termios.tcgetattr(tty)
    new = termios.tcgetattr(tty)
    # set lflag
    new[3] = new[3] & ~termios.ECHO
    try:
        termios.tcsetattr(tty, termios.TCSADRAIN, new)
        for c in text:
            fcntl.ioctl(tty, termios.TIOCSTI, c)
            # hack: it looks like some input gets dropped if we write too fast
            #   e.g. Try for: quit > Are you sure? > y
            #        not    : quit > y > Are you sure?
            if c == "\n":
                time.sleep(GHOST_TYPE_LINE_DELAY)
            else:
                time.sleep(GHOST_TYPE_CHAR_DELAY)
    finally:
        # turn echo back on, no matter what
        termios.tcsetattr(tty, termios.TCSADRAIN, old)


@contextmanager
def ghost_type(cmd=None, text="", force_newline=True, check=False):
    """Execute cmd interactively, but ask a ghost to type text into STDIN."""
    # https://unix.stackexchange.com/questions/48103/construct-a-command-by-putting-a-string-into-a-tty/48221
    proc = None
    pid = os.getpid()
    # It's possible for a program to change it's own TTY settings
    # while ioctl_yeet will probably keep the TTY settings sane within the current context (e.g. tool prompt)
    # we need to save and restore the current / shell TTY settings here
    self_tty = tty_path = Path(f"/proc/self/fd/0").open("w")
    old = termios.tcgetattr(self_tty)
    try:
        if cmd is not None:
            proc = subprocess.Popen(cmd, stdin=sys.stdin, stdout=sys.stdout, stderr=sys.stderr)
            try:
                # Allow child some time to start and make any TTY changes it wants to make
                # WARNING: Magic sleep that is also wrong
                # a build can take arbitrarily long before the actual app makes ioctl changes
                proc.wait(1)
                return proc
            except subprocess.TimeoutExpired:
                pass
        try:
            tty_path = Path(f"/proc/{pid}/fd/0")
            if text:
                if force_newline and not text.endswith("\n"):
                    text += "\n"
                with tty_path.resolve().open("w") as tty:
                    ioctl_yeet(tty, text)
            yield proc, tty_path
        finally:
            if proc is not None:
                proc.terminate()
                proc.wait()

        if proc is not None:
            ret = proc.poll()
            if check and proc.poll() != 0:
                cmd_str = cmd
                if not isinstance(cmd_str, str):
                    cmd_str = shlex.join(cmd_str)
                raise Exception(
                    f"[-] Command returned {ret}: {cmd_str}",
                )
    finally:
        # Also flush all buffered input to avoid accidentally typing into
        # the user's shell
        termios.tcsetattr(self_tty, termios.TCSAFLUSH, old)
        self_tty.close()

    return proc


def ghost_type_listener(fd, tty, stop=None):
    poller = select.poll()
    poller.register(fd, select.POLLIN)
    chunk_len = 4096
    while stop is None or not stop.is_set():
        ready = poller.poll(1)
        if not ready:
            continue
        # ready
        chunk = os.read(fd, chunk_len)
        if not chunk:
            break
        # Definitely not binary safe
        # Definitely do not care
        ioctl_yeet(tty, chunk.decode("latin-1"))


@contextmanager
def ghost_type_ctx(cmd=None, text="", force_newline=True, check=False, magic_fd=42):
    """See ghost_type. Any text written to /proc/PID/fd/42 will be ghost_type'd to cmd

    Unix only (lol)
    WARNING: gigahax
    """
    (r, w) = os.pipe()
    os.dup2(w, magic_fd, inheritable=False)
    with ghost_type(cmd=cmd, text=text, force_newline=force_newline, check=check) as (proc, tty_path):
        with tty_path.resolve().open("w") as tty:
            with _thread_ctx(r, tty, _target=ghost_type_listener, daemon=True) as (t, stop):
                yield proc, Path(f"/proc/{os.getpid()}/fd/{magic_fd}")
                # Try to get thread to exit
                stop.set()
                os.close(w)
                t.join()

        if proc is not None:
            proc.terminate()
            proc.wait()
