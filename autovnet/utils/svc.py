import re

from typing import Optional

from ..models import Mount

# manually imported because of circular import


def peek_mount(opt: str, payload_dir: str, suffix: str = "") -> (Optional[Mount], str):
    codename_pattern = re.compile("^[a-z_]+$")
    if codename_pattern.match(opt):
        codename = opt
        # Loading a codename, peek at the mount config to get the local addr
        try:
            mnt = Mount.load(payload_dir, codename, suffix=suffix)
            return mnt, codename
        except FileNotFoundError as e:
            pass
    return None, None
