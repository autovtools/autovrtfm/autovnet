# Level 0

Our goal is to connect via SSH.

This is the info we were given:

* Host: `bandit.labs.overthewire.org`
* Port: `2220`
* Username: `bandit0`
* Password: `bandit0`

## Determining the Correct Syntax

On Linux, `man` is your friend.

Read the manual for `ssh` to get an idea for the syntax:
```{: .host .host-title}
man ssh
```

The most relevant parts are:
```{: .check .check-title .no-copy}
ssh [-46AaCfGgKkMNnqsTtVvXxYy] [-B bind_interface] [-b bind_address] [-c cipher_spec]
         [-D [bind_address:]port] [-E log_file] [-e escape_char] [-F configfile] [-I pkcs11]
         [-i identity_file] [-J destination] [-L address] [-l login_name] [-m mac_spec] [-O ctl_cmd]
         [-o option] [-p port] [-Q query_option] [-R address] [-S ctl_path] [-W host:port]
         [-w local_tun[:remote_tun]] destination [command [argument ...]]
...
ssh connects and logs into the specified destination, which may be specified as either
     [user@]hostname or a URI of the form ssh://[user@]hostname[:port].
```

So, the basic syntax we are looking for is:
* `ssh [-p port] [user@]hostname`


## Connecting

We can fill in the values we were given to create a working command:

!!! note
    `-oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null` are advanced options that you can read about in `man ssh_config`.

    They aren't necessary, but the quick version is that these options avoid the `Are you sure you want to continue connecting (yes/no/[fingerprint])?` prompt you may have seen before

```{: .host .host-title}
ssh -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -p 2220 bandit0@bandit.labs.overthewire.org
```

```{: .check .check-title .no-copy}
Warning: Permanently added '[bandit.labs.overthewire.org]:2220' (ED25519) to the list of known hosts.
                         _                     _ _ _   
                        | |__   __ _ _ __   __| (_) |_ 
                        | '_ \ / _` | '_ \ / _` | | __|
                        | |_) | (_| | | | | (_| | | |_ 
                        |_.__/ \__,_|_| |_|\__,_|_|\__|
                                                       

                      This is an OverTheWire game server. 
            More information on http://www.overthewire.org/wargames

bandit0@bandit.labs.overthewire.org's password: 
```

You are being prompted for the `bandit0@bandit.labs.overthewire.org's password`.

Remember, we were given this in the challenge.

Enter the password:
```{: .target .target-title}
bandit0
```

You should successfully log in.
```{: .check .check-title .no-copy}
...
bandit0@bandit:~$
```

!!! warning
    **READ AND FOLLOW THE RULES PROVIDED BY OVERTHEWIRE**

    **They** own this system, not you.

## Disconnecting

Well... that's all for Level 0.

Exit your `ssh` connection.
```{: .target .target-title}
exit
```

```{: .check .check-title .no-copy}
logout
Connection to bandit.labs.overthewire.org closed.
```
