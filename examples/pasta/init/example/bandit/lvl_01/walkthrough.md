# Level 1

Our goal is extract a password from the `readme` file in the home directory.

## Connecting

```{: .host .host-title}
ssh -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -p 2220 bandit0@bandit.labs.overthewire.org
```

```{: .check .check-title .no-copy}
Warning: Permanently added '[bandit.labs.overthewire.org]:2220' (ED25519) to the list of known hosts.
                         _                     _ _ _
                        | |__   __ _ _ __   __| (_) |_
                        | '_ \ / _` | '_ \ / _` | | __|
                        | |_) | (_| | | | | (_| | | |_
                        |_.__/ \__,_|_| |_|\__,_|_|\__|


                      This is an OverTheWire game server.
            More information on http://www.overthewire.org/wargames

bandit0@bandit.labs.overthewire.org's password:
```

You are being prompted for the `bandit0@bandit.labs.overthewire.org's password`.

Remember, we were given this in the challenge.

Enter the password:
```{: .target .target-title}
bandit0
```

You should successfully log in.
```{: .check .check-title .no-copy}
...
bandit0@bandit:~$
```

## Finding the Home Directory

On Linux, you can find the home directory of your user several ways:

* `~` is a shorthand reference to your home directory
* `$HOME` is an environment variable that is set to your home directory
* When you log in via `ssh`, you start in your home directory, so `pwd` shows your home directory
* The home directory for all users are listed in `/etc/passwd`

Since we are already in our home directory, (indicated by the `~` in our prompt), we don't actually have to do anything.

## Finding `readme`

One of the most frequently executed Linux commands is `ls`, which lists the files in a directory.

As always, you can use `man ls` to find out more.

```{: .target .target-title}
ls
```

```{: .check .check-title .no-copy}
readme
```

Oh... there it is - the file we are looking for.

## Reading File Contents

To read the contents of a text file on Linux, you can use the `cat` command.

The cat command accepts the file name / path to read.

!!! note
    Usually, when you type a Linux command that accepts a file path, you can use the `[TAB]` key to complete the file path!

    Linux pros spam `[TAB]` as they type paths, because spelling is hard, and tabbing is both fast and easy.

```{: .target .target-title}
cat readme
```

```{: .check .check-title .no-copy}
NH2SXQwcBdpmTEzi3bvBHMM9H66vVXjL
```

Very cool, that's the password for the `bandit01` user!
