# Level 3

## Connecting

```{: .host .host-title}
ssh -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -p 2220 bandit2@bandit.labs.overthewire.org
```

```{: .check .check-title .no-copy}
...
bandit2@bandit.labs.overthewire.org's password:
```

Enter the password:
```{: .target .target-title}
rRGizSaX8Mk1RTb1CNQoXTcYZWU6lgzi
```

You should successfully log in.
```{: .check .check-title .no-copy}
...
bandit2@bandit:~$
```

## Winning

```{: .target .target-title}
cat 'spaces in this filename'
```

```{: .check .check-title .no-copy}
aBZ0W5EmUfAf7kHTQeOwd8bauFJ2lAiG
```
