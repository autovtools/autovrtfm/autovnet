# Level 4

## Connecting

```{: .host .host-title}
ssh -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -p 2220 bandit3@bandit.labs.overthewire.org
```

```{: .check .check-title .no-copy}
...
bandit3@bandit.labs.overthewire.org's password:
```

Enter the password:
```{: .target .target-title}
aBZ0W5EmUfAf7kHTQeOwd8bauFJ2lAiG
```

You should successfully log in.
```{: .check .check-title .no-copy}
...
bandit3@bandit:~$
```

## Winning

```{: .target .target-title}
ls -al inhere
```

```{: .check .check-title .no-copy}
total 12
drwxr-xr-x 2 root    root    4096 Dec  3 08:14 .
drwxr-xr-x 3 root    root    4096 Dec  3 08:14 ..
-rw-r----- 1 bandit4 bandit3   33 Dec  3 08:14 .hidden
```

```{: .target .target-title}
cat inhere/.hidden
```

```{: .check .check-title .no-copy}
2EW7BBsr6aMMoJ2HjW067dm8EgX26xNe
```
