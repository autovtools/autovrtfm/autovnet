# Prep

## Structure

This module is structured as a walkthrough.


## Formatting

Let's practice running some commands, and make sure you are online.

This guide uses color-coded code blocks so you know where you should run each command.

```{: .host .host-title}
echo "Commands like this will run on your system"
```

```{: .target .target-title}
echo "Commands like this will run on the target system"
```

```{: .check .check-title .no-copy}
This is not a command, it is example output
```

??? hint

    This is a hint / spoiler.

    Hint: the answer is 42

## Connectivity Check

We will ping `8.8.8.8`, a popular IP address owned by Google, to check if we are online

```{: .host .host-title}
ping -c 1 8.8.8.8
```

You should see something like this:

!!! note
    Example output in this guide may not exactly match your output.
    This is okay.

```{.check .check-title .no-copy}
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=11.1 ms

--- 8.8.8.8 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 11.098/11.098/11.098/0.000 ms
```


Now, let's do another test.

`curl` will fetch a website over HTTP.

This test verifies that we can browse the web, and resolve DNS names like `google.com`

```{: .host .host-title}
curl google.com
```

```{: .check .check-title .no-copy}
<HTML><HEAD><meta http-equiv="content-type" content="text/html;charset=utf-8">
<TITLE>301 Moved</TITLE></HEAD><BODY>
<H1>301 Moved</H1>
The document has moved
<A HREF="http://www.google.com/">here</A>.
</BODY></HTML>
```

Success! You are online!

## Bandit

Now, we'll browse to [Over the Wire: Bandit](https://overthewire.org/wargames/bandit/), which this guide will walk through.

```{: .host .host-title}
firefox https://overthewire.org/wargames/bandit/
```

This should open a new browser tab.


Now, we'll do the real test, and verify that you can connect to the bandit server.

```{: .host .host-title}
nc -v bandit.labs.overthewire.org 2220
```

```{: .check .check-title .no-copy}
Connection to bandit.labs.overthewire.org (13.50.114.40) 2220 port [tcp/*] succeeded!
SSH-2.0-OpenSSH_8.9p1
```

See that `SSH-2.0...` line?
That means we can connect to the remote SSH server, and are able to continue!

!!! note
    If you get a connect timeout, your network admin might block output connections to random high ports.
    Use a VPN if you can, otherwise, you may not be able to follow this guide :(
