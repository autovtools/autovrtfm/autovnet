# Formatting

Ready to write your own `pasta`-formatted documentation?

`pasta` uses the following technologies:

* [markdown](https://www.markdownguide.org/)
* [mkdocs](https://www.mkdocs.org/)
