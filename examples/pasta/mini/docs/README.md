# mini `pasta`

[TOC]

## Intro

Bite-sized `pasta` providing a minimal example.

This uses the magic variable `LHOST`:
```bash
ping {{LHOST}}
```

You can also run shell commands, if you need to.

Here is your `hostname`:
```bash
$ hostname
{{shell_exec("hostname")}}
```

Here is your default route on your system:
```bash
# This is the command we are about to run:
$ {{IP_ROUTE_CMD}}

# This the the actual output:
# (if reading the .md file, notice that we only need 1 pair of curly braces here)
{{ shell_exec( IP_ROUTE_CMD ) }}
```
