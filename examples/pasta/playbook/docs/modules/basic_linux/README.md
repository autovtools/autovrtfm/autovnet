# basic_linux

This is an example sequence of TTPs targeting a Linux server.

The intent is to show how individual plays can be written independently, then composed together.

The order and structure of the final, rendered site is defined in `mkdocs.yml`.
