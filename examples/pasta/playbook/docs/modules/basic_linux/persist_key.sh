#!/bin/bash

set -euo pipefail
# This is just a simple bash script
# It's purpose is to demonstrate how you can write scripts
#   referenced by a pasta file

# Scripts are also rendered as jinja templates
PUBKEY_PATH="/opt/rtfm/keys/{{TARGET}}/id_rsa.pub"

cat << EOF
# RUN AS ROOT ON {{TARGET}} :

mkdir -p ~/.ssh; echo "$(cat "${PUBKEY_PATH}")" >> ~/.ssh/authorized_keys
EOF

