# Reverse Shells Pasta

[TOC]

## Intro

This is an example project to show off the `autovnet pasta` feature.

It is based on a [reverse shell cheatsheet](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Reverse%20Shell%20Cheatsheet.md).

