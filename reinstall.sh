#!/bin/bash
set -eu

poetry build
pipx uninstall autovnet || true
pipx install --force dist/autovnet-*.whl
# TODO: remove after https://github.com/tiangolo/typer-cli/issues/118 is resolved
pipx inject --force autovnet typer-cli==0.0.13

# Regenerate the help so we don't forget to
# TODO: make it a pre-commit hook
.run typer autovnet.main utils docs --name autovnet --output autovnet/docs/usage/help.md

# Re-build the docs
autovnet docs build --force

# Re-install completions
autovnet --install-completion

# Hack in a copy of pylint so we can lint it
pipx inject --force autovnet pylint
.run pylint -E autovnet || pipx uninstall autovnet

cat << EOF
# Run:
. ~/.$(basename "${SHELL}")rc
EOF
