#!/bin/bash
if [ ! -f pyproject.toml ]; then
    echo "Run from root of checkout"
    exit 1
fi

typer autovnet.main utils docs --name autovnet --output docs/usage/help.md
sed -i "s|${HOME}|~|g" docs/usage/help.md
