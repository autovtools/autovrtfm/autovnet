#!/bin/bash

set -euo pipefail

cat << EOF
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! DANGER DANGER DANGER
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! uninstall might not remove everything from autovnet
! uninstall will PERMANENTLY DELETE all data associated with autovnet
! uninstall might PERMANENTLY DELETE non-autovnet data!
! (no warranty, use at your own risk)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
EOF

read -p "ENTER to continue CTRL+C to abort"

set -x
rm -rf ~/.venv/rtfm
sudo rm -rf /opt/rtfm
rm -rf ~/.autovnet
rm -rf ~/autovnet

# If you are using docker in even a somewhat sane way, this is always safe to do.
# If you are a psychopath who uses your local docker cache to save images that you can't easily re-download or rebuild,
# you kinda deserve having them pruned without warning, tbh. But here's a warning, plus docker will prompt for confirmation.
set +x
read -p "About to run 'docker system prune -a' CTRL+C to abort"
set -x
docker system prune -a
